# TODOS

## After Migration

- migrate download to improved media server
- use next image [loader](https://nextjs.org/docs/basic-features/image-optimization#loader) with media server
- improve article pagination with flexible teaser placement
- collect web vitals
- translate [i18next](https://github.com/isaachinman/next-i18next)
- improve next.js data fetching for navigations
- import sponsors into MetaData Properties
- import ewz and dynamo sponsors as tags (maybe dynamo directly from category)
- add CDN https://console.cloud.google.com/net-services/cdn/list?project=tsri-246213&authuser=1

