# Tsri

## Development

### Prerequisites

- [Node v12.x.x][node-download-url]
- [Yarn v1.17.x][yarn-download-url]

### Setup for development

1. Setup `yarn install && yarn setup && yarn installAll`
2. Start env (db/media) `yarn env:start` (to reset database/media run `yarn env:reset`)
3. Start in watch mode `yarn watch`

**Login Editor:**  
Username: admin@tsri.ch  
Password: 123

The following servers will be available:

- **Website:** [http://localhost:5000](http://localhost:5000)
- **Wepublish API:** [http://localhost:4000](http://localhost:4000)
- **Editor:** [http://localhost:3000](http://localhost:3000)

### Error handling

Use the handleError() function from the helpers.ts file.

### Related repositories

- [karma.run-packages][karmarun-packages-repo-url]
- [wepublish][wepublish-packages-repo-url]

[node-download-url]: https://nodejs.org/en/download/current/
[yarn-download-url]: https://yarnpkg.com/en/docs/install
[vscode-download-url]: https://code.visualstudio.com/Download
[vscode-prettier-download-url]: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
[karmarun-packages-repo-url]: https://github.com/karmarun/karma.run-packages/
[wepublish-packages-repo-url]: https://github.com/wepublish/wepublish/
