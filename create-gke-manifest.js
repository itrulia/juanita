const fs = require('fs')
const {spawn, exec} = require('child_process')
const {env} = require('husky/.eslintrc')
try {
  require('dotenv').config()
} catch (e) {}

const {CI_REGISTRY_IMAGE, CI_COMMIT_SHORT_SHA, CI_COMMIT_TAG, TARGET} = process.env

const SHA_OR_TAG = CI_COMMIT_TAG ? CI_COMMIT_TAG : CI_COMMIT_SHORT_SHA

let ENVIRONMENT_NAME = 'development'
console.log('Target', TARGET)
if (TARGET === 'production') {
  ENVIRONMENT_NAME = 'production'
}
const NAMESPACE = envSwitch(ENVIRONMENT_NAME, 'juanita', 'juanita-dev')

const domain = 'tsri.ch'
const devDomain = 'tsri.dev'
const domainCn = envSwitch(ENVIRONMENT_NAME, `${domain}`, `${devDomain}`)
const domainSan = envSwitch(
  ENVIRONMENT_NAME,
  `www.${domain},xn--tsri-1ra.ch,www.xn--tsri-1ra.ch,mitmachen.${domain}`,
  `www.${devDomain},mitmachen.${devDomain}`
)

const domainMedia = envSwitch(ENVIRONMENT_NAME, `media.${domain}`, `media.${devDomain}`)
const domainCachedMedia = envSwitch(
  ENVIRONMENT_NAME,
  `cached-media.${domain}`,
  `cached-media.${devDomain}`
)
const domainAPI = envSwitch(ENVIRONMENT_NAME, `api.${domain}`, `api.${devDomain}`)
const domainEditor = envSwitch(ENVIRONMENT_NAME, `editor.${domain}`, `editor.${devDomain}`)

main().catch(e => {
  process.stderr.write(e.toString())
  process.exit(1)
})

async function main() {
  await applyNamespace()
  /*if (TARGET !== 'production') {
    await applyMongo()
  }*/
  await applyWebsite()
  await applyMediaServer()
  await applyApiServer()
  await applyEditor()
  await applyDjango()
}

async function applyNamespace() {
  let namespace = {
    apiVersion: 'v1',
    kind: 'Namespace',
    metadata: {
      name: NAMESPACE,
      labels: {
        name: NAMESPACE
      }
    }
  }
  await applyConfig('namespace', namespace)
}

async function applyWebsite() {
  const servicePort = 5000
  const app = 'website'
  const appName = `${app}-${ENVIRONMENT_NAME}`

  const service = {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: appName,
      namespace: NAMESPACE
    },
    spec: {
      ports: [
        {
          name: 'http',
          port: servicePort,
          protocol: 'TCP',
          targetPort: servicePort
        }
      ],
      selector: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      sessionAffinity: 'None',
      type: 'ClusterIP'
    }
  }
  await applyConfig(`service-${app}`, service)

  function getRule(host) {
    return {
      host: host,
      http: {
        paths: [
          {
            backend: {
              service: {
                name: appName,
                port: {
                  number: servicePort
                }
              }
            },
            pathType: 'Prefix',
            path: '/'
          },
          {
            backend: {
              service: {
                name: `django-${ENVIRONMENT_NAME}`,
                port: {
                  number: 8000
                }
              }
            },
            pathType: 'Prefix',
            path: '/admin'
          },
          {
            backend: {
              service: {
                name: `django-${ENVIRONMENT_NAME}`,
                port: {
                  number: 8000
                }
              }
            },
            pathType: 'Prefix',
            path: '/shop'
          },
          {
            backend: {
              service: {
                name: `django-${ENVIRONMENT_NAME}`,
                port: {
                  number: 8000
                }
              }
            },
            pathType: 'Prefix',
            path: '/minishop'
          },
          {
            backend: {
              service: {
                name: `django-${ENVIRONMENT_NAME}`,
                port: {
                  number: 8000
                }
              }
            },
            pathType: 'Prefix',
            path: '/api/sentry_dsn'
          },
          {
            backend: {
              service: {
                name: `django-${ENVIRONMENT_NAME}`,
                port: {
                  number: 8000
                }
              }
            },
            pathType: 'Prefix',
            path: '/api/payrexx_webhook'
          },
          {
            backend: {
              service: {
                name: `django-${ENVIRONMENT_NAME}`,
                port: {
                  number: 8000
                }
              }
            },
            pathType: 'Prefix',
            path: '/api/v0/agenda/'
          }
        ]
      }
    }
  }

  const ingress = {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      annotations: {
        'kubernetes.io/ingress.class': 'nginx',
        'nginx.ingress.kubernetes.io/ssl-redirect': 'true',
        'nginx.ingress.kubernetes.io/proxy-body-size': '1m',
        'nginx.ingress.kubernetes.io/proxy-read-timeout': '30',
        'cert-manager.io/cluster-issuer': 'letsencrypt-production'
      }
    },
    spec: {
      ingressClassName: 'nginx',
      rules: [getRule(domainCn)],
      tls: [
        {
          hosts: [domainCn],
          secretName: `${appName}-tls`
        }
      ]
    }
  }
  await applyConfig(`ingress-${app}`, ingress)

  if (domainSan) {
    const domains = domainSan.split(',')
    const rules = domains.map(host => ({
      host: host,
      http: {
        paths: [
          {
            backend: {
              service: {
                name: appName,
                port: {
                  number: servicePort
                }
              }
            },
            pathType: 'Prefix',
            path: '/(.*)'
          }
        ]
      }
    }))
    const rewriteIngress = {
      apiVersion: 'networking.k8s.io/v1',
      kind: 'Ingress',
      metadata: {
        name: `${appName}-rewrite`,
        namespace: NAMESPACE,
        labels: {
          app: app,
          release: ENVIRONMENT_NAME
        },
        annotations: {
          'kubernetes.io/ingress.class': 'nginx',
          'nginx.ingress.kubernetes.io/ssl-redirect': 'true',
          'nginx.ingress.kubernetes.io/proxy-body-size': '1m',
          'nginx.ingress.kubernetes.io/proxy-read-timeout': '30',
          'nginx.ingress.kubernetes.io/rewrite-target': `https://${domainCn}/$1`,
          'cert-manager.io/cluster-issuer': 'letsencrypt-production',
          'nginx.ingress.kubernetes.io/server-snippet': `
            if ($request_uri ~* 'acme-challenge') { 
              break;
            }
            if ($host = 'mitmachen.${devDomain}') {
              return 301 https://${devDomain}/mitmachen;
            }
            if ($host = 'mitmachen.${domain}') {
              return 301 https://${domain}/mitmachen;
            }
          `
        }
      },
      spec: {
        ingressClassName: 'nginx',
        rules: [...rules],
        tls: [
          {
            hosts: domains,
            secretName: `${appName}-tls-rewrite`
          }
        ]
      }
    }
    await applyConfig(`rewrite-ingress-${app}`, rewriteIngress)
  }

  // Info Resources: https://github.com/kubernetes/community/blob/master/contributors/design-proposals/node/resource-qos.md
  const deployment = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: app,
          release: ENVIRONMENT_NAME
        }
      },
      strategy: {
        rollingUpdate: {
          maxSurge: 1,
          maxUnavailable: 0
        },
        type: 'RollingUpdate'
      },
      template: {
        metadata: {
          name: appName,
          labels: {
            app: app,
            release: ENVIRONMENT_NAME
          }
        },
        spec: {
          containers: [
            {
              name: appName,
              image: `${CI_REGISTRY_IMAGE}/website:${SHA_OR_TAG}`,
              env: [
                {
                  name: 'NODE_ENV',
                  value: `production`
                },
                {
                  name: 'HOST_ENV',
                  value: envSwitch(ENVIRONMENT_NAME, 'production', 'development')
                },
                {
                  name: 'API_URL',
                  value: `https://${domainAPI}`
                },
                {
                  name: 'API_URL_SSR',
                  value: ''
                },
                {
                  name: 'MY_POD_IP',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'status.podIP'
                    }
                  }
                },
                {
                  name: 'MY_POD_IP_PORT',
                  value: `${servicePort}`
                },
                {
                  name: 'ALLOWED_HOSTS',
                  value: `${domainCn},${domainSan}`
                },
                {
                  name: 'STRIPE_SECRET_KEY',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'stripe_secret_key'
                    }
                  }
                },
                {
                  name: 'TSRI_AGENDA_API_URL',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'tsri_agenda_api_url'
                    }
                  }
                },
                {
                  name: 'TSRI_AGENDA_API_USER',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'tsri_agenda_api_user'
                    }
                  }
                },
                {
                  name: 'TSRI_AGENDA_API_PASS',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'tsri_agenda_api_pass'
                    }
                  }
                },
                {
                  name: 'IMAGE_DOMAIN_LIST',
                  value: `${domainMedia},${domainCachedMedia},tsrich.s3.eu-central-1.amazonaws.com`
                },
                {
                  name: 'NEXTAUTH_URL',
                  value: `https://${domainCn}`
                },
                {
                  name: 'NEXTAUTH_SECRET',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'nextauth_secret'
                    }
                  }
                },
                {
                  name: 'OAUTH_GOOGLE_CLIENT_ID',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'oauth_google_client_id'
                    }
                  }
                },
                {
                  name: 'OAUTH_GOOGLE_CLIENT_KEY',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'oauth_google_client_key'
                    }
                  }
                },
                {
                  name: 'FACEBOOK_CLIENT_ID',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'facebook_client_id'
                    }
                  }
                },
                {
                  name: 'FACEBOOK_CLIENT_SECRET',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'facebook_client_secret'
                    }
                  }
                },
                {
                  name: 'TWITTER_CLIENT_ID',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'twitter_client_id'
                    }
                  }
                },
                {
                  name: 'TWITTER_CLIENT_SECRET',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'twitter_client_secret'
                    }
                  }
                },
                {
                  name: 'MONGO_URL',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'mongo_url'
                    }
                  }
                  /*...envSwitch(
                    ENVIRONMENT_NAME,
                    {
                      valueFrom: {
                        secretKeyRef: {
                          name: 'juanita-secrets',
                          key: 'mongo_url'
                        }
                      }
                    },
                    {value: `mongodb://mongo-${ENVIRONMENT_NAME}:27017/juanita`}
                  ) */
                },
                {
                  name: 'MONGODB_URI',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'oauth_mongodb_uri'
                    }
                  }
                  /* name: 'MONGODB_URI',
                  ...envSwitch(
                    ENVIRONMENT_NAME,
                    {
                      valueFrom: {
                        secretKeyRef: {
                          name: 'juanita-secrets',
                          key: 'oauth_mongodb_uri'
                        }
                      }
                    },
                    {value: `mongodb://mongo-${ENVIRONMENT_NAME}:27017/juanita_auth`}
                  ) */
                },
                {
                  name: 'MAILGUN_API_KEY',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'mailgun_api_key'
                    }
                  }
                },
                {
                  name: 'MAILGUN_BASE_DOMAIN',
                  value: 'api.eu.mailgun.net'
                },
                {
                  name: 'MAILGUN_MAIL_DOMAIN',
                  value: 'mg.tsrimails.ch'
                },
                {
                  name: 'SENTRY_JOIN_DSN',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'sentry_join_dsn'
                    }
                  }
                },
                {
                  name: 'SENTRY_RELEASE',
                  value: SHA_OR_TAG
                }
              ],
              ports: [
                {
                  containerPort: servicePort,
                  protocol: 'TCP'
                }
              ],
              imagePullPolicy: 'IfNotPresent',
              resources: {
                requests: {
                  cpu: envSwitch(ENVIRONMENT_NAME, '200m', '50m'),
                  memory: envSwitch(ENVIRONMENT_NAME, '500Mi', '256Mi')
                },
                limits: {
                  memory: envSwitch(ENVIRONMENT_NAME, '500Mi', '256Mi')
                }
              },
              readinessProbe: {
                httpGet: {
                  path: '/',
                  port: servicePort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                successThreshold: 1,
                timeoutSeconds: 10
              },
              livenessProbe: {
                httpGet: {
                  path: '/',
                  port: servicePort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                periodSeconds: 60,
                successThreshold: 1,
                timeoutSeconds: 10
              }
            }
          ]
        }
      }
    }
  }
  await applyConfig(`deployment-${app}`, deployment)

  const horizontalPodAutoscaler = {
    apiVersion: 'autoscaling/v2beta1',
    kind: 'HorizontalPodAutoscaler',
    metadata: {
      name: appName,
      namespace: NAMESPACE
    },
    spec: {
      minReplicas: 1,
      maxReplicas: 3,
      metrics: [
        {
          resource: {
            name: 'memory',
            targetAverageUtilization: 90
          },
          type: 'Resource'
        }
      ],
      scaleTargetRef: {
        apiVersion: 'apps/v1',
        kind: 'Deployment',
        name: appName
      }
    }
  }

  await applyConfig(`hpa-${app}`, horizontalPodAutoscaler)
}

async function applyMediaServer() {
  const app = 'media'
  const appName = `${app}-${ENVIRONMENT_NAME}`
  const appPort = 4100

  const pvc = {
    apiVersion: 'v1',
    kind: 'PersistentVolumeClaim',
    metadata: {
      name: 'juanita-media',
      namespace: NAMESPACE
    },
    spec: {
      accessModes: ['ReadWriteOnce'],
      resources: {
        requests: {
          storage: '100Gi'
        }
      }
    }
  }

  await applyConfig(`pvc-${app}`, pvc)

  if (ENVIRONMENT_NAME === 'development') {
    const pv = {
      apiVersion: 'v1',
      kind: 'PersistentVolume',
      metadata: {
        namespace: NAMESPACE,
        name: 'juanita-media-cloned-from-prod'
      },
      spec: {
        storageClassName: 'standard',
        capacity: {
          storage: '100Gi'
        },
        accessModes: ['ReadWriteOnce'],
        claimRef: {
          namespace: NAMESPACE,
          name: 'juanita-media'
        },
        gcePersistentDisk: {
          pdName: 'juanita-prod-media-clone-20220628',
          fsType: 'ext4'
        }
      }
    }

    await applyConfig(`pv-${app}`, pv)
  }

  const deployment = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      replicas: 1,
      strategy: {
        type: 'Recreate'
        /* rollingUpdate: {
          maxSurge: 1,
          maxUnavailable: 0
        },
        type: 'RollingUpdate' */
      },
      selector: {
        matchLabels: {
          app: app,
          release: ENVIRONMENT_NAME
        }
      },
      template: {
        metadata: {
          name: appName,
          labels: {
            app: app,
            release: ENVIRONMENT_NAME
          }
        },
        spec: {
          containers: [
            {
              name: appName,
              image: `ghcr.io/wepublish/karma-media-server:next`,
              env: [
                {
                  name: 'NODE_ENV',
                  value: `production`
                },
                {
                  name: 'STORAGE_PATH',
                  value: '/home/node/.media'
                },
                {
                  name: 'NUM_CLUSTERS',
                  value: '1'
                },
                {
                  name: 'TOKEN',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'juanita-secrets',
                      key: 'media_server_token'
                    }
                  }
                }
              ],
              ports: [
                {
                  containerPort: appPort,
                  protocol: 'TCP'
                }
              ],
              imagePullPolicy: 'IfNotPresent',
              resources: {
                requests: {
                  cpu: envSwitch(ENVIRONMENT_NAME, '20m', '10m'),
                  memory: envSwitch(ENVIRONMENT_NAME, '512Mi', '128Mi')
                }
              },
              terminationMessagePath: '/dev/termination-log',
              terminationMessagePolicy: 'File',
              volumeMounts: [
                {
                  name: 'media-volume',
                  mountPath: '/home/node/.media'
                }
              ],
              readinessProbe: {
                httpGet: {
                  path: '/_healthz',
                  port: appPort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                successThreshold: 1,
                timeoutSeconds: 1
              },
              livenessProbe: {
                httpGet: {
                  path: '/_healthz',
                  port: appPort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                periodSeconds: 60,
                successThreshold: 1,
                timeoutSeconds: 1
              }
            }
          ],
          dnsPolicy: 'ClusterFirst',
          restartPolicy: 'Always',
          schedulerName: 'default-scheduler',
          terminationGracePeriodSeconds: 30,
          securityContext: {
            fsGroup: 1000
          },
          volumes: [
            {
              name: 'media-volume',
              persistentVolumeClaim: {
                claimName: 'juanita-media'
              }
            }
          ]
        }
      }
    }
  }

  await applyConfig(`deployment-${app}`, deployment)

  const backendConfig = {
    apiVersion: 'cloud.google.com/v1',
    kind: 'BackendConfig',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      cdn: {
        enabled: true,
        cachePolicy: {
          includeHost: true,
          includeProtocol: true,
          includeQueryString: true
        }
      }
    }
  }

  await applyConfig(`backendconfig-${app}`, backendConfig)

  const service = {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      annotations: {
        'cloud.google.com/backend-config': `{"ports": {"${appPort}":"${appName}"}}`
      }
    },
    spec: {
      ports: [
        {
          name: 'http',
          port: appPort,
          protocol: 'TCP',
          targetPort: appPort
        }
      ],
      selector: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      type: 'NodePort'
    }
  }
  await applyConfig(`service-${app}`, service)

  const managedCert = {
    apiVersion: 'networking.gke.io/v1',
    kind: 'ManagedCertificate',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      domains: [domainCachedMedia]
    }
  }

  await applyConfig(`managedCert-${app}`, managedCert)

  let ingress = {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      annotations: {
        'nginx.ingress.kubernetes.io/ssl-redirect': 'true',
        'nginx.ingress.kubernetes.io/proxy-body-size': '20m',
        'nginx.ingress.kubernetes.io/proxy-read-timeout': '30',
        'cert-manager.io/cluster-issuer': 'letsencrypt-production'
      }
    },
    spec: {
      ingressClassName: 'nginx',
      rules: [
        {
          host: domainMedia,
          http: {
            paths: [
              {
                backend: {
                  service: {
                    name: appName,
                    port: {
                      number: appPort
                    }
                  }
                },
                pathType: 'Prefix',
                path: '/'
              }
            ]
          }
        }
      ],
      tls: [
        {
          hosts: [domainMedia],
          secretName: `${appName}-tls`
        }
      ]
    }
  }
  await applyConfig(`ingress-${app}`, ingress)

  let cacheIngress = {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: `cached-${appName}`,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      annotations: {
        'kubernetes.io/ingress.class': 'gce',
        'networking.gke.io/managed-certificates': appName,
        'kubernetes.io/ingress.global-static-ip-name': `static-media-ip-${ENVIRONMENT_NAME}`
      }
    },
    spec: {
      //ingressClassName: 'gce',
      defaultBackend: {
        service: {
          name: appName,
          port: {
            number: appPort
          }
        }
      }
    }
  }
  await applyConfig(`ingress-cached-${app}`, cacheIngress)
}

async function applyApiServer() {
  const app = 'api'
  const appName = `${app}-${ENVIRONMENT_NAME}`
  const appPort = 4000

  const apiENV = [
    {
      name: 'NODE_ENV',
      value: `production`
    },
    {
      name: 'HOST_ENV',
      value: envSwitch(ENVIRONMENT_NAME, 'production', 'development')
    },
    {
      name: 'GOOGLE_APPLICATION_CREDENTIALS',
      value: '/var/secrets/google/key.json'
    },
    {
      name: 'MEDIA_SERVER_URL',
      value: `https://${domainMedia}`
    },
    {
      name: 'MEDIA_SERVER_CACHED_URL',
      value: `https://${domainCachedMedia}`
    },
    {
      name: 'MEDIA_SERVER_USE_CACHED',
      value: 'true'
    },
    {
      name: 'MEDIA_SERVER_INTERNAL_URL',
      value: `http://media-${ENVIRONMENT_NAME}.${NAMESPACE}.svc.cluster.local:4100`
    },
    {
      name: 'MEDIA_ADDRESS',
      value: `${domainMedia}`
    },
    {
      name: 'MEDIA_PORT',
      value: '443'
    },
    {
      name: 'MONGO_URL',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'mongo_url'
        }
      }
      /*...envSwitch(
        ENVIRONMENT_NAME,
        {
          valueFrom: {
            secretKeyRef: {
              name: 'juanita-secrets',
              key: 'mongo_url'
            }
          }
        },
        {value: `mongodb://mongo-${ENVIRONMENT_NAME}:27017/juanita`}
      ) */
    },
    {
      name: 'MONGO_LOCALE',
      value: 'de'
    },

    {
      name: 'HOST_URL',
      value: `https://${domainAPI}`
    },
    {
      name: 'WEBSITE_URL',
      value: envSwitch(ENVIRONMENT_NAME, `https://${domain}`, `https://${devDomain}`)
    },
    {
      name: 'MEDIA_SERVER_TOKEN',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'media_server_token'
        }
      }
    },
    {
      name: 'OAUTH_GOOGLE_DISCOVERY_URL',
      value: 'https://accounts.google.com'
    },
    {
      name: 'OAUTH_GOOGLE_CLIENT_ID',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'oauth_google_client_id'
        }
      }
    },
    {
      name: 'OAUTH_GOOGLE_CLIENT_KEY',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'oauth_google_client_key'
        }
      }
    },
    {
      name: 'OAUTH_GOOGLE_REDIRECT_URL',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'oauth_google_redirect_url'
        }
      }
    },
    {
      name: 'TSRI_AGENDA_API_URL',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'tsri_agenda_api_url'
        }
      }
    },
    {
      name: 'TSRI_AGENDA_API_USER',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'tsri_agenda_api_user'
        }
      }
    },
    {
      name: 'TSRI_AGENDA_API_PASS',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'tsri_agenda_api_pass'
        }
      }
    },
    {
      name: 'MAILGUN_API_KEY',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'mailgun_api_key'
        }
      }
    },
    {
      name: 'MAILGUN_BASE_DOMAIN',
      value: 'api.eu.mailgun.net'
    },
    {
      name: 'MAILGUN_MAIL_DOMAIN',
      value: envSwitch(ENVIRONMENT_NAME, 'mg.tsrimails.ch', 'mg.dev.tsrimails.ch')
    },
    {
      name: 'MAILGUN_WEBHOOK_SECRET',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'mailgun_webhook_secret'
        }
      }
    },
    {
      name: 'GDRIVE_DRIVE_ID',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'google_drive_id'
        }
      }
    },
    {
      name: 'JWT_SECRET_KEY',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'jwt_secret_key'
        }
      }
    },
    {
      name: 'STRIPE_SECRET_KEY',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'stripe_secret_key'
        }
      }
    },
    {
      name: 'STRIPE_WEBHOOK_SECRET',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'stripe_webhook_secret'
        }
      }
    },
    {
      name: 'PAYREXX_INSTANCE_NAME',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'payrexx_instance_name'
        }
      }
    },
    {
      name: 'PAYREXX_API_SECRET',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'payrexx_api_secret'
        }
      }
    },
    {
      name: 'SENTRY_DSN',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'sentry_dsn'
        }
      }
    },
    {
      name: 'SENTRY_ENV',
      value: envSwitch(ENVIRONMENT_NAME, 'production', 'staging')
    },
    {
      name: 'GOOGLE_PROJECT',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'google_project'
        }
      }
    },
    {
      name: 'PGHOST',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-migration-secrets',
          key: 'pghost'
        }
      }
    },
    {
      name: 'PGUSER',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-migration-secrets',
          key: 'pguser'
        }
      }
    },
    {
      name: 'PGDATABASE',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-migration-secrets',
          key: 'pgdatabase'
        }
      }
    },
    {
      name: 'PGPASSWORD',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-migration-secrets',
          key: 'pgpassword'
        }
      }
    },
    {
      name: 'PGPORT',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-migration-secrets',
          key: 'pgport'
        }
      }
    },
    {
      name: 'INVOICE_REMINDER_FREQ',
      value: '3'
    },
    {
      name: 'INVOICE_REMINDER_MAX_TRIES',
      value: '5'
    },
    {
      name: 'ENABLE_ANONYMOUS_COMMENTS',
      value: 'false'
    },
    {
      name: 'ALGEBRAIC_CAPTCHA_CHALLENGE',
      valueFrom: {
        secretKeyRef: {
          name: 'juanita-secrets',
          key: 'algebraic_captcha_challenge'
        }
      }
    }
  ]

  const deployment = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: app,
          release: ENVIRONMENT_NAME
        }
      },
      strategy: {
        rollingUpdate: {
          maxSurge: 1,
          maxUnavailable: 0
        },
        type: 'RollingUpdate'
      },
      template: {
        metadata: {
          name: appName,
          labels: {
            app: app,
            release: ENVIRONMENT_NAME
          }
        },
        spec: {
          volumes: [
            {
              name: 'google-cloud-key',
              secret: {
                secretName: 'log-the-things'
              }
            },
            {
              name: 'google-drive-key',
              secret: {
                secretName: 'maillog-uploader'
              }
            }
          ],
          containers: [
            {
              name: appName,
              image: `${CI_REGISTRY_IMAGE}/api:${SHA_OR_TAG}`,
              volumeMounts: [
                {
                  name: 'google-cloud-key',
                  mountPath: '/var/secrets/google'
                },
                {
                  name: 'google-drive-key',
                  mountPath: '/var/secrets/drive'
                }
              ],
              env: apiENV,
              ports: [
                {
                  containerPort: appPort,
                  protocol: 'TCP'
                }
              ],
              imagePullPolicy: 'IfNotPresent',
              resources: {
                requests: {
                  cpu: envSwitch(ENVIRONMENT_NAME, '600m', '500m'),
                  memory: envSwitch(ENVIRONMENT_NAME, '256Mi', '200Mi')
                }
              },
              terminationMessagePath: '/dev/termination-log',
              terminationMessagePolicy: 'File',
              readinessProbe: {
                exec: {
                  command: ['node', './dist/index.js', '_healthz']
                },
                initialDelaySeconds: 5,
                successThreshold: 1,
                timeoutSeconds: 10
              },
              livenessProbe: {
                exec: {
                  command: ['node', './dist/index.js', '_healthz']
                },
                initialDelaySeconds: 5,
                periodSeconds: 60,
                successThreshold: 1,
                timeoutSeconds: 10
              }
            }
          ]
        }
      }
    }
  }
  await applyConfig(`deployment-${app}`, deployment)

  const service = {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: appName,
      namespace: NAMESPACE
    },
    spec: {
      ports: [
        {
          name: 'http',
          port: appPort,
          protocol: 'TCP',
          targetPort: appPort
        }
      ],
      selector: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      type: 'ClusterIP'
    }
  }
  await applyConfig(`service-${app}`, service)

  let ingress = {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      annotations: {
        'kubernetes.io/ingress.class': 'nginx',
        'nginx.ingress.kubernetes.io/ssl-redirect': 'true',
        'nginx.ingress.kubernetes.io/proxy-body-size': '10m',
        'nginx.ingress.kubernetes.io/proxy-read-timeout': '30',
        'cert-manager.io/cluster-issuer': 'letsencrypt-production',
        'nginx.ingress.kubernetes.io/affinity': 'cookie',
        'nginx.ingress.kubernetes.io/session-cookie-expires': '172800',
        'nginx.ingress.kubernetes.io/session-cookie-max-age': '172800',
        'nginx.ingress.kubernetes.io/session-cookie-name': 'stickyCookieSession'
      }
    },
    spec: {
      ingressClassName: 'nginx',
      rules: [
        {
          host: domainAPI,
          http: {
            paths: [
              {
                backend: {
                  service: {
                    name: appName,
                    port: {
                      number: appPort
                    }
                  }
                },
                pathType: 'Prefix',
                path: '/'
              }
            ]
          }
        }
      ],
      tls: [
        {
          hosts: [domainAPI],
          secretName: `${appName}-tls`
        }
      ]
    }
  }
  await applyConfig(`ingress-${app}`, ingress)

  function createCronJob(jobName, args, schedule) {
    return {
      apiVersion: 'batch/v1',
      kind: 'CronJob',
      metadata: {
        name: `${appName}-${jobName}`,
        namespace: NAMESPACE,
        labels: {
          app: `${app}-cron`,
          release: ENVIRONMENT_NAME
        }
      },
      spec: {
        schedule,
        jobTemplate: {
          spec: {
            template: {
              metadata: {
                name: `${appName}-${jobName}`,
                labels: {
                  app: `${app}-cron`,
                  release: ENVIRONMENT_NAME
                }
              },
              spec: {
                volumes: [
                  {
                    name: 'google-cloud-key',
                    secret: {
                      secretName: 'log-the-things'
                    }
                  },
                  {
                    name: 'google-drive-key',
                    secret: {
                      secretName: 'maillog-uploader'
                    }
                  }
                ],
                containers: [
                  {
                    name: `${appName}-${jobName}`,
                    image: `${CI_REGISTRY_IMAGE}/api:${SHA_OR_TAG}`,
                    volumeMounts: [
                      {
                        name: 'google-cloud-key',
                        mountPath: '/var/secrets/google'
                      },
                      {
                        name: 'google-drive-key',
                        mountPath: '/var/secrets/drive'
                      }
                    ],
                    args,
                    env: apiENV
                  }
                ],
                restartPolicy: 'Never'
              }
            }
          }
        }
      }
    }
  }

  if (ENVIRONMENT_NAME === 'production') {
    await applyConfig(
      `cronjob-weekly_newsletter-${app}`,
      createCronJob(
        'weekly-agenda',
        ['node', './dist/index.js', 'send-weekly-newsletter'],
        '50 12 * * 3'
      )
    )

    await applyConfig(
      `cronjob-daily-maillog-analytics-${app}`,
      createCronJob(
        'daily-maillog-analytics',
        ['node', './dist/index.js', 'maillog-analytics'],
        '50 2 * * *'
      )
    )

    await applyConfig(
      `cronjob-daily-check-invoice-${app}`,
      createCronJob(
        'daily-check-invoice',
        ['node', './dist/index.js', 'check-open-invoices'],
        '0 3 * * *'
      )
    )

    await applyConfig(
      `cronjob-daily-membership-renewal-${app}`,
      createCronJob(
        'daily-membership-renewal',
        ['node', './dist/index.js', 'renew-memberships'],
        '10 3 * * *'
      )
    )

    await applyConfig(
      `cronjob-daily-charge-invoice-${app}`,
      createCronJob(
        'daily-charge-invoice',
        ['node', './dist/index.js', 'charge-open-invoices'],
        '20 3 * * *'
      )
    )

    await applyConfig(
      `cronjob-daily-remind-open-invoices-${app}`,
      createCronJob(
        'daily-remind-open-invoices',
        ['node', './dist/index.js', 'remind-open-invoices'],
        '30 3 * * *'
      )
    )

    await applyConfig(
      `cronjob-daily-remind-expired-subscription-${app}`,
      createCronJob(
        'daily-remind-expired-subscription',
        ['node', './dist/index.js', 'remind-expired-subscription'],
        '35 3 * * *'
      )
    )
  }
}

async function applyEditor() {
  const app = 'editor'
  const appName = `${app}-${ENVIRONMENT_NAME}`
  const appPort = 3000

  const deployment = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: app,
          release: ENVIRONMENT_NAME
        }
      },
      strategy: {
        rollingUpdate: {
          maxSurge: 1,
          maxUnavailable: 0
        },
        type: 'RollingUpdate'
      },
      template: {
        metadata: {
          name: appName,
          labels: {
            app: app,
            release: ENVIRONMENT_NAME
          }
        },
        spec: {
          containers: [
            {
              name: appName,
              image: `${CI_REGISTRY_IMAGE}/editor:${SHA_OR_TAG}`,
              env: [
                {
                  name: 'NODE_ENV',
                  value: `production`
                },
                {
                  name: 'PEER_BY_DEFAULT',
                  value: 'true'
                },
                {
                  name: 'API_URL',
                  value: `https://${domainAPI}`
                }
              ],
              ports: [
                {
                  containerPort: appPort,
                  protocol: 'TCP'
                }
              ],
              imagePullPolicy: 'IfNotPresent',
              resources: {
                requests: {
                  cpu: envSwitch(ENVIRONMENT_NAME, '20m', '10m'),
                  memory: envSwitch(ENVIRONMENT_NAME, '100Mi', '100Mi')
                }
              },
              terminationMessagePath: '/dev/termination-log',
              terminationMessagePolicy: 'File',
              readinessProbe: {
                httpGet: {
                  path: '/',
                  port: appPort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                successThreshold: 1,
                timeoutSeconds: 1
              },
              livenessProbe: {
                httpGet: {
                  path: '/',
                  port: appPort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                periodSeconds: 60,
                successThreshold: 1,
                timeoutSeconds: 1
              }
            }
          ],
          dnsPolicy: 'ClusterFirst',
          restartPolicy: 'Always',
          schedulerName: 'default-scheduler',
          terminationGracePeriodSeconds: 30
        }
      }
    }
  }
  await applyConfig(`deployment-${app}`, deployment)

  const service = {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: appName,
      namespace: NAMESPACE
    },
    spec: {
      ports: [
        {
          name: 'http',
          port: appPort,
          protocol: 'TCP',
          targetPort: appPort
        }
      ],
      selector: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      type: 'ClusterIP'
    }
  }
  await applyConfig(`service-${app}`, service)

  let ingress = {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      annotations: {
        'kubernetes.io/ingress.class': 'nginx',
        'nginx.ingress.kubernetes.io/ssl-redirect': 'true',
        'nginx.ingress.kubernetes.io/proxy-body-size': '20m',
        'nginx.ingress.kubernetes.io/proxy-read-timeout': '30',
        'cert-manager.io/cluster-issuer': 'letsencrypt-production'
      }
    },
    spec: {
      ingressClassName: 'nginx',
      rules: [
        {
          host: domainEditor,
          http: {
            paths: [
              {
                backend: {
                  service: {
                    name: appName,
                    port: {
                      number: appPort
                    }
                  }
                },
                pathType: 'Prefix',
                path: '/'
              }
            ]
          }
        }
      ],
      tls: [
        {
          hosts: [domainEditor],
          secretName: `${appName}-tls`
        }
      ]
    }
  }
  await applyConfig(`ingress-${app}`, ingress)
}

async function applyDjango() {
  const servicePort = 8000
  const app = 'django'
  const appName = `${app}-${ENVIRONMENT_NAME}`

  const service = {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: appName,
      namespace: NAMESPACE
    },
    spec: {
      ports: [
        {
          name: 'http',
          port: servicePort,
          protocol: 'TCP',
          targetPort: servicePort
        }
      ],
      selector: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      sessionAffinity: 'None',
      type: 'ClusterIP'
    }
  }
  await applyConfig(`service-${app}`, service)

  const image = `registry.gitlab.com/tsri/tsri.ch:66539453`

  function getEnvs() {
    return [
      {
        name: 'DATABASE_URL',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'dburl'
          }
        }
      },
      {
        name: 'CACHE_URL',
        value: `hiredis://redis-master.redis.svc.cluster.local:6379/1/?key_prefix=${envSwitch(
          ENVIRONMENT_NAME,
          'tsri_prod',
          'tsri_staging'
        )}'`
      },
      {
        name: 'EMAIL_URL',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'email_url'
          }
        }
      },
      {
        name: 'SECRET_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'secret_key'
          }
        }
      },
      {
        name: 'SENTRY_DSN',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'sentry_dsn'
          }
        }
      },
      {
        name: 'SENTRY_DSN_REACT_JOIN',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'sentry_dsn_react_join'
          }
        }
      },
      {
        name: 'ALLOWED_HOSTS',
        value: envSwitch(
          ENVIRONMENT_NAME,
          '["tsri.ch", ".tsri.ch", "xn--tsri-1ra.ch", ".xn--tsri-1ra.ch", "prod.gifstr.io"]',
          '["tsri.dev", ".tsri.dev"]'
        )
      },
      {
        name: 'ALLOWED_CORS_ORIGINS',
        value: envSwitch(
          ENVIRONMENT_NAME,
          '["https://tsri.ch",]',
          '["https://tsri.dev", "http://localhost:5000"]'
        )
      },
      {
        name: 'SENTRY_RELEASE',
        value: SHA_OR_TAG
      },
      {
        name: 'PUSH_NOTIFICATION_API_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'push_notification_api_key'
          }
        }
      },
      {
        name: 'MAILGUN_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'mailgun_key'
          }
        }
      },
      {
        name: 'MAILGUN_URL',
        value: 'https://api.eu.mailgun.net/v3'
      },
      {
        name: 'MAILGUN_DOMAIN',
        value: 'mg.tsrimails.ch'
      },
      {
        name: 'MAILCHIMP_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'mailchimp_key'
          }
        }
      },
      {
        name: 'MAILCHIMP_USER',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'mailchimp_user'
          }
        }
      },
      {
        name: 'PAYREXX_API_SECRET',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'payrexx_api_secret'
          }
        }
      },
      {
        name: 'PAYREXX_INSTANCE_NAME',
        value: envSwitch(ENVIRONMENT_NAME, 'tsri', 'tsridev')
      },
      {
        name: 'GOOGLE_CLIENT_ID',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'google_client_id'
          }
        }
      },
      {
        name: 'GOOGLE_CLIENT_SECRET',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'google_client_secret'
          }
        }
      },
      {
        name: 'GOOGLE_ANALYTICS_REACT_JOIN',
        value: 'UA-53750015-2'
      },
      {
        name: 'LIVE',
        value: envSwitch(ENVIRONMENT_NAME, 'True', 'False')
      },
      {
        name: 'CANONICAL_DOMAIN',
        value: domainCn
      },
      {
        name: 'CANONICAL_DOMAIN_SECURE',
        value: 'True'
      },
      {
        name: 'STRIPE_PUBLISHABLE_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'stripe_publishable_key'
          }
        }
      },
      {
        name: 'STRIPE_SECRET_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'stripe_secret_key'
          }
        }
      },
      {
        name: 'POSTFINANCE_PSPID',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'postfinance_pspid'
          }
        }
      },
      {
        name: 'POSTFINANCE_LIVE',
        value: envSwitch(ENVIRONMENT_NAME, 'True', 'False')
      },
      {
        name: 'POSTFINANCE_SHA1_IN',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'postfinance_sha1_in'
          }
        }
      },
      {
        name: 'POSTFINANCE_SHA1_OUT',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'postfinance_sha1_out'
          }
        }
      },
      {
        name: 'FACEBOOK_CLIENT_ID',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'facebook_client_id'
          }
        }
      },
      {
        name: 'FACEBOOK_CLIENT_SECRET',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'facebook_client_secret'
          }
        }
      },
      {
        name: 'TWITTER_CLIENT_ID',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'twitter_client_id'
          }
        }
      },
      {
        name: 'TWITTER_CLIENT_SECRET',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'twitter_client_secret'
          }
        }
      },
      {
        name: 'AWS_ACCESS_KEY_ID',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'aws_access_key_id'
          }
        }
      },
      {
        name: 'AWS_SECRET_ACCESS_KEY',
        valueFrom: {
          secretKeyRef: {
            name: 'tsri-secrets',
            key: 'aws_secret_access_key'
          }
        }
      }
    ]
  }

  const deployment = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: app,
          release: ENVIRONMENT_NAME
        }
      },
      strategy: {
        rollingUpdate: {
          maxSurge: 1,
          maxUnavailable: 0
        },
        type: 'RollingUpdate'
      },
      template: {
        metadata: {
          name: appName,
          labels: {
            app: app,
            release: ENVIRONMENT_NAME
          }
        },
        spec: {
          containers: [
            {
              name: appName,
              image: image,
              env: getEnvs(),
              ports: [
                {
                  containerPort: servicePort,
                  protocol: 'TCP'
                }
              ],
              imagePullPolicy: 'IfNotPresent',
              resources: {
                requests: {
                  cpu: envSwitch(ENVIRONMENT_NAME, '20m', '20m'),
                  memory: envSwitch(ENVIRONMENT_NAME, '200Mi', '100Mi')
                }
              },
              readinessProbe: {
                httpGet: {
                  httpHeaders: [
                    {
                      name: 'Host',
                      value: domainCn
                    }
                  ],
                  path: '/admin',
                  port: servicePort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 5,
                successThreshold: 1,
                timeoutSeconds: envSwitch(ENVIRONMENT_NAME, 60, 60)
              },
              livenessProbe: {
                httpGet: {
                  httpHeaders: [
                    {
                      name: 'Host',
                      value: domainCn
                    }
                  ],
                  path: '/admin',
                  port: servicePort,
                  scheme: 'HTTP'
                },
                initialDelaySeconds: 60,
                periodSeconds: 60,
                successThreshold: 1,
                timeoutSeconds: envSwitch(ENVIRONMENT_NAME, 60, 60)
              }
            }
          ],
          imagePullSecrets: [
            {
              name: 'gitlab-registry'
            }
          ]
        }
      }
    }
  }
  await applyConfig(`deployment-${app}`, deployment)
}

async function applyMongo() {
  const app = 'mongo'
  const port = 27017
  const appName = `${app}-${ENVIRONMENT_NAME}`

  const pvc = {
    apiVersion: 'v1',
    kind: 'PersistentVolumeClaim',
    metadata: {
      name: `mongo-data`,
      namespace: NAMESPACE
    },
    spec: {
      accessModes: ['ReadWriteOnce'],
      resources: {
        requests: {
          storage: '2Gi'
        }
      }
    }
  }

  await applyConfig(`pvc-${app}`, pvc)

  const deployment = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: appName,
      namespace: NAMESPACE,
      labels: {
        app: app,
        release: ENVIRONMENT_NAME
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: app,
          release: ENVIRONMENT_NAME
        }
      },
      strategy: {
        rollingUpdate: {
          maxSurge: 1,
          maxUnavailable: 0
        },
        type: 'RollingUpdate'
      },
      template: {
        metadata: {
          name: appName,
          labels: {
            app: app,
            release: ENVIRONMENT_NAME
          }
        },
        spec: {
          containers: [
            {
              name: appName,
              image: 'mongo:4.2.3-bionic',
              env: [],
              ports: [
                {
                  containerPort: port,
                  protocol: 'TCP'
                }
              ],
              imagePullPolicy: 'IfNotPresent',
              resources: {
                requests: {
                  cpu: '0m',
                  memory: '128Mi'
                }
              },
              terminationMessagePath: '/dev/termination-log',
              terminationMessagePolicy: 'File',
              volumeMounts: [
                {
                  name: 'mongo-volume',
                  mountPath: '/data/db'
                }
              ]
            }
          ],
          dnsPolicy: 'ClusterFirst',
          restartPolicy: 'Always',
          schedulerName: 'default-scheduler',
          terminationGracePeriodSeconds: 30,
          volumes: [
            {
              name: 'mongo-volume',
              persistentVolumeClaim: {
                claimName: `mongo-data`
              }
            }
          ]
        }
      }
    }
  }
  await applyConfig(`deployment-${app}`, deployment)

  const service = {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: appName,
      namespace: NAMESPACE
    },
    spec: {
      ports: [
        {
          name: 'http',
          port: port,
          protocol: 'TCP',
          targetPort: port
        }
      ],
      selector: {
        app: app,
        release: ENVIRONMENT_NAME
      },
      type: 'ClusterIP'
    }
  }
  await applyConfig(`service-${app}`, service)
}

async function applyConfig(name, obj) {
  const configPath = 'kubernetesConfigs'
  try {
    await execCommand(`mkdir ${configPath}`)
  } catch (e) {}
  const filename = `./${configPath}/${name}.json`
  await writeFile(filename, obj)
}

function writeFile(filePath, json) {
  return new Promise(function(resolve, reject) {
    fs.writeFile(filePath, JSON.stringify(json, null, 2), function(error) {
      if (error) {
        return reject(error)
      } else {
        resolve(true)
      }
    })
  })
}

function execCommand(command) {
  return new Promise((resolve, reject) => {
    exec(command, function(error, stdout, stderr) {
      if (error) {
        reject(error)
      } else {
        resolve(stdout)
      }
    })
  })
}

function envSwitch(env, prod, staging) {
  return env === 'production' ? prod : staging
}
