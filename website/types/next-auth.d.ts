import NextAuth, {DefaultSession} from 'next-auth'
import {SubscriptionState} from '../src/utils/WepublishNextAuthAdapter'

declare module 'next-auth' {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: {
      /** The user's postal address. */
      subscriptionState: SubscriptionState
    } & DefaultSession['user']
  }
}
