This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Setup

You have to add the following environment variables in `.env`

```
ASSET_HOST=http://localhost:3001
NEXT_PUBLIC_API_URL=http://localhost:4000
NEXT_PUBLIC_WEBSITE_URL=http://localhost:5000
NEXT_INTERNAL_API_URL=http://localhost:4000
INTERNAL_API_URL=http://localhost:4000
IMAGE_DOMAIN_LIST=tsrich.s3.eu-central-1.amazonaws.com,localhost (strings separated with commas)
TSRI_AGENDA_API_URL=_PLACEHOLDER_
TSRI_AGENDA_API_USER=_PLACEHOLDER_
TSRI_AGENDA_API_PASS=_PLACEHOLDER_
```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
