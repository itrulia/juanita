#!/usr/bin/env sh

if [ "$NEXT_PUBLIC_HOST_ENV" == "production" ]
then
  printf "User-agent: *\nAllow: /\n" > ./public/robots.txt
else
  printf "User-agent: *\nDisallow: /\n" > ./public/robots.txt
fi
