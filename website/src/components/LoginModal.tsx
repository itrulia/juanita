import React, {useEffect, useRef, useState} from 'react'
import Link from 'next/link'
import {getProviders, signIn, signOut, useSession} from 'next-auth/react'
import {useAppContext} from '../utils/AppContext.'
import {useTranslation} from 'next-i18next'
import * as Sentry from '@sentry/nextjs'
import {useRouter} from 'next/router'
import {toast} from 'react-toastify'
import {useMatomo} from '@datapunt/matomo-tracker-react'
import {TrackingEventCategories} from './utility'
import {ChooseRightModal} from './Modal'

export function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

function LoginModal() {
  const {t} = useTranslation('common')
  const {state: appState, update: updateAppState} = useAppContext()
  const router = useRouter()
  const {trackEvent} = useMatomo()
  const [loginProviders, setLoginProviders] = useState({})
  const [emailAddress, setEmailAddress] = useState<string>('')
  const [emailAddressError, setEmailAddressError] = useState<string | null>(null)
  const [emailLinkSent, setEmailLinkSent] = useState<boolean>(false)
  const [errorMessage, setErrorMessage] = useState<string | null>(null)

  useEffect(() => {
    getProviders().then(data => setLoginProviders(data))
  }, [])

  const [subscriptionNeedsAction, setSubscriptionNeedsAction] = useState(false)
  const {data: session} = useSession()

  const closeButtonRef = useRef(null)

  useEffect(() => {
    if (session) {
      // @ts-ignore
      setSubscriptionNeedsAction(!session.user.subscriptionState.isActive)
    }
  }, [session])

  function handlingClose() {
    trackEvent({
      category: TrackingEventCategories.Authentication,
      action: 'close',
      name: 'login-modal'
    })
    setEmailLinkSent(false)
    setErrorMessage(null)
    setEmailAddress('')
    updateAppState({...appState, showLoginModal: false})
  }

  useEffect(() => {
    if (router.query.showLogin !== undefined) {
      updateAppState({...appState, showLoginModal: true})
      if (router.query.errorCode) {
        console.log('error message', router.query.errorCode)
        switch (router.query.errorCode) {
          case 'OAuthAccountNotLinked':
            setErrorMessage(t('loginModal.oauthNotLinked'))
            break
          case 'OAuthUserNotExist':
            setErrorMessage(t('loginModal.oauthNotExist'))
            break
          case 'Verification':
            setErrorMessage(t('loginModal.verificationError'))
            break
          default:
            setErrorMessage(t('loginModal.defaultError'))
        }
      }
      router.push(window.location.pathname, undefined, {shallow: true})
    }
  }, [router.query])

  async function handlingMagicLink() {
    try {
      if (validateEmail(emailAddress)) {
        trackEvent({
          category: TrackingEventCategories.Authentication,
          action: 'send-login-link',
          name: 'login-modal'
        })
        await signIn('email', {email: emailAddress, redirect: false})
        toast.success(t('loginModal.linkSentTo', {email: emailAddress}))
        setEmailAddressError(null)
        setEmailLinkSent(true)
      } else {
        setEmailAddressError(t('loginModal.loginWithMailInvalidEMail'))
      }
    } catch (error) {
      Sentry.captureException(error)
      setEmailAddressError(t('loginModal.loginWithMailError'))
    }
  }

  return (
    <ChooseRightModal
      show={appState.showLoginModal}
      showTopClose={true}
      allowBackgroundClose={false}
      onClose={() => handlingClose()}>
      {!session && (
        <>
          <h3 className="text-lg leading-6 font-medium text-gray-900">
            {t('loginModal.welcomeBack')}
          </h3>
          <div className="mt-2 grid grid-cols-1 justify-items-center">
            {!errorMessage && <p>{t('loginModal.loginWithTsri')}</p>}
            {errorMessage && <p className="text-error">{errorMessage}</p>}
            {loginProviders.hasOwnProperty('email') && (
              <>
                {!emailLinkSent && (
                  <div className="mt-4 ml-auto mr-auto sm:w-80">
                    {emailAddressError && <p className="text-error">{emailAddressError}</p>}
                    <div className="relative text-gray-700">
                      <input
                        value={emailAddress}
                        onChange={event => {
                          setEmailAddressError(null)
                          setEmailAddress(event.target.value)
                        }}
                        onKeyPress={event => {
                          if (event.key === 'Enter') {
                            handlingMagicLink()
                          }
                        }}
                        className="w-1/2 float-left h-10 pl-3 pr-4 text-base placeholder-gray-600 border focus:shadow-outline"
                        type="email"
                        placeholder={t('loginModal.loginWithMailPlaceholder')}
                      />
                      <button
                        onClick={() => handlingMagicLink()}
                        className="w-1/2 inset-y-0 right-0 flex items-center px-4 text-white bg-tsri h-10 hover:bg-indigo-500 focus:bg-indigo-700">
                        {t('loginModal.loginWithMail')}
                      </button>
                    </div>
                  </div>
                )}
                {emailLinkSent && (
                  <div className="mt-4 ml-auto mr-auto sm:w-80">
                    <button
                      disabled={true}
                      className="inset-y-0 h-10 w-full cursor-default px-4 text-center text-white bg-tsri">
                      {t('loginModal.linkSent')}
                    </button>
                  </div>
                )}
              </>
            )}
            {loginProviders.hasOwnProperty('google') && (
              <>
                <button
                  onClick={() => {
                    trackEvent({
                      category: TrackingEventCategories.Authentication,
                      action: 'login-google',
                      name: 'login-modal'
                    })
                    signIn('google', {})
                  }}
                  className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full sm:w-80">
                  {t('loginModal.loginWithGoogle')}
                </button>
              </>
            )}
            {loginProviders.hasOwnProperty('facebook') && (
              <>
                <button
                  onClick={() => {
                    trackEvent({
                      category: TrackingEventCategories.Authentication,
                      action: 'login-facebook',
                      name: 'login-modal'
                    })
                    signIn('facebook')
                  }}
                  className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full sm:w-80">
                  {t('loginModal.loginWithFacebook')}
                </button>
              </>
            )}
            {loginProviders.hasOwnProperty('twitter') && (
              <>
                <button
                  onClick={() => {
                    trackEvent({
                      category: TrackingEventCategories.Authentication,
                      action: 'login-twitter',
                      name: 'login-modal'
                    })
                    signIn('twitter')
                  }}
                  className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full sm:w-80">
                  {t('loginModal.loginWithTwitter')}
                </button>
              </>
            )}
            <p className="mt-10">
              {t('loginModal.notYetAMember')}&nbsp;
              <span className="text-tsri">
                <Link href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`}>
                  {t('loginModal.becomeMember')}
                </Link>
              </span>
            </p>
          </div>
        </>
      )}
      {session && (
        <>
          <h3 className="text-lg leading-6 font-medium text-gray-900">
            {t('loginModal.hello', {userName: session.user.name})}
          </h3>
          <div className="mt-2 grid grid-cols-1 justify-items-center">
            {subscriptionNeedsAction && (
              <div
                onClick={() => {
                  router.push('/account/subscription')
                  handlingClose()
                }}
                className="p-4 mt-4 w-full text-sm bg-red rounded-lg cursor-pointer"
                role="alert">
                <span className="font-medium">{t('loginModal.attention')}</span>{' '}
                {t('loginModal.subscriptionNeedsYourAttention')}
              </div>
            )}
            <button
              onClick={() => {
                router.push('/account/profile')
                handlingClose()
              }}
              className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full">
              Profil
            </button>
            <button
              onClick={() => {
                trackEvent({
                  category: TrackingEventCategories.Authentication,
                  action: 'logout',
                  name: 'login-modal'
                })
                signOut({callbackUrl: window.location.origin})
              }}
              className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full">
              Ausloggen
            </button>
          </div>
        </>
      )}
    </ChooseRightModal>
  )
}

export default LoginModal
