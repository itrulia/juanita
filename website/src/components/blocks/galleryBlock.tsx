import React, {useEffect, useState} from 'react'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import {Carousel} from 'react-responsive-carousel'
import {ImageData} from '../types'

export interface GalleryBlockProps {
  readonly media: ImageData[]
  readonly loop?: boolean
}

export function GalleryBlock({media, loop}: GalleryBlockProps) {
  const [maxHeight, setMaxHeiht] = useState<number>(600)

  useEffect(() => {
    setMaxHeiht(globalThis.innerWidth < 640 ? 300 : 600)
  }, [])

  return (
    <Carousel
      className="mb-6"
      animationHandler="slide"
      axis="horizontal"
      width="100%"
      autoPlay={false}
      showArrows={true}
      dynamicHeight={false}
      showThumbs={false}
      showStatus={false}
      infiniteLoop={loop ?? true}
      showIndicators={false}>
      {media.map((media, index) => (
        <div key={index}>
          <img src={media.largeURL} style={{maxHeight: `${maxHeight}px`, objectFit: 'contain'}} />
          {media.caption ? <p className="legend">{media.caption}</p> : null}
        </div>
      ))}
    </Carousel>
  )
}
