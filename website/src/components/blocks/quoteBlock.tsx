import React from 'react'

export interface QuoteBlockProps {
  readonly text: string
  readonly author?: string
}

export function QuoteBlock({text, author}: QuoteBlockProps) {
  const ref = React.createRef<HTMLQuoteElement>()

  const isHTML: boolean = author === '__html'
  const html = {__html: text}

  return (
    <>
      {isHTML && <div className="w-full lg:w-3/4 mb-6" dangerouslySetInnerHTML={html}></div>}
      {!isHTML && (
        <blockquote className="px-10 w-full lg:w-3/4 mb-12 mt-12" ref={ref}>
          <p className="text-4xl italic mb-4">{text}</p>
          {author && <cite className="text-left block">{author}</cite>}
        </blockquote>
      )}
    </>
  )
}
