import React, {useContext, useMemo} from 'react'
import {FlexAlignment, FlexTeaserFragment, TeaserFragment} from '../../../@types/codegen/graphql'
import {Responsive, WidthProvider} from 'react-grid-layout'
import TeaserView from '../../atoms/teaser/teaserView'
import DailyBriefing from '../../atoms/teaser/customTeaser/dailyBriefing'
import styled, {ThemeContext, css} from 'styled-components'
import {GRID_COLUMNS, GRID_GAP, removePx} from '../../../../styles/helpers'
import SubscribBriefing from '../../atoms/teaser/customTeaser/subscribBriefing'

const GridFlexBoxContainer = styled.div`
  margin: 0 -${GRID_GAP}px;

  ${props => css`
    @media (min-width: ${props.theme.screens.sm}) {
      margin: 0 0;
    }

    @media (min-width: ${props.theme.screens.xl}) {
      margin: 0 -${GRID_GAP}px;
    }
  `}
`
const ResponsiveGridLayout = WidthProvider(Responsive)
interface GridFlexBlockProps {
  flexTeasers: FlexTeaserFragment[]
}
export default function GridFlexBlock({flexTeasers}: GridFlexBlockProps) {
  const flexTeaserPrefix = 'flexTeaserIndex-'
  const theme = useContext(ThemeContext)

  const breakpoints = {xs: 0, sm: removePx(theme.screens.sm), lg: removePx(theme.screens.lg)}
  const cols = {xs: GRID_COLUMNS, sm: GRID_COLUMNS, lg: GRID_COLUMNS}

  // generate auto responsive layout for each breakpoint
  const layouts = useMemo(() => {
    const layoutObject = {}
    // iterate breakpoints
    Object.keys(breakpoints).forEach(bpKey => {
      layoutObject[bpKey] = generateLayout(bpKey)
    })
    return layoutObject
  }, [flexTeasers])

  function generateLayout(breakpointKey: string) {
    const stack = []

    flexTeasers.forEach((flexTeaser, flexTeaserIndex) => {
      const {alignment} = flexTeaser
      stack.push({
        i: `${flexTeaserPrefix}${flexTeaserIndex}`,
        static: true,
        ...getAutoResponsiveAlignment(breakpointKey, stack, flexTeaserIndex, alignment)
      })
    })
    return stack
  }

  // since We.Publish doesn't provide breakpoints on the grid, create an automatically responsive grid.
  function getAutoResponsiveAlignment(
    breakpoint: string,
    stack: any[],
    index: number,
    alignment: FlexAlignment
  ) {
    const w = getAutoResponsiveWidth(breakpoint, alignment.w)
    const newAlignment = {
      ...alignment,
      w
    }

    // shorten height on primary element on mobile
    if (breakpoint === 'xs') {
      if (alignment.h >= 6) {
        newAlignment.h = 4
      }
    }

    if (breakpoint === 'xs' || breakpoint === 'sm') {
      // leave initial element on its original place
      if (index <= 0) {
        return newAlignment
      }
      const previousElement = stack[index - 1]
      const followingXPosition = previousElement.x + previousElement.w
      const sumUpHeight = Math.max(...stack.map(element => element.y)) + previousElement.h
      if (followingXPosition + w > GRID_COLUMNS) {
        // start new line
        newAlignment.x = 0
        newAlignment.y = sumUpHeight
        return newAlignment
      } else {
        // add on the left side of the current line
        newAlignment.x = followingXPosition
        newAlignment.y = previousElement.y
        return newAlignment
      }
    }
    // desktop: just return original element
    return newAlignment
  }

  // when mobile sets width automatically to full width
  function getAutoResponsiveWidth(breakpoint: string, width: number) {
    if (breakpoint === 'xs') {
      return GRID_COLUMNS
    } else if (breakpoint === 'sm') {
      if (width >= GRID_COLUMNS / 2) {
        return GRID_COLUMNS
      }
      return GRID_COLUMNS / 2
    }
    return width
  }

  function getTeaserType(teaser: TeaserFragment) {
    if (teaser?.preTitle === 'daily-briefing') {
      return <DailyBriefing teaser={teaser} />
    } else if (teaser?.preTitle === 'subscribe-briefing') {
      return <SubscribBriefing teaser={teaser} />
    } else {
      return <TeaserView teaser={teaser} />
    }
  }

  return (
    <GridFlexBoxContainer>
      <ResponsiveGridLayout
        className="layout"
        layouts={layouts}
        breakpoints={breakpoints}
        cols={cols}
        autoSize
        compactType="vertical"
        margin={[GRID_GAP, GRID_GAP]}>
        {flexTeasers.map((flexTeaser, flexTeaserIndex) => (
          <div key={`${flexTeaserPrefix}${flexTeaserIndex}`}>
            {getTeaserType(flexTeaser.teaser)}
          </div>
        ))}
      </ResponsiveGridLayout>
    </GridFlexBoxContainer>
  )
}
