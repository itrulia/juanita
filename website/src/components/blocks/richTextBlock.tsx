import React from 'react'
import {Node} from 'slate'
import {RichText} from '../atoms/RichText'
import {whenDesktop, whenTablet} from '../../../styles/deprecated_helpers'
import styled from 'styled-components'
import {Color} from '../../../styles/deprecated_colors'
import {pxToRem} from '../../../styles/helpers'

const BaseRichTextBlock = styled.div`
  line-height: 1.75rem;
  font-size: 1.25rem;

  strong {
    font-weight: bold;
  }

  italic {
    font-style: italic;
  }

  h2 {
    line-height: 1.2em;
    font-size: 1.6em;
    margin-top: 0;
    margin-bottom: 0.5em;
    font-weight: normal;
  }

  h3 {
    line-height: 1.2em;
    font-size: 1.3em;
    margin-top: 0;
    /* margin-bottom: 1em; */
    font-weight: normal;
  }

  p {
    line-height: 1.75rem;
    font-size: 1.25rem;
    margin-bottom: 1.5rem;
    margin-top: 0;
  }

  p a {
    color: ${Color.PrimaryLight};
    text-decoration: underline;
    transition: color 200ms ease;

    &:hover {
      color: ${Color.Black};
    }
  }
`

export interface RichTextBlockProps {
  readonly value: Node[]
}

export function RichTextBlock({value}: RichTextBlockProps) {
  const ref = React.createRef<HTMLParagraphElement>()

  return (
    <BaseRichTextBlock className="w-full lg:w-3/4" ref={ref}>
      <RichText value={value} />
    </BaseRichTextBlock>
  )
}
