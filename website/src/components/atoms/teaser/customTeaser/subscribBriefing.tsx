import React from 'react'
import {TeaserViewProps} from '../teaserView'
import styled, {css} from 'styled-components'
import {Color} from '../../../../../styles/deprecated_colors'
import Link from 'next/link'
import Button, {ButtonType} from '../../Button'

const SubscribeBriefingContainer = styled.div`
  height: 100%;
  background-color: ${Color.Primary};
  color: white;
  padding: 32px;
  text-align: center;
`

const SubscribeBriefingTitle = styled.div`
  font-weight: 700;
  font-size: 40px;
  line-height: 48px;
  text-transform: uppercase;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      font-size: 28px;
      line-height: 38px;
    }
  `}
`
const SubscribeBriefingLead = styled.div`
  font-weight: 500;
  font-size: 16px;
  line-height: 135%;

  ${props => css`
    @media (min-width: ${props.theme.screens.lg}) {
      font-weight: 400;
      font-size: 18px;
    }
  `}
`

export default function SubscribBriefing({teaser}: TeaserViewProps) {
  return (
    <SubscribeBriefingContainer>
      {teaser.title && <SubscribeBriefingTitle>{teaser.title}</SubscribeBriefingTitle>}
      {teaser.lead && (
        <SubscribeBriefingLead className={'pt-7'}>{teaser.lead}</SubscribeBriefingLead>
      )}
      <div className={'pt-7'}>
        <Link href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`}>
          <a>
            <Button className={'px-2 lg:px-2 text-base'} type={ButtonType.White}>
              Werde Tsüri Member
            </Button>
          </a>
        </Link>
      </div>
    </SubscribeBriefingContainer>
  )
}
