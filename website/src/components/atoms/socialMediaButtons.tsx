import React from 'react'
import {FacebookIcon, InstagramIcon, MailIcon, TwitterIcon, WebsiteIcon} from './icon'
import {AuthorLink} from '../../@types/codegen/graphql'

export interface SocialMediaButtonsProps {
  links: AuthorLink[]
}

export function SocialMediaButtons({links}: SocialMediaButtonsProps) {
  return (
    <div className="flex flex-row">
      {links?.map(({title, url}, i) => (
        <a
          className="w-8 h-8 mr-4 inline-flex justify-center rounded-full"
          key={i}
          rel="noreferrer"
          href={url}
          target="_blank">
          {title === 'Twitter' ? (
            <img src="/icons/twitter.png" alt="twitter logo" />
          ) : title === 'Facebook' ? (
            <img src="/icons/facebook.png" alt="facebook logo" />
          ) : title === 'Instagram' ? (
            <img src="/icons/instagram.png" alt="instagram logo" />
          ) : title === 'TrustJ' || url.includes('trust-j.org/') ? (
            <img src="/icons/trusj.png" alt="website" />
          ) : title === 'Email' ? (
            <img src="/icons/email.png" alt="email" />
          ) : (
            <img src="/icons/website.png" alt="website" />
          )}
        </a>
      ))}
    </div>
  )
}

export default SocialMediaButtons
