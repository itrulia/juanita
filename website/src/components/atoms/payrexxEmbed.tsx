import React, {useEffect, useRef} from 'react'
import {transformCssStringToObject} from '../utility'
import * as Sentry from '@sentry/nextjs'

export interface PayrexxEmbedProps {
  title?: string
  url?: string
  width?: number
  height?: number
  styleCustom?: string
}

export function PayrexxEmbed(data: PayrexxEmbedProps) {
  const refIframe = useRef<HTMLIFrameElement>(null)

  useEffect(() => {
    const listener = event => {
      if (typeof event.data === 'string') {
        try {
          const data = JSON.parse(event.data)
          if (data && data.payrexx && data.payrexx.height) {
            refIframe.current.style.height = `${parseInt(data.payrexx.height)}px`
          }
        } catch (e) {
          Sentry.captureException(e)
        }
      }
    }
    globalThis.addEventListener('message', listener)
    return () => globalThis.removeEventListener('message', listener)
  }, [])

  return (
    <div className="mb-6" style={{width: '100%'}}>
      <iframe
        ref={refIframe}
        style={{
          position: 'relative',
          top: 0,
          left: 0,
          border: 'none'
        }}
        frameBorder={0}
        scrolling="no"
        allowFullScreen
        title={data.title}
        src={data.url}
        width={`${data.width}%`}
        height={globalThis.innerWidth < 650 ? data.height + 110 : data.height}
        onLoad={() => {
          refIframe.current.contentWindow.postMessage(
            JSON.stringify({origin: window.location.origin}),
            data.url
          )
        }}
      />
    </div>
  )
}
