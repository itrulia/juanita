import React, {useEffect, useRef, createContext, ReactNode, useContext} from 'react'
import {TwitterTweetEmbedData} from '../types'

import {useScript} from '../utility'

// Define some globals set by the Twitter SDK.
declare global {
  interface Window {
    twttr: any
  }
}
export interface TwitterContextState {
  readonly isLoaded: boolean
  readonly isLoading: boolean

  load(): void
}

export const TwitterContext = createContext(null as TwitterContextState | null)

export interface TwitterProviderProps {
  children?: ReactNode
}

export function TwitterProvider({children}: TwitterProviderProps) {
  const contextValue = useScript('https://platform.twitter.com/widgets.js', () =>
    typeof window !== 'undefined' ? !!window.twttr : false
  )
  return <TwitterContext.Provider value={contextValue}>{children}</TwitterContext.Provider>
}

export function TwitterTweetEmbed({userID, tweetID}: TwitterTweetEmbedData) {
  const wrapperRef = useRef<HTMLDivElement | null>(null)
  const context = useContext(TwitterContext)

  if (!context) {
    throw new Error(
      `Coulnd't find TwitterContext, did you include TwitterProvider in the component tree?`
    )
  }

  const {isLoaded, isLoading, load} = context
  // const css = useStyle(isLoaded)

  useEffect(() => {
    if (isLoaded) {
      console.log('executing load')
      //window.twttr.widgets.createTweet(`${tweetID}`, wrapperRef.current)
      window.twttr.widgets.load(wrapperRef.current)
    } else if (!isLoading) {
      load()
    }
  }, [isLoaded, isLoading])

  return (
    <div className="w-full" ref={wrapperRef}>
      <blockquote className="twitter-tweet" data-width="100%">
        <a
          href={`https://twitter.com/${encodeURIComponent(userID)}/status/${encodeURIComponent(
            tweetID
          )}`}
        />
      </blockquote>
    </div>
  )
}
