import React, {useEffect, useState} from 'react'
import {FullNavigationFragment} from '../../@types/codegen/graphql'
import {useRouter} from 'next/router'

export interface RibbonProps {
  navigation: FullNavigationFragment
}

export function Ribbon({navigation}: RibbonProps) {
  const router = useRouter()

  const [showRibbon, setShowRibbon] = useState<boolean>(false)
  const [ribbonUrl, setRibbonUrl] = useState<string>('')
  const [ribbonText, setRibbonText] = useState<string>('')

  useEffect(() => {
    setShowRibbon(navigation.links.length > 0)
    if (navigation.links.length > 0) {
      const firstElement = navigation.links[0]
      setRibbonText(firstElement.label)
      if (firstElement.__typename === 'ArticleNavigationLink') {
        setRibbonUrl(`/zh/${firstElement.article.slug}`)
      } else if (firstElement.__typename === 'PageNavigationLink') {
        setRibbonUrl(`/${firstElement.page.slug}`)
      } else {
        setRibbonUrl(firstElement.url)
      }
    }
  }, [navigation.links])

  // hide ribbon on profile pages
  if (router.route.includes('/account')) return null

  return (
    <>
      {showRibbon && (
        <a href={ribbonUrl} target="_blank" rel="noreferrer">
          <div
            style={{
              transform: 'rotate(-45deg)',
              position: 'fixed',
              right: '-50px',
              width: '200px',
              padding: '16px',
              textAlign: 'center',
              color: '#f0f0f0',
              bottom: '23px'
            }}
            className="bg-tsri font-bold z-50">
            {ribbonText}
          </div>
        </a>
      )}
    </>
  )
}
