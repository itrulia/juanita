import React, {useEffect, useRef} from 'react'
import {transformCssStringToObject, useWindowSize} from '../utility'
import * as Sentry from '@sentry/nextjs'

export interface WeMakeItEmbedProps {
  title?: string
  url?: string
  width?: number
  height?: number
  styleCustom?: string
}

export function WeMakeItEmbed(data: WeMakeItEmbedProps) {
  const refIframe = useRef<HTMLIFrameElement>(null)

  const size = useWindowSize()

  return (
    <div className="mb-6" style={{width: '100%'}}>
      <iframe
        ref={refIframe}
        style={{
          position: 'relative',
          top: 0,
          left: 0,
          border: 'none'
        }}
        frameBorder={0}
        scrolling="no"
        allowFullScreen
        title={data.title}
        src={data.url}
        width={size.width < 650 ? size.width - 40 : `${data.width}`}
        height={size.width < 650 ? data.height - 110 : data.height}
      />
    </div>
  )
}
