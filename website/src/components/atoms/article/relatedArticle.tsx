import React from 'react'
import {ArticleRefFragment, ImageRefFragment} from '../../../@types/codegen/graphql'
import styled from 'styled-components'
import Link from 'next/link'

interface RAICProps {
  image: ImageRefFragment
}
const RelatedArticleImageContainer = styled.div`
  width: 100%;
  padding-top: 100%;
  background-size: cover;
  background-image: url('${(props: RAICProps) => props.image?.mediumURL}');
`
const RelatedArticleTitle = styled.div`
  font-weight: 600;
  font-size: 20px;
  line-height: 135%;

  @media (min-width: ${props => props.theme.screens.sm}) {
    font-weight: 700;
    font-size: 26px;
    line-height: 31px;
  }
`

interface RelatedArticleProps {
  article: ArticleRefFragment
}
export default function RelatedArticle({article}: RelatedArticleProps) {
  return (
    <Link href={`/zh/${article?.slug}.${article?.id}`}>
      <div>
        <RelatedArticleImageContainer image={article.image} />
        <RelatedArticleTitle className={'pt-4 pl-1'}>{article?.title}</RelatedArticleTitle>
      </div>
    </Link>
  )
}
