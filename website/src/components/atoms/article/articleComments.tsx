import React from 'react'
import {useScript} from '../../../helpers'
import styled from 'styled-components'

const CommentsDivider = styled.div`
  width: 100%;
  border-top: 1px solid black;
  margin-top: 100px;
`
const CommentsHeadline = styled.div`
  font-weight: 500;
  font-size: 17px;
  line-height: 23px;
  text-transform: uppercase;

  @media (min-width: ${props => props.theme.screens.sm}) {
    font-size: 23px;
    line-height: 28px;
  }
`

interface ArticleCommentsProps {
  slug: string
}
export default function ArticleComments({slug}: ArticleCommentsProps) {
  useScript('https://commento.tsri.app/js/commento.js', {
    'data-page-id': `/zh/${slug}/`
  })

  return (
    <>
      <CommentsDivider />
      <CommentsHeadline className={'pt-6'}>Kommentare</CommentsHeadline>
      <div id="commento"></div>
    </>
  )
}
