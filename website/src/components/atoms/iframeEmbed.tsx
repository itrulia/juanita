import React, {useEffect} from 'react'
import {transformCssStringToObject} from '../utility'

export interface IframeEmbedProps {
  title?: string
  url?: string
  width?: number
  height?: number
  styleCustom?: string
}

export function IframeEmbed(data: IframeEmbedProps) {
  const ratio = !!data.width && !!data.height ? data.width / data.height : 0
  const noRatio = !!data.styleCustom || ratio === 0
  const customStyleCss =
    noRatio && data.styleCustom !== ''
      ? transformCssStringToObject(data.styleCustom ?? '')
      : {
          width: '100%',
          height: '100%'
        }
  useEffect(() => {
    const listener = event => {
      if (typeof event.data['datawrapper-height'] !== 'undefined') {
        const iframes = document.querySelectorAll('iframe')
        for (let chartId in event.data['datawrapper-height']) {
          for (let i = 0; i < iframes.length; i++) {
            if (iframes[i].contentWindow === event.source) {
              const iframe = iframes[i]
              iframe.style.height = event.data['datawrapper-height'][chartId] + 'px'
            }
          }
        }
      }
    }
    globalThis.addEventListener('message', listener)
    return () => globalThis.removeEventListener('message', listener)
  })

  return (
    <div className="mb-6" style={{position: 'relative', width: '100%'}}>
      <div style={{width: '100%', paddingTop: `${noRatio ? '0' : (1 / ratio) * 100 + '%'}`}} />
      <iframe
        style={{
          position: noRatio ? 'relative' : 'absolute',
          top: 0,
          left: 0,
          border: 'none',
          ...customStyleCss
        }}
        frameBorder={0}
        scrolling="no"
        allowFullScreen
        title={data.title}
        src={data.url}
        width={data.width}
        height={data.height}
      />
    </div>
  )
}
