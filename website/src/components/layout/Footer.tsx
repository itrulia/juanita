import Link from 'next/link'

export const Footer = ({navigation}) => (
  <footer className="flex-shrink-0 w-full bottom-0 min-h-16">
    <div className="bg-black flex flex-col md:flex-row px-20 py-3 justify-between items-center">
      <div className="flex-1 md:flex-none">
        <Link href="/">
          <img className="w-20 md:w-28 cursor-pointer	" src="/logo-tsri-color.svg" alt="Tsri logo" />
        </Link>
      </div>
      <nav>
        <ul className="flex flex-wrap">
          {navigation?.links?.map(({article, page, url, label}, i) => {
            const href = article?.slug
              ? `/zh/${article?.slug}.${article?.id}`
              : page?.slug
              ? `/${page?.slug}`
              : url
            return (
              <li key={i} className="text-white capitalize block py-3 px-4">
                <Link href={href || '/404'}>{label}</Link>
              </li>
            )
          })}
        </ul>
      </nav>
    </div>
  </footer>
)

export default Footer
