import {useEffect, useState} from 'react'
import {Burger, MegaMenu} from './Menu'
import Link from 'next/link'
import {useAppContext} from '../../utils/AppContext.'
import {NavsProps} from '../../pages/_app'
import {useTranslation} from 'next-i18next'
import {useMatomo} from '@datapunt/matomo-tracker-react'
import {TrackingEventCategories} from '../utility'
import {useSession} from 'next-auth/react'

export type HeaderNavigations = Omit<NavsProps, 'footerNavigation'>

export interface HeaderProps {
  headerNavigations: HeaderNavigations
}

export default function Header(props: HeaderProps) {
  const [isMegaMenuOpen, toggleMenu] = useState(false)
  const {trackEvent} = useMatomo()
  const containerClassName = isMegaMenuOpen ? 'bg-blue  z-20' : 'bg-white'

  const {t} = useTranslation('common')

  const {state: appState, update: updateAppState} = useAppContext()

  async function handleProfileClick() {
    toggleMenu(false)
    trackEvent({
      category: TrackingEventCategories.Authentication,
      action: 'open',
      name: 'login-modal'
    })
    updateAppState({...appState, showLoginModal: true})
  }

  const [subscriptionNeedsAction, setSubscriptionNeedsAction] = useState(false)
  const {data: session} = useSession()

  useEffect(() => {
    if (session) {
      // @ts-ignore
      setSubscriptionNeedsAction(!session.user.subscriptionState.isActive)
    }
  }, [session])

  return (
    <header
      className={`top-0 z-20 w-screen ${isMegaMenuOpen ? 'fixed mobile:h-screen' : 'sticky'}`}>
      <div
        onClick={event => {
          event.stopPropagation()
        }}
        className={`${containerClassName} top-0 transition-all duration-200 ease-in-out flex flex-col py-4 md:py-3 lg:py-4 xl:py-7 z-20`}>
        <div className="mx-4 sm:mx-auto sm:container">
          <div className="w-full flex items-center">
            <div className="order-2 flex-1 lg:flex-none">
              <Link href="/">
                <a>
                  <img
                    style={{
                      marginTop: '-10px'
                    }}
                    className="w-20 md:w-24 lg:w-32 xl:w-40 cursor-pointer"
                    src="/logo.png"
                    alt="Tsri logo"
                  />
                </a>
              </Link>
            </div>
            <div className={`flex order-1 md:flex-1 ${isMegaMenuOpen ? 'text-white' : ''}`}>
              <Burger isOpen={isMegaMenuOpen} onClick={() => toggleMenu(!isMegaMenuOpen)} />
              <nav className="font-semibold md:text-lg md:pt-1">
                <ul className="md:inline-block md:flex hidden">
                  <li className="px-4 uppercase">
                    <Link href="/">
                      <a>{t('header.home')}</a>
                    </Link>
                  </li>
                  <li className="px-4 uppercase">
                    <Link href="/agenda">
                      <a>{t('header.agenda')}</a>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
            <div className="order-3 lg:flex-1 flex items-center justify-end">
              <span
                className="w-7 h-7 mx-2 md:mx-4 xl:mx-6 relative inline-block cursor-pointer"
                onClick={() => handleProfileClick()}>
                <img className="" src="/icons/account.png" alt="account icon" />
                {subscriptionNeedsAction && (
                  <span className="absolute top-0 -right-2 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white transform -translate-y-1/2 bg-red rounded-full">
                    !
                  </span>
                )}
              </span>

              {/* <img className="w-7 h-7 mx-2 md:mx-4 xl:mx-6" src="/icons/search.png" alt="search" /> */}
              <img
                onClick={() => {
                  window.location.href = `${process.env.NEXT_PUBLIC_WEBSITE_URL}/shop`
                }}
                className="w-7 h-7 ml-2 md:mx-4 xl:mx-6 cursor-pointer"
                src="/icons/cart.png"
                alt="search"
              />
              <Link href={`${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`}>
                <button
                  className={`ml-5 px-5 py-3 lg:px-3 lg:py-3 lg:text-lg lg:inline-block hidden ${
                    isMegaMenuOpen ? 'bg-white' : 'bg-blue text-white'
                  }`}>
                  {t('header.becomeMember')}
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
      {isMegaMenuOpen && (
        <MegaMenu
          className="fixed mobile:overflow-y-scroll bg-blue transition-all duration-200 ease-in-out mobile:h-full py-4 md:py-3 lg:px-12 lg:py-4 xl:py-7"
          headerNavigations={props.headerNavigations}
        />
      )}
    </header>
  )
}
