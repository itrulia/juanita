import {PaymentElement, useElements, useStripe} from '@stripe/react-stripe-js'
import React, {useState} from 'react'
import Button, {ButtonType} from '../atoms/Button'
import {useTranslation} from 'next-i18next'
import {toast} from 'react-toastify'
import * as Sentry from '@sentry/nextjs'
import {Loader} from '../atoms/loader'
import {handleError} from '../../helpers'

export interface StripePaymentProps {
  amount: number
  onClose(success: boolean): void
}

export default function StripePayment({amount, onClose}: StripePaymentProps) {
  const {t} = useTranslation('common')
  const stripe = useStripe()
  const elements = useElements()

  const [isLoading, setIsLoading] = useState<boolean>(true)

  const handlePayment = async event => {
    setIsLoading(true)
    event.preventDefault()

    if (!stripe || !elements) return // TODO: do we need to show an error?

    try {
      const {error} = await stripe.confirmPayment({
        elements,
        confirmParams: {
          return_url: window.location.href
        },
        redirect: 'if_required'
      })

      if (error) {
        handleError({errorText: error.message})
      } else {
        toast.success('Bezahlung erfolgreich')
        onClose(true)
      }
    } catch (error) {
      handleError({errorText: error.message})
      onClose(false)
    }
    setIsLoading(false)
  }

  return (
    <>
      <Loader isLoading={isLoading} />
      <form onSubmit={handlePayment}>
        <PaymentElement onReady={() => setIsLoading(false)} />
        {!isLoading && (
          <div className="mt-3 grid grid-flow-row gap-4">
            <Button buttonType="submit" disabled={!stripe} type={ButtonType.Primary}>
              Bezahlen
            </Button>
            <Button buttonType={'reset'} type={ButtonType.Secondary} onClick={() => onClose(false)}>
              Abbrechen
            </Button>
          </div>
        )}
      </form>
    </>
  )
}
