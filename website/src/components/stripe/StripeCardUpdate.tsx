import {CardElement, useElements, useStripe} from '@stripe/react-stripe-js'
import React, {useState} from 'react'
import Button, {ButtonType} from '../atoms/Button'
import {useTranslation} from 'next-i18next'
import {StripeCustomer} from '../../pages/account/subscription'
import {
  PaymentProviderCustomer,
  useUpdatePaymentProviderCustomersMutation
} from '../../@types/codegen/graphql'
import {toast} from 'react-toastify'
import * as Sentry from '@sentry/nextjs'
import {Loader} from '../atoms/loader'

export interface StripeCardUpdateProps {
  stripeCustomer?: StripeCustomer
  paymentProviderCustomers: PaymentProviderCustomer[]
  onClose(refresh: boolean): void
}

export default function StripeCardUpdate({
  stripeCustomer,
  paymentProviderCustomers,
  onClose
}: StripeCardUpdateProps) {
  const {t} = useTranslation('common')
  const stripe = useStripe()
  const elements = useElements()

  const [isLoading, setIsLoading] = useState<boolean>(true)

  const [updatePaymentProviderCustomers] = useUpdatePaymentProviderCustomersMutation()

  const handleCardSaving = async event => {
    setIsLoading(true)
    event.preventDefault()

    if (!stripe || !elements) return // TODO: do we need to show an error?

    try {
      const pm = await stripe.createPaymentMethod({
        type: 'card',
        card: elements.getElement('card')
      })

      const res = await fetch(
        `/api/updatePaymentMethod?pm=${pm.paymentMethod.id}${
          stripeCustomer ? `&customerID=${stripeCustomer.customer.id}` : ''
        }`
      )
      if (res.status === 200) {
        // paymentProviderID needs to be the same as set in the api/src/index.ts StripePaymentProvider
        const data = await res.json()
        const updatedPaymentProviderCustomers = paymentProviderCustomers.filter(
          ppc => ppc.paymentProviderID !== 'stripe'
        )
        updatedPaymentProviderCustomers.push({
          paymentProviderID: 'stripe',
          customerID: data.customerID
        })
        const result = await updatePaymentProviderCustomers({
          variables: {
            input: updatedPaymentProviderCustomers
          }
        })
        if (result.errors) {
          Sentry.captureException(result.errors)
        }
        onClose(true)
      } else {
        Sentry.captureException(res)
        toast.error(t('account.errorToast'), {autoClose: false}) //TODO: more information
      }
    } catch (error) {
      Sentry.captureException(error)
      toast.error(t('account.errorToast'), {autoClose: false}) //TODO: more information
    }
    setIsLoading(false)
  }

  return (
    <form onSubmit={handleCardSaving}>
      <Loader isLoading={isLoading} />
      <CardElement
        className="my-2"
        options={{hidePostalCode: true}}
        onReady={() => setIsLoading(false)}
      />
      {!isLoading && (
        <div className="mt-3">
          <Button buttonType="submit" disabled={!stripe} type={ButtonType.Primary}>
            {t('save')}
          </Button>
          <Button
            buttonType={'reset'}
            type={ButtonType.Secondary}
            className="ml-4"
            onClick={() => onClose(false)}>
            {t('abort')}
          </Button>
        </div>
      )}
    </form>
  )
}
