import React, {useMemo} from 'react'
import {RichText} from './atoms/RichText'
import {FullAuthorFragment} from '../@types/codegen/graphql'
import {getIsPromo} from './ArticleComponent'

export interface SponsorHeaderProps {
  advertiser?: FullAuthorFragment[]
}

/**
 * Whenever an article contains the tag "promotion", the authors are considered as sponsors.
 * @param authors
 * @constructor
 */
export function AdHeader({advertiser}: SponsorHeaderProps) {
  function getFirstLink(author: FullAuthorFragment): string {
    const links = author.links
    return links?.length ? links[0].url : ''
  }

  const isPromo = useMemo(() => {
    return getIsPromo(advertiser)
  }, [advertiser])

  if (!advertiser) return null

  return (
    <>
      {advertiser.map(sponsor => (
        <div
          key={sponsor.id}
          className="flex justify-start items-center border-b-2 border-tsri mb-4">
          {sponsor.image && (
            <div className="w-32 sm:w-20 pb-4">
              <img className="object-contain w-full object-right-bottom" src={sponsor.image.url} />
            </div>
          )}
          <div className="w-max pb-4 pl-4 text-tsri text-lg">
            <a href={getFirstLink(sponsor)} target="_blank" rel="noreferrer">
              {isPromo && <b>Rubrik Kultur wird präsentiert von:</b>}
              {!isPromo && <b>Präsentiert von: </b>}
              <RichText value={sponsor.bio} />
            </a>
          </div>
        </div>
      ))}
    </>
  )
}
