import {useRouter} from 'next/router'
import React, {useEffect, useState} from 'react'
import {BlockType, HeaderType, PublishedArticle} from './types'
import {articleAdapter} from './route/articleAdapter'
import {Article, useArticleLazyQuery, useArticleQuery} from '../@types/codegen/graphql'
import {TSRI_PAGE_TITLE} from '../helpers'
import {Loader} from './atoms/loader'
import App from './App'
import Head from 'next/head'
import {AdHeader} from './AdHeader'
import TsriTitleBlock from './blocks/tsriTitleBlock'
import {BlockRenderer} from './blocks/blockRenderer'
import {FullAuthorFragment} from '../@types/codegen/api'
import RelatedArticles from './atoms/article/relatedArticles'
import ArticleComments from './atoms/article/articleComments'

export interface ArticleProps {
  articleId: string
  isPreview: boolean
  navs: any
  article: Article
}

/**
 * Helper functions to implement tsri sponsoring
 * https://tsueri.atlassian.net/browse/TSRI-59
 * https://tsueri.atlassian.net/browse/TSRI-28
 */
const SPONSOR_NAMES = ['Energie 360°', 'EKZ', 'Mangold-Lobby', 'Stadt Zürich', 'Stapferhaus']
const PROMOTION_NAMES = ['Jugendkulturhaus Dynamo', 'Dynamo']
const AD_NAMES = [...SPONSOR_NAMES, ...PROMOTION_NAMES]
export function getNonPromotionAuthors(authors: FullAuthorFragment[]): FullAuthorFragment[] {
  return authors.filter(author => !PROMOTION_NAMES.includes(author.name))
}

export function getNonAdAuthors(authors: FullAuthorFragment[]): FullAuthorFragment[] {
  return authors.filter(author => !AD_NAMES.includes(author.name))
}
export function getAdvertisers(authors: FullAuthorFragment[]): FullAuthorFragment[] {
  return authors.filter(author => AD_NAMES.includes(author.name))
}
export function getIsSponsor(authors: FullAuthorFragment[]): boolean {
  return !!authors.find(author => SPONSOR_NAMES.includes(author.name))
}

export function getIsPromo(authors: FullAuthorFragment[]): boolean {
  return !!authors.find(author => PROMOTION_NAMES.includes(author.name))
}

const mapAuthors = (metaData: any[] | undefined) => {
  return metaData?.map((author, index) => {
    return <meta key={index} property="article:author" content={author.url} />
  })
}

export default function ArticleComponent({articleId, article, navs, isPreview}: ArticleProps) {
  const router = useRouter()
  const [articleData, setArticleData] = useState<PublishedArticle>(articleAdapter(article))
  const isTicker = articleData && articleData.tags.some(tag => tag === 'ticker')

  const {loading, error, data: updatedArticle, startPolling, stopPolling} = useArticleQuery({
    skip: !isTicker,
    variables: {
      id: articleId
    }
  })

  /**
   * In case we're navigating inside an article client-side (related article). Update article data.
   */
  const [fetchArticle] = useArticleLazyQuery({
    onCompleted: data => {
      if (data?.article) {
        setArticleData(articleAdapter(data.article))
      }
    }
  })
  useEffect(() => {
    const {slugID = ''} = router.query
    const [slug, id] = (slugID as string).split('.')
    fetchArticle({
      variables: {
        id: id
      }
    })
  }, [router.query])

  useEffect(() => {
    if (isTicker) {
      startPolling(10000)
      return () => stopPolling()
    }
  }, [])

  useEffect(() => {
    if (updatedArticle?.article) {
      setArticleData(articleAdapter(updatedArticle.article))
    }
  }, [updatedArticle])

  if (router.isFallback || !articleData) {
    return <Loader text="Laden..." isLoading={true} />
  }

  const {
    title,
    lead,
    image,
    tags,
    authors,
    publishedAt,
    updatedAt,
    blocks,
    socialMediaTitle,
    socialMediaDescription,
    socialMediaImage,
    socialMediaAuthors,
    canonicalUrl,
    url
  } = articleData

  const advertisers = getAdvertisers(authors)

  return (
    <App navs={navs}>
      <Head>
        <title>{`${title} - ${TSRI_PAGE_TITLE}`}</title>
        {lead && <meta name="description" content={lead} />}
        <link rel="canonical" href={url} />
        <meta property="og:title" content={socialMediaTitle || title} />
        <meta property="og:type" content="article" />
        <meta property="og:url" content={url} />
        {socialMediaDescription && (
          <meta property="og:description" content={socialMediaDescription} />
        )}
        {(image || socialMediaImage) && (
          <meta property="og:image" content={socialMediaImage?.ogURL ?? image?.ogURL ?? ''} />
        )}
        {socialMediaAuthors && mapAuthors(socialMediaAuthors)}
        {socialMediaAuthors?.length === 0 && mapAuthors(authors)}

        <meta property="article:published_time" content={publishedAt.toISOString()} />
        <meta property="article:modified_time" content={updatedAt.toISOString()} />
        {tags.map(tag => (
          <meta key={tag} property="article:tag" content={tag} />
        ))}

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={socialMediaTitle || title} />
        <meta name="twitter:site" content="@Tsueri_ch" />
        <meta name="twitter:description" content={socialMediaDescription || lead} />
        <meta name="twitter:image" content={socialMediaImage?.ogURL ?? image?.ogURL ?? ''} />
      </Head>
      <div
        className="xl:pr-52"
        style={
          isPreview
            ? {
                backgroundColor: '#eee',
                backgroundImage:
                  'repeating-linear-gradient(45deg, transparent, transparent 35px, rgba(255, 255, 255, 0.5) 35px, rgba(255, 255, 255, 0.5) 70px)'
              }
            : {}
        }>
        {blocks.length && blocks[0].type === BlockType.Title ? (
          <>
            {!!advertisers?.length && <AdHeader advertiser={advertisers} />}
            <TsriTitleBlock
              shareUrl={canonicalUrl || url}
              title={blocks[0].value.title}
              lead={blocks[0].value.lead}
              authors={authors}
              publishedAt={publishedAt}
              updatedAt={updatedAt}
              isPeerArticle={false}
              tags={tags}
              type={HeaderType.Default}
              isTicker={isTicker}
            />
            <BlockRenderer
              articleShareUrl={'canonicalURL'}
              publishedAt={publishedAt}
              updatedAt={updatedAt}
              isArticle={true}
              blocks={[...blocks.slice(1, 1), ...blocks.slice(1)]}
              tags={tags}
            />
          </>
        ) : (
          <BlockRenderer
            articleShareUrl={'canonicalURL'}
            publishedAt={publishedAt}
            updatedAt={updatedAt}
            isArticle={true}
            blocks={blocks}
            tags={tags}
          />
        )}
        {/* related articles */}
        <div className={'pt-20 sm:mt-20'}>
          <RelatedArticles article={articleData} amount={3} />
        </div>

        {/* commenting system */}
        <ArticleComments slug={articleData.slug} />
      </div>
    </App>
  )
}
