import Link from 'next/link'
import App from './App'
import {useTranslation} from 'next-i18next'
import React from 'react'
import {TSRI_PAGE_TITLE} from '../helpers'
import Head from 'next/head'

export interface OldTeaserGridProps {
  navs: any
  title?: string
  newerArticlesLink?: string
  olderArticlesLink?: string
  children: React.ReactNode
}

export default function OldTeaserGrid({
  navs,
  title,
  newerArticlesLink,
  olderArticlesLink,
  children
}: OldTeaserGridProps) {
  const {t} = useTranslation('common')
  return (
    <App navs={navs || {}}>
      <Head>
        <title>
          {title
            ? `${title.charAt(0).toUpperCase() + title.slice(1)} - ${TSRI_PAGE_TITLE}`
            : TSRI_PAGE_TITLE}
        </title>
      </Head>
      <div className="">
        {title && (
          <div className="w-full">
            <h1 className="text-tsri uppercase">{title}</h1>
            {title === 'klima' && (
              <a
                href="https://www.facebook.com/pg/klimastadtzuerich/events/"
                target="_blank"
                rel="noreferrer">
                <div className="flex items-center my-4 border-blue border-2 w-full sm:w-2/5 m-auto">
                  <img
                    className="h-16"
                    src="https://storage.googleapis.com/tsri-static/website-static/klimastadt-zuerich-logo.jpg"
                  />
                  <div>
                    <p>{t('other.klimastadt')}</p>
                  </div>
                </div>
              </a>
            )}
          </div>
        )}
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-10">
          {children}
        </div>
        <hr className="border-gray-200 mt-10 mb-4" />
        <div className="flex mb-4 transition-colors duration-200 hover:text-tsri">
          {newerArticlesLink && (
            <div className="flex mr-8">
              <Link href={newerArticlesLink}>{t('newer-articles')}</Link>
            </div>
          )}
          {olderArticlesLink && (
            <div className="flex text-right ml-auto">
              <Link href={olderArticlesLink}>{t('older-articles')}</Link>
            </div>
          )}
        </div>
      </div>
    </App>
  )
}
