import Header from 'components/layout/Header'
import Footer from 'components/layout/Footer'
import Head from 'next/head'
import {ToastContainer} from 'react-toastify'
import LoginModal from './LoginModal'
import {TSRI_PAGE_TITLE} from '../helpers'
import {Ribbon} from './atoms/ribbon'
import FACEBOOK_PIXEL_1 from './facebook/pixel-1'
import React from 'react'

export default function App({children, navs}) {
  const {footerNavigation, ribbonNavigation, ...headerNavigations} = navs
  return (
    <>
      <Head>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v1=new" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v1=new" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v1=new" />
        <link rel="manifest" href="/site.webmanifest?v1=new" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v1=supernew" color="#5bbad5" />
        <link rel="shortcut icon" href="/favicon.ico?v1=new" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#36ADDF" />
        <meta name="apple-mobile-web-app-status-bar" content="#90cdf4" />
        <FACEBOOK_PIXEL_1 />
        <title>{TSRI_PAGE_TITLE}</title>
      </Head>
      <div className="min-h-screen relative flex flex-col">
        <ToastContainer hideProgressBar={true} draggable={false} />
        <LoginModal />
        {ribbonNavigation && <Ribbon navigation={ribbonNavigation} />}
        <Header headerNavigations={headerNavigations} />
        <main className="flex-main pb-4 mx-4 md:mx-auto md:container pt-10">{children}</main>
        <Footer navigation={footerNavigation} />
      </div>
    </>
  )
}
