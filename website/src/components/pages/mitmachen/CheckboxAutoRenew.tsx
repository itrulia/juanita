import React from 'react'
import {PaymentPeriodicity} from '../../../@types/codegen/api'
import {useTranslation} from 'next-i18next'

interface CheckboxAutoRenewProps {
  autoRenew: boolean
  paymentPeriodicity: PaymentPeriodicity
  onAutoRenewChange(autoRenew: boolean): void
}

export default function CheckboxAutoRenew({autoRenew, paymentPeriodicity, onAutoRenewChange}) {
  const {t} = useTranslation()

  return (
    <label className="text-gray-700 ml-1">
      <input
        type="checkbox"
        className="accent-tsri"
        checked={autoRenew}
        disabled={paymentPeriodicity === PaymentPeriodicity.Monthly}
        onChange={() => {
          onAutoRenewChange(!autoRenew)
        }}
      />
      <span className="ml-2 uppercase">{t('account.autoRenew')}</span>
    </label>
  )
}
