import React, {useEffect, useMemo, useState} from 'react'
import {useForm} from 'react-hook-form'
import Button, {ButtonType} from '../../atoms/Button'
import {
  ChallengeInput,
  FullMemberPlanFragment,
  PaymentPeriodicity
} from '../../../@types/codegen/api'
import {
  getMonthsinNumberFromPaymentPeriodicity,
  handleError,
  PaymentSlugs,
  TSRI_SELLING_MEMBER_PLAN_TAG,
  validateEmailRegex
} from '../../../helpers'
import ChallengeModal from './ChallengeModal'
import {
  useChallengeQuery,
  useCreateSubscriptionMutation,
  useMemberPlanListQuery,
  useRegisterMemberAndReceivePaymentMutation
} from '../../../@types/codegen/graphql'
import {useCookies} from 'react-cookie'
import {useSession} from 'next-auth/react'
import {InfoBox} from '../../atoms/InfoBox'
import Link from 'next/link'
import SelectPaymentInterval from './SelectPaymentInterval'
import CheckboxAutoRenew from './CheckboxAutoRenew'
import Spinner from 'components/atoms/Spinner'

const noMemberPlanText = `Es ist kein gültiger Memberplan konfiguriert. Ein Memberplan sollte das Tag ${TSRI_SELLING_MEMBER_PLAN_TAG} enthalten.`

export function checkMemberPlan(
  memberPlan: FullMemberPlanFragment | undefined,
  hideError: boolean = false
): boolean {
  const memberPlanSlug = memberPlan?.slug
  if (memberPlanSlug) return true
  if (!hideError) {
    handleError({
      errorText: noMemberPlanText
    })
  }
  return false
}

interface MitmachenFormProps {
  isLoading?(state: boolean): void
}

export default function MitmachenForm({isLoading}: MitmachenFormProps) {
  const [name, setName] = useState<string | undefined>(undefined)
  const [preferredName, setPreferredName] = useState<string | undefined>(undefined)
  const [email, setEmail] = useState<string | undefined>(undefined)
  const [monthlyAmount, setMonthlyAmount] = useState<number>(15)
  const [paymentPeriodicity, setPaymentPeriodicity] = useState<PaymentPeriodicity>(
    PaymentPeriodicity.Yearly
  )
  const [autoRenew, setAutoRenew] = useState<boolean>(true)
  const [paymentProvider, setPaymentProvider] = useState<PaymentSlugs>(PaymentSlugs.STRIPE_PAYMENT)
  const [showChallenge, setShowChallenge] = useState<boolean>(false)
  const baseUrl = process.env.NEXT_PUBLIC_WEBSITE_URL
  const successUrl = `${baseUrl}/account/subscription?paymentSuccess=true`
  const failureUrl = `${baseUrl}/account/subscription?paymentSuccess=false`

  const {data: session, status: sessionStatus} = useSession()
  const [cookie, setCookie] = useCookies()
  const [registerMemberAndReceivePaymentMutation] = useRegisterMemberAndReceivePaymentMutation({
    fetchPolicy: 'no-cache',
    onError: error => {
      error.graphQLErrors.forEach(graphQLError => {
        const errorText =
          graphQLError.extensions.code === 'EMAIL_ALREADY_IN_USE'
            ? 'Du bist bereits registriert. Bitte logge Dich ein.'
            : graphQLError.message
        handleError({errorText, autoClose: false})
      })
    }
  })
  const [createSubscriptionMutation] = useCreateSubscriptionMutation({
    fetchPolicy: 'no-cache',
    onError: error => handleError({errorText: error.message})
  })
  const {
    handleSubmit,
    register,
    formState: {errors}
  } = useForm()

  // load memberplans
  const {data: memberPlanList, loading: loadingMemberPlanList} = useMemberPlanListQuery({
    variables: {
      first: 10
    }
  })

  // load challenge
  const {data: challengeData, refetch: refetchChallenge} = useChallengeQuery()

  const monthCount = useMemo(() => {
    return getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity)
  }, [paymentPeriodicity])
  const totalAmount = useMemo(() => {
    return getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity) * monthlyAmount
  }, [paymentPeriodicity, monthlyAmount])
  const hasSubscription: boolean = useMemo(() => {
    return session?.user?.subscriptionState.hasSubscription
  }, [session?.user?.subscriptionState.hasSubscription])
  const isLoggedIn: boolean = useMemo(() => {
    return !!session?.user
  }, [session?.user])

  const memberPlan: FullMemberPlanFragment | undefined = useMemo(() => {
    if (!memberPlanList) return undefined
    return memberPlanList?.memberPlans?.nodes?.find(findMemberPlan => {
      return findMemberPlan?.tags?.includes(TSRI_SELLING_MEMBER_PLAN_TAG)
    })
  }, [memberPlanList])

  /**
   * Handle loading logic for this component
   */
  const [subscriptionLoading, setSubscriptionLoading] = useState<boolean>(false)
  const [registerMemberLoading, setRegisterMemberLoading] = useState<boolean>(false)

  const loadingSession = useMemo(() => {
    return sessionStatus === 'loading'
  }, [sessionStatus])

  // loading memo
  const componentLoading = useMemo(() => {
    const loading =
      loadingMemberPlanList || loadingSession || subscriptionLoading || registerMemberLoading
    isLoading(loading)
    return loading
  }, [loadingMemberPlanList, loadingSession, subscriptionLoading, registerMemberLoading])

  /**
   * Force auto-renew to be set, if payment periodicity is one month only
   * @param paymentPeriodicity
   */
  function handlePaymentPeriodicity(paymentPeriodicity: PaymentPeriodicity) {
    setPaymentPeriodicity(paymentPeriodicity)
    if (paymentPeriodicity === PaymentPeriodicity.Monthly) {
      setAutoRenew(true)
    }
  }

  async function submitForm() {
    if (isLoggedIn) {
      await createSubscription()
    } else {
      // show challenge first
      setShowChallenge(true)
    }
  }

  async function createSubscription() {
    if (!checkMemberPlan(memberPlan)) {
      return <div>hallo</div>
    }

    setSubscriptionLoading(true)
    try {
      const {data} = await createSubscriptionMutation({
        variables: {
          autoRenew,
          monthlyAmount: monthlyAmount * 100,
          memberPlanSlug: memberPlan.slug,
          paymentPeriodicity,
          paymentMethodSlug: paymentProvider,
          failureUrl,
          successUrl
        }
      })
      const returnedPaymentMethodSlug = data.createSubscription.paymentMethod.slug as PaymentSlugs
      const intentSecret = data.createSubscription.intentSecret

      redirectToPayment(returnedPaymentMethodSlug, intentSecret)
      setSubscriptionLoading(false)
    } catch (e) {
      setSubscriptionLoading(false)
      handleError({errorText: e.toString()})
    }
  }

  /**
   * Simply create a new subscription without register any user.
   */

  /**
   * New user is registered and subscription is created.
   * @param challengeSolution
   */
  async function registerMemberAndReceivePayment(challengeAnswer: ChallengeInput) {
    if (!checkMemberPlan(memberPlan)) {
      return
    }

    setRegisterMemberLoading(true)
    // close challenge modal
    await refetchChallenge()
    setShowChallenge(false)
    const memberPlanSlug = memberPlan.slug

    try {
      const {data} = await registerMemberAndReceivePaymentMutation({
        variables: {
          autoRenew,
          monthlyAmount: monthlyAmount * 100,
          name,
          preferredName,
          email,
          paymentMethodSlug: paymentProvider,
          challengeAnswer,
          paymentPeriodicity,
          memberPlanSlug,
          successURL: successUrl,
          failureURL: failureUrl
        }
      })
      setRegisterMemberLoading(false)
      const registerResponse = data.registerMemberAndReceivePayment
      const sessionToken = registerResponse.session.token
      const intentSecret = registerResponse.payment.intentSecret
      const returnedPaymentMethodSlug = registerResponse.payment.paymentMethod.slug as PaymentSlugs

      if (!sessionToken) {
        handleError({errorText: 'Kein Session-Token von der API erhalten.'})
        return
      }
      // set cookies, to logged-in after the next page reload
      await setCookie('juanita-next-auth.session-token', sessionToken, {
        path: '/',
        secure: true,
        sameSite: 'lax'
      })

      // payment
      redirectToPayment(returnedPaymentMethodSlug, intentSecret)
    } catch (e) {
      setRegisterMemberLoading(false)
      return
    }
  }

  function redirectToPayment(paymentSlug: PaymentSlugs, intentSecret: string) {
    // some error checks
    if (!intentSecret) {
      handleError({errorText: 'Kein Intentsecret von der API erhalten.'})
      return
    }
    if (paymentSlug !== paymentProvider) {
      handleError({
        errorText:
          'Unerwarteter Fehler: Payment-Methode von der API stimmt nicht mit der Payment-Methode Deines Clients überein.'
      })
      return
    }

    if (paymentSlug === PaymentSlugs.PAYREXX_PAYMENT) {
      // go to Payrexx payment
      window.location.href = intentSecret
    } else if (paymentSlug === PaymentSlugs.STRIPE_PAYMENT) {
      // go to stripe payment form and login through site reload
      window.location.href = `${baseUrl}/mitmachen/payment?intentSecret=${intentSecret}&amount=${totalAmount}`
    } else {
      handleError({errorText: `PaymentSlug wird nicht unterstützt: ${paymentSlug}`})
    }
  }

  // component is loading. show spinner.
  if (componentLoading) {
    return null
  }

  // user already has a subscription. avoid multiple subscriptions per user
  if (hasSubscription) {
    return (
      <div className="pt-8">
        <InfoBox>
          <div>
            Du hast bereits ein Abo.{' '}
            <Link href="/account/subscription">
              Klicke hier, um in dein Profil zu gelangen und das Abo zu verwalten.
            </Link>
          </div>
        </InfoBox>
      </div>
    )
  }

  // no member plan available. show error box.
  if (!checkMemberPlan(memberPlan, true)) {
    return (
      <div className="pt-8">
        <InfoBox>{noMemberPlanText}</InfoBox>
      </div>
    )
  }

  return (
    <>
      <div className="grid grid-cols-1 lg:grid-cols-6">
        <div
          id="mitmachen-form"
          className="mt-16 lg:col-start-2 lg:col-span-4 border-dashed border border-tsri p-4">
          <div className="grid grid-cols-1 md:justify-items-center">
            <p
              className={`text-3xl lg:text-4xl font-bold text-tsri ${
                isLoggedIn ? 'md:w-2/3 lg:w-1/2' : ''
              }`}>
              Jetzt Tsüri-Member werden
            </p>
          </div>
          <form onSubmit={handleSubmit(submitForm)}>
            <div
              className={`grid grid-cols-1 md:gap-4 lg:gap-6 ${
                isLoggedIn ? 'justify-items-center' : 'md:grid-cols-2'
              }`}>
              {!isLoggedIn && (
                <div>
                  {/* name */}
                  <div className="mt-4">
                    <p className="uppercase">Voller Name *</p>
                    <input
                      className="w-full border-solid border-2 p-2"
                      value={name || ''}
                      {...register('name', {required: true})}
                      type="text"
                      onChange={event => setName(event.target.value)}
                    />
                    {errors.name && <span className="text-error">Bitte Namen eingeben</span>}
                  </div>

                  {/* preferred name */}
                  <div className="mt-4">
                    <p className="uppercase">Bevorzugter Name</p>
                    <input
                      className="w-full border-solid border-2 p-2"
                      value={preferredName || ''}
                      {...register('preferredName')}
                      type="text"
                      onChange={event => setPreferredName(event.target.value)}
                    />
                  </div>

                  {/* email */}
                  <div className="mt-4">
                    <p className="uppercase">E-Mail *</p>
                    <input
                      className="w-full border-solid border-2 p-2"
                      value={email || ''}
                      type="text"
                      {...register('email', {
                        required: true,
                        pattern: {value: validateEmailRegex, message: 'Keine Gültige E-Mail'}
                      })}
                      onChange={event => setEmail(event.target.value)}
                    />
                    {errors.email && (
                      <span className="text-error">Bitte gültige E-Mail eingeben</span>
                    )}
                  </div>

                  {/* payment intervall */}
                  <div className="mt-4">
                    <SelectPaymentInterval
                      paymentPeriodicity={paymentPeriodicity}
                      onPaymentPeriodicityChange={paymentPeriodicity =>
                        handlePaymentPeriodicity(paymentPeriodicity)
                      }
                    />
                  </div>

                  {/* auto renewal */}
                  <div className="mt-4">
                    <CheckboxAutoRenew
                      autoRenew={autoRenew}
                      paymentPeriodicity={paymentPeriodicity}
                      onAutoRenewChange={autoRenew => setAutoRenew(autoRenew)}
                    />
                  </div>
                </div>
              )}

              <div className={`${isLoggedIn ? 'md:w-2/3 lg:w-1/2' : ''}`}>
                {/* slider */}
                <div className="mt-8 md:mt-9">
                  <label className="uppercase">
                    Ich unterstütze Tsüri.ch mit {monthlyAmount} CHF pro Monat.
                  </label>
                  <input
                    type="range"
                    min={memberPlan.amountPerMonthMin / 100}
                    max="50"
                    step="1"
                    value={monthlyAmount}
                    onChange={event => {
                      setMonthlyAmount(parseInt(event.target.value))
                    }}
                    className="appearance-none w-full h-2 bg-tsri mt-4"
                    id="monthlyAmountSlider"
                  />
                </div>

                {isLoggedIn && (
                  <>
                    <div className="mt-4 md:w-1/2">
                      <SelectPaymentInterval
                        paymentPeriodicity={paymentPeriodicity}
                        onPaymentPeriodicityChange={paymentPeriodicity =>
                          handlePaymentPeriodicity(paymentPeriodicity)
                        }
                      />
                    </div>
                    <div className="mt-4 md:w-1/2">
                      <CheckboxAutoRenew
                        autoRenew={autoRenew}
                        paymentPeriodicity={paymentPeriodicity}
                        onAutoRenewChange={autoRenew => setAutoRenew(autoRenew)}
                      />
                    </div>
                  </>
                )}

                {/* abo overview */}
                <div className="mt-8 text-center">
                  <p className="uppercase w-full border-b border-black border-solid">
                    {monthCount} {monthCount === 1 ? 'Monat' : 'Monate'} für {totalAmount} CHF
                  </p>
                  <p className="font-bold">YES - Ich will Tsüri-Member werden</p>
                </div>

                {/* submit */}
                <div className="mt-8 text-center w-full">
                  <div className="flex">
                    <div className="flex-1">
                      <select
                        className="w-full border-solid border-2 min-h-full"
                        style={{height: '100% !important'}}
                        value={paymentProvider}
                        onChange={event => {
                          setPaymentProvider(event.target.value as PaymentSlugs)
                        }}>
                        <option value={PaymentSlugs.STRIPE_PAYMENT}>
                          Mit Kreditkarte (Visa, Mastercard)
                        </option>
                        <option value={PaymentSlugs.PAYREXX_PAYMENT}>
                          Mit TWINT, Paypal, Postfinance, usw.
                        </option>
                      </select>
                    </div>
                    <div className="ml-1">
                      <Button type={ButtonType.Primary} buttonType="submit" className="w-full">
                        bezahlen
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

          {/* challenge */}
          <ChallengeModal
            show={showChallenge}
            challenge={challengeData?.challenge}
            onCancel={async () => {
              setShowChallenge(false)
              await refetchChallenge()
            }}
            onSetChallengeAnswer={async challengeAnswer => {
              await registerMemberAndReceivePayment(challengeAnswer)
            }}
          />
        </div>
      </div>
    </>
  )
}
