import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  faBicycle,
  faBookOpen,
  faCalendarWeek,
  faEnvelopeOpen,
  faIcons,
  faLifeRing,
  faNewspaper,
  faPercentage,
  IconDefinition,
  faArrowUp
} from '@fortawesome/free-solid-svg-icons'
import Image from 'next/image'
import mitmachenCollageImg from '../../../../public/imgs/mitmachen/mitmachen_collage.png'
import SocialMediaShareBtns from '../../atoms/SocialMediaShareBtns'

interface TsriChance {
  icon: IconDefinition
  title: string
  text: string
}

interface MemberAdvantage {
  icon: IconDefinition
  title: string
}

export default function MitmachenAppendix() {
  const baseUrl: string = `${process.env.NEXT_PUBLIC_WEBSITE_URL}/mitmachen`
  const shareIconSize: number = 40
  const tsriChances: TsriChance[] = [
    {
      icon: faBicycle,
      title: 'Die Stadt bewegen',
      text:
        'Wir setzen uns aktiv für mehr Diversität, Nachhaltigkeit, Gleichstellung und ein schönes Zürich ein, in dem alle Platz haben.'
    },
    {
      icon: faBookOpen,
      title: 'Geschichten erzählen',
      text:
        'Wir erzählen Geschichten, die andere nicht erzählen und machen Zürich so zu einer schöneren und offeneren Stadt.'
    },
    {
      icon: faLifeRing,
      title: 'Überleben',
      text:
        'Ohne den Support von dir und allen anderen Member, wäre Tsüri.ch schon längst tot. Merci tuusig für deine Unterstützung. <3'
    }
  ]

  const memberAdvantages: MemberAdvantage[] = [
    {
      icon: faCalendarWeek,
      title: 'Die beste Agenda der Stadt per Mail'
    },
    {
      icon: faEnvelopeOpen,
      title: 'Ein monatliches Inside-Tsüri-Mail'
    },
    {
      icon: faNewspaper,
      title: 'Ein tägliches News-Briefing'
    },
    {
      icon: faPercentage,
      title: 'Rabatt im Tsüri-Shop'
    },
    {
      icon: faIcons,
      title: 'Teilnahme an exklusiven Memberevents (Führungen, Spaziergänge, Konzerte)'
    }
  ]

  function scrollToBecomeMemberForm() {
    if (typeof document === 'undefined') {
      return
    }
    document.getElementById('mitmachen-form').scrollIntoView({behavior: 'smooth'})
  }

  return (
    <>
      <div className="w-full pt-16">
        <p className="text-3xl lg:text-4xl text-tsri font-bold md:text-center">
          Was Tsüri.ch dank den Membern tun kann
        </p>

        {/* member advantages */}
        <div className="grid grid-cols-12 gap-x-6 justify-items-center">
          {tsriChances.map(tsriChance => (
            <div className="col-span-10 md:col-span-4" key={tsriChance.title}>
              <div className="grid grid-flow-col auto-cols-auto items-center gap-6 pt-4">
                <div>
                  <FontAwesomeIcon icon={tsriChance.icon} className="text-tsri h-12 text-center" />
                </div>
                <div>
                  <p className="text-xl lg:text-2xl font-bold">{tsriChance.title}</p>
                  <p className="hyphens-auto lg:text-xl">{tsriChance.text}</p>
                </div>
              </div>
            </div>
          ))}
        </div>

        {/* image collage */}
        <div className="grid grid-flow-col auto-cols-auto pt-16">
          <Image src={mitmachenCollageImg} />
        </div>

        {/* member advantages */}
        <p className="text-3xl lg:text-4xl text-tsri font-bold pt-16 md:text-center">
          Deine Vorteile als Tsüri-Member
        </p>
        <div className="grid grid-cols-2 md:grid-cols-3 pt-4 gap-4 md:gap-8 justify-around">
          {memberAdvantages.map(memberAdvantage => (
            <div key={memberAdvantage.title} className="">
              <div className="grid grid-flow-col auto-cols-auto justify-items-center">
                <div>
                  <FontAwesomeIcon icon={memberAdvantage.icon} className="text-tsri h-8" />
                </div>
              </div>
              <p className="text-center lg:text-xl pt-4 hyphens-auto">{memberAdvantage.title}</p>
            </div>
          ))}
        </div>

        {/* share this page */}
        <p className="text-3xl lg:text-4xl text-tsri font-bold pt-16 text-center hyphens-auto md:text-center">
          Teile diese Seite mit deinen Freund:innen
        </p>
        <div className="flex justify-center gap-4 pt-4">
          <SocialMediaShareBtns url={baseUrl} size={shareIconSize} />
        </div>

        {/* become a tsüri member scroll to */}
        <div
          className="flex justify-center gap-4 text-tsri cursor-pointer pb-16 pt-4"
          onClick={() => {
            scrollToBecomeMemberForm()
          }}>
          <FontAwesomeIcon icon={faArrowUp} className="h-8" />
          <p className="text-3xl lg:text-4xl font-bold text-center">Jetzt Tsüri-Member werden</p>
          <FontAwesomeIcon icon={faArrowUp} className="h-8" />
        </div>
      </div>
    </>
  )
}
