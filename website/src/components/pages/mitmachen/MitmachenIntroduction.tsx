import React from 'react'
import Image from 'next/image'
import mitmachen from '../../../../public/imgs/mitmachen/mitmachen_header.jpg'

export default function MitmachenIntroduction({memberCount}: {memberCount?: number}) {
  return (
    <>
      {/* image with overlay */}
      <div className="relative w-full h-52 md:h-80 xl:h-96 -mt-10">
        <Image alt="Lochergut" src={mitmachen} layout="fill" objectFit="cover" />
        <div className="absolute h-full w-full bg-black/[0.2] text-tsri font-bold text-center">
          <div className="flex flex-wrap content-center h-full">
            <div className="basis-full text-3xl md:text-4xl lg:text-5xl xl:text-6xl">
              #MirSindTsüri
            </div>
            {!!memberCount && (
              <div className="mt-4 basis-full text-xl md:text-2xl lg:text-3xl xl:text-4xl">
                {memberCount} Tsüri-Member und du?
              </div>
            )}
          </div>
        </div>
      </div>

      {/* introduction text */}
      <div className="grid grid-cols-6 xl:grid-cols-12 mt-16">
        <div className="col-span-6 md:col-span-4 xl:col-span-6 md:col-start-2 xl:col-start-4">
          <p className="text-3xl lg:text-4xl text-tsri font-bold md:text-center">
            Zusammen bewegen wir die Stadt
          </p>
          <p className="mt-4 md:text-center lg:text-xl">
            Wir sind dein Zürcher Community-Magazin. Gemeinsam mit dir und allen anderen Membern
            führen wir Debatten, sind nah am Geschehen und haben zusammen Spass. Als Tsüri-Member
            bist du Teil dieses interaktiven Journalismus-Projekts.
            {!!memberCount && (
              <span className="font-bold">
                Bereits {memberCount} Zürcher:innen und Nicht-Zürcher:innen sind Tsüri-Member. Und
                du?
              </span>
            )}
          </p>
        </div>
      </div>
    </>
  )
}
