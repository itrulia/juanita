import React, {useState} from 'react'
import Modal from '../../Modal'
import {useForm} from 'react-hook-form'
import Button, {ButtonType} from '../../atoms/Button'
import {Challenge, ChallengeInput} from '../../../@types/codegen/api'

interface ChallengeViewProps {
  show: boolean
  challenge?: Challenge
  onCancel(): void
  onSetChallengeAnswer(challengeAnswer: ChallengeInput): void
}

export default function ChallengeModal(props: ChallengeViewProps) {
  const [challengeSolution, setChallengeSolution] = useState<string | undefined>(undefined)
  const {
    register,
    handleSubmit,
    formState: {errors}
  } = useForm()

  const submitForm = data => {
    if (!challengeSolution) {
      return
    }
    const challengeAnswer: ChallengeInput = {
      challengeID: props.challenge.challengeID,
      challengeSolution
    }
    props.onSetChallengeAnswer(challengeAnswer)
  }

  if (!props.challenge?.challenge) {
    return <>Fehler: Keine Challenge verfügbar. Bitte wende Dich an info@tsri.ch</>
  }

  return (
    <>
      <Modal
        show={props.show}
        showTopClose
        allowBackgroundClose={false}
        onClose={() => {
          props.onCancel()
        }}>
        <form onSubmit={handleSubmit(submitForm)}>
          <div className="grid grid-cols-4 items-center">
            {/* title */}
            <div className="col-span-4 text-2xl font-bold text-left z-10">Spam-Schutz</div>

            {/* challenge image */}
            <div className="col-span-2 -mt-6">
              <img
                src={`data:image/svg+xml;utf8,${encodeURIComponent(props.challenge.challenge)}`}
              />
            </div>
            {/* challenge solution */}
            <div className="col-span-2 pl-2 -mt-4">
              <input
                className="w-full border-solid border-2 p-2"
                value={challengeSolution || ''}
                {...register('challengeSolution', {required: true})}
                type="text"
                onChange={event => setChallengeSolution(event.target.value)}
              />
              {errors.challengeSolution && <span className="text-error">Resultat eingeben</span>}
            </div>
            {/* submit */}
            <div className="col-span-4">
              <Button type={ButtonType.Primary} buttonType="submit" className="w-full">
                Weiter
              </Button>
            </div>
          </div>
        </form>
      </Modal>
    </>
  )
}
