import {
  ArticleMeta,
  Block,
  BlockType,
  EmbedType,
  HeaderType,
  ImageData,
  TitleBlockValue
} from '../types'
import {ListicalItem} from '../blocks/listicalBlock'

export enum BlockTypes {
  ArticleGridBlock = 'ArticleGridBlock',
  RichTextBlock = 'RichTextBlock',
  ImageBlock = 'ImageBlock',
  ImageGalleryBlock = 'ImageGalleryBlock',
  FacebookPostBlock = 'ImageGalleryBlock',
  InstagramPostBlock = 'InstagramPostBlock',
  TwitterTweetBlock = 'TwitterTweetBlock',
  VimeoVideoBlock = 'TwitterTweetBlock',
  YouTubeVideoBlock = 'YouTubeVideoBlock',
  SoundCloudTrackBlock = 'SoundCloudTrackBlock',
  ListicleBlock = 'ListicleBlock',
  LinkPageBreakBlock = 'LinkPageBreakBlock'
}

export function getArticleBlocks(blocks: any, articleMeta: ArticleMeta) {
  return getBlocks(blocks, articleMeta)
}

export function getBlocks(blocks: any, articleMeta?: ArticleMeta): Block[] {
  let hasTitleImage = false

  return blocks.map((block: any, index: number) => {
    switch (block.__typename) {
      case BlockTypes.RichTextBlock:
        return {
          type: BlockType.RichText,
          key: index,
          value: block.richText
        }

      case 'ImageBlock':
        if (!block.image) return null

        if (index == 0) {
          hasTitleImage = true
          return {
            type: BlockType.TitleImage,
            key: index,
            value: {
              ...block.image,
              caption: block.caption
            }
          }
        } else
          return {
            type: BlockType.Image,
            key: index,

            value: {
              ...block.image,
              caption: block.caption
            }
          }

      case 'ImageGalleryBlock':
        return {
          type: BlockType.Gallery,
          key: index,
          value: {
            title: '',
            media: block.images.map((image: any) => imageEdgeToMedia(image))
          }
        }

      case 'TeaserGridBlock':
        return {
          type: BlockType.Grid,
          key: index,
          value: {
            numColumns: block.numColumns,
            teasers: block.teasers
          }
        }

      case 'TeaserGridFlexBlock':
        return {
          type: BlockType.GridFlex,
          key: index,
          value: {
            flexTeasers: block.flexTeasers
          }
        }

      case 'ListicleBlock':
        return {
          type: BlockType.Listicle,
          key: index,
          value: listicleToListical(block.items)
        }

      case 'LinkPageBreakBlock':
        return {
          type: BlockType.LinkPageBreak,
          key: index,
          value: block
        }

      case 'QuoteBlock':
        return {
          type: BlockType.Quote,
          key: index,
          value: {
            text: block.quote,
            author: block.author
          }
        }

      case 'TitleBlock':
        let value: TitleBlockValue = {
          type: HeaderType.Default,
          title: block.title,
          lead: block.lead
        }
        if (articleMeta && (index == 0 || (hasTitleImage && index == 1))) {
          value.preTitle = articleMeta.preTitle
          value.date = new Date(articleMeta.publishedAt)
          value.isHeader = true
        }
        if (articleMeta && articleMeta.isBreaking) {
          value.type = HeaderType.Breaking
          value.preTitle = articleMeta.preTitle
          value.date = new Date(articleMeta.publishedAt)
          value.isHeader = true
        }
        return {
          type: BlockType.Title,
          key: index,
          value: value
        }

      // embeds
      case 'FacebookPostBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.FacebookPost,
            userID: block.userID,
            postID: block.postID
          }
        }

      case 'InstagramPostBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.InstagramPost,
            postID: block.postID
          }
        }

      case 'TwitterTweetBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.TwitterTweet,
            userID: block.userID,
            tweetID: block.tweetID
          }
        }

      case 'VimeoVideoBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.VimeoVideo,
            videoID: block.videoID
          }
        }

      case 'YouTubeVideoBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.YouTubeVideo,
            videoID: block.videoID
          }
        }

      case 'SoundCloudTrackBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.SoundCloudTrack,
            trackID: block.trackID
          }
        }

      case 'BildwurfAdBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.BildwurfAd,
            zoneID: block.zoneID
          }
        }

      case 'EmbedBlock':
        return {
          type: BlockType.Embed,
          key: index,
          value: {
            type: EmbedType.IFrame,
            title: block.title,
            url: block.url,
            width: block.width,
            height: block.height,
            styleCustom: block.styleCustom
          }
        }

      default:
        return {}
    }
  })
}

export function imageAdapter(image: any): ImageData {
  return image
}

export function imageEdgeToMedia(imageEdge: any): ImageData {
  return {
    ...imageEdge.image,
    caption: imageEdge.caption
  }
}

export function listicleToListical(listicle: any): ListicalItem {
  return listicle.map((listItem: any) => {
    return {
      title: listItem.title,
      text: listItem.richText,
      image: listItem.image && imageAdapter(listItem.image)
    }
  })
}
