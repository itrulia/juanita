import {
  ApolloClient,
  NormalizedCacheObject,
  InMemoryCache,
  createHttpLink,
  ApolloLink
} from '@apollo/client'
import {useMemo} from 'react'
import {onError} from '@apollo/client/link/error'
import * as Sentry from '@sentry/nextjs'

let apolloClient

export const getApolloClient = (ctx?: any, initialState?: NormalizedCacheObject) => {
  const authLink = new ApolloLink((operation, forward) => {
    if (ctx) {
      const context = operation.getContext()
      const newContext = {
        headers: {
          cookie: ctx.headers.cookie,
          ...context.headers
        },
        ...context
      }
      operation.setContext(newContext)
    }

    return forward(operation)
  })
  const ssrMode = typeof window === 'undefined'

  const uri = ssrMode // TODO: improve this
    ? process.env.API_URL_SSR
      ? process.env.API_URL_SSR
      : `http://${process.env.MY_POD_IP}:${process.env.MY_POD_IP_PORT}/api/proxy`
    : `${process.env.NEXT_PUBLIC_WEBSITE_URL}/api/proxy`

  const httpLink = createHttpLink({
    uri,
    //uri: `${process.env.NEXT_PUBLIC_WEBSITE_URL}/api/proxy`,
    credentials: 'include'
  })
  const cache = new InMemoryCache().restore(initialState || {})

  const ErrorLink = onError(({graphQLErrors, networkError, operation, forward}) => {
    if (graphQLErrors) {
      graphQLErrors.forEach(graphQLError => {
        Sentry.captureException(graphQLError)
      })
      if (networkError) {
        Sentry.captureException(networkError)
      }
    }
    forward(operation)
  })

  return new ApolloClient({
    link: authLink.concat(ErrorLink).concat(httpLink),
    cache
  })
}

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? getApolloClient()

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract()
    // Restore the cache using the data passed from getStaticProps/getServerSideProps
    // combined with the existing cached data
    _apolloClient.cache.restore({...existingCache, ...initialState})
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient

  return _apolloClient
}
{
}
export function useApollo(initialState) {
  return useMemo(() => initializeApollo(initialState), [initialState])
}
