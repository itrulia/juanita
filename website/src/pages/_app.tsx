import '../../styles/globals.scss'
import {ApolloProvider} from '@apollo/client'
import {ThemeProvider} from 'styled-components'
import {CookiesProvider} from 'react-cookie'
import {theme} from '../../tailwind.config'
import {useApollo} from '../withApollo'
import {AppContext, AppProps} from 'next/app'
import App from 'next/app'
import {useRouter} from 'next/router'
import {useEffect} from 'react'
import {SessionProvider} from 'next-auth/react'
import {AppProvider} from '../utils/AppContext.'
import {appWithTranslation} from 'next-i18next'

import 'react-toastify/dist/ReactToastify.css'
import {FullNavigationFragment} from '../@types/codegen/graphql'
import {ssrNavigationList} from '../@types/codegen/page'
import {TwitterProvider} from '../components/atoms/twitterEmbed'
import {InstagramProvider} from '../components/atoms/instagramEmbed'
import {createInstance, MatomoProvider, useMatomo} from '@datapunt/matomo-tracker-react'

function MainApp({children}) {
  const router = useRouter()
  const {trackPageView} = useMatomo()

  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      trackPageView({})
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return children
}

function MyApp({Component, pageProps: {session, ...pageProps}}: AppProps) {
  const router = useRouter()
  const apolloClient = useApollo(pageProps.initialApolloState)

  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      // @ts-ignore
      if (window && window.gtag) {
        // @ts-ignore
        window.gtag('config', process.env.NEXT_PUBLIC_GA_TRACKING_ID, {
          page_path: url
        })
      }
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  const instance = createInstance({
    urlBase: process.env.NEXT_PUBLIC_MATOMO_BASE_URL,
    siteId: parseInt(process.env.NEXT_PUBLIC_MATOMO_ID)
  })

  return (
    <ApolloProvider client={apolloClient}>
      <ThemeProvider theme={theme}>
        <CookiesProvider>
          <SessionProvider session={session}>
            <AppProvider>
              <TwitterProvider>
                <InstagramProvider>
                  <MatomoProvider value={instance}>
                    <MainApp>
                      <Component {...pageProps} />
                    </MainApp>
                  </MatomoProvider>
                </InstagramProvider>
              </TwitterProvider>
            </AppProvider>
          </SessionProvider>
        </CookiesProvider>
      </ThemeProvider>
    </ApolloProvider>
  )
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//

export interface NavsProps {
  aboutUsNavigation: FullNavigationFragment
  categoriesNavigation: FullNavigationFragment
  eventsNavigation: FullNavigationFragment
  fokusMonatNavigation: FullNavigationFragment
  footerNavigation: FullNavigationFragment
  ribbonNavigation: FullNavigationFragment
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`

  const appProps = await App.getInitialProps(appContext)
  const navigationResponse = await ssrNavigationList.getServerPage({}, appContext.ctx.req)
  const {
    aboutUsNavigation,
    categoriesNavigation,
    eventsNavigation,
    fokusMonatNavigation,
    footerNavigation,
    ribbonNavigation
  } = navigationResponse.props.data
  const navs: NavsProps = {
    aboutUsNavigation,
    categoriesNavigation,
    eventsNavigation,
    fokusMonatNavigation,
    footerNavigation,
    ribbonNavigation
  }
  return Object.assign({}, appProps, {pageProps: {navs}})
}

export default appWithTranslation(MyApp)
