import Custom500 from './500'

function Error({statusCode}) {
  console.log('statuscode', statusCode) //TODO: check sentry log
  return (
    <>
      <Custom500 />
    </>
  )
}

Error.getInitialProps = ({res, err}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return {statusCode}
}

export default Error
