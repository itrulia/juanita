import {getSession} from 'next-auth/react'

export async function getServerSideProps({req, query}) {
  const session = await getSession({req})

  if (query.error === 'Verification' && session) {
    console.log('Verification failed but user has session')
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    }
  }

  return {
    redirect: {
      destination: `/?showLogin=true&errorCode=${query.error}`,
      permanent: false
    }
  }
}

export default function Error() {
  return null
}
