interface AuthErrorQuery {
  error?: string
  callbackUrl?: string
}

interface AuthErrorGetServerSideProps {
  query?: AuthErrorQuery
}

export async function getServerSideProps(props: AuthErrorGetServerSideProps) {
  const {query} = props

  if (query.callbackUrl?.includes('/account/profile')) {
    return {
      redirect: {
        destination: `/account/profile/?socialLinkError=${query.error}`
      }
    }
  }

  return {
    redirect: {
      destination: `/?showLogin=true&errorCode=${query.error}`,
      permanent: false
    }
  }
}

export default function Signin() {
  return null
}
