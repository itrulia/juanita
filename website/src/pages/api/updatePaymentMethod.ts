import {NextApiRequest, NextApiResponse} from 'next'
import Stripe from 'stripe'

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {apiVersion: '2020-08-27'})

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {customerID, pm} = req.query

  if (!pm) return res.status(400).json({error: 'Bad input. pm "PaymentMethod ID" is required'})

  if (!customerID) {
    const newCustomer = await stripe.customers.create({
      payment_method: pm as string,
      invoice_settings: {
        default_payment_method: pm as string
      }
    })
    res.status(200).json({customerID: newCustomer.id})
  } else {
    await stripe.paymentMethods.attach(pm as string, {
      customer: customerID as string
    })
    await stripe.customers.update(customerID as string, {
      invoice_settings: {
        default_payment_method: pm as string
      }
    })
    res.status(200).json({customerID})
  }
}
