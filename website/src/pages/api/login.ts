import {NextApiRequest, NextApiResponse} from 'next'
import {GraphQLClient} from 'graphql-request'
import {getSdk} from '../../@types/codegen/api'
import cookie from 'cookie'
import * as Sentry from '@sentry/nextjs'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const client = new GraphQLClient(process.env.API_URL)
  const sdk = getSdk(client)

  const {jwt, next} = req.query //TODO: implement error handling

  if (!jwt) {
    res.redirect('/')
    return
  }
  try {
    const {createSessionWithJWT} = await sdk.CreateSessionWithJWT({
      jwt: jwt as string
    })

    if (createSessionWithJWT.token) {
      res.setHeader(
        'Set-Cookie',
        cookie.serialize('juanita-next-auth.session-token', createSessionWithJWT.token, {
          path: '/',
          secure: true,
          httpOnly: true,
          sameSite: 'lax'
        })
      )
      res.redirect(typeof next === 'string' ? next : '/')
      return
    }
  } catch (error) {
    console.error(error)
    Sentry.captureException(error)
  }
  res.redirect('/')
}
