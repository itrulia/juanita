import NextAuth from 'next-auth'
import FormData from 'form-data'
import * as Sentry from '@sentry/nextjs'
import GoogleProvider from 'next-auth/providers/google'
// import CredentialsProvider from 'next-auth/providers/credentials'
import EmailProvider from 'next-auth/providers/email'
import FacebookProvider from 'next-auth/providers/facebook'
import TwitterProvider from 'next-auth/providers/twitter'

import {wepublishNextAuthAdapter} from '../../../utils/WepublishNextAuthAdapter'
import mongoClient from '../../../utils/mongodb'

export default NextAuth({
  // Configure one or more authentication providers
  secret: process.env.NEXTAUTH_SECRET,
  providers: [
    GoogleProvider({
      clientId: process.env.OAUTH_GOOGLE_CLIENT_ID,
      clientSecret: process.env.OAUTH_GOOGLE_CLIENT_KEY
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET
    }),
    TwitterProvider({
      clientId: process.env.TWITTER_CLIENT_ID,
      clientSecret: process.env.TWITTER_CLIENT_SECRET
    }),
    /* CredentialsProvider({
      name: 'Credentials',
      credentials: {
        username: { label: "Username", type: 'text'},
        password: {label: "Password", type: 'password'}
      },
      async authorize(credentials, req) {
        const client = await dbAdapter as MongoDBAdapter
        const user = await client.user.getUserForCredentials({email: credentials.username, password: credentials.password})
        return user && user.active ? {
          id: user.id,
          emailVerified: new Date(),
          name: user.name,
          email: user.email
        } : null
      }
    }), */
    EmailProvider({
      server: {},
      secret: process.env.NEXTAUTH_SECRET,
      async sendVerificationRequest(props) {
        const {identifier: email, url} = props
        try {
          const auth = Buffer.from(`api:${process.env.MAILGUN_API_KEY}`).toString('base64')

          await new Promise((resolve, reject) => {
            const form = new FormData()
            form.append('from', 'Dein Stadtmagazin do-not-reply@tsri.ch')
            form.append('to', email)
            form.append('subject', `Tsüri.ch Login Link`)
            form.append('template', 'juanita_login_link')
            form.append('v:loginurl', url)
            form.append('o:tracking-clicks', 'htmlonly')
            form.submit(
              {
                protocol: 'https:',
                host: process.env.MAILGUN_BASE_DOMAIN,
                path: `/v3/${process.env.MAILGUN_MAIL_DOMAIN}/messages`,
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  Authorization: `Basic ${auth}`
                }
              },
              (err, res) => {
                return err || res.statusCode !== 200 ? reject(err || res) : resolve({})
              }
            )
          })
        } catch (error) {
          Sentry.captureException(error)
        }
      }
    })
  ],
  adapter: wepublishNextAuthAdapter(mongoClient),
  session: {
    //jwt: false,
    strategy: 'database',
    //maxAge: 30 * 24 * 60 * 60, // 30 days
    maxAge: 90 * 24 * 60 * 60, // 90 days
    updateAge: 24 * 60 * 60 // 24 hours
  },
  debug: false,
  callbacks: {
    async signIn({user, account, profile, email, credentials}) {
      const client = await mongoClient
      const tsriUser = await client.user.getUser(user.email)
      if (tsriUser) return true
      return `${process.env.NEXT_PUBLIC_WEBSITE_URL}/?showLogin=true&errorCode=OAuthUserNotExist` //TODO: add message for user that he/she should sign up
    },
    async session({session, user, token}) {
      return Object.assign(session, {
        user: {...session.user, subscriptionState: user.subscriptionState}
      })
    }
  },
  pages: {
    signIn: '/auth/signin',
    error: '/auth/error'
  },
  theme: {
    colorScheme: 'light', // "auto" | "dark" | "light"
    brandColor: '#36ADDF', // Hex color value
    logo: 'https://tsri.ch/logo.png' // Absolute URL to logo image
  },
  cookies: {
    sessionToken: {
      name: `juanita-next-auth.session-token`,
      options: {
        httpOnly: true,
        sameSite: 'lax',
        path: '/',
        secure: true
      }
    }
  }
})
