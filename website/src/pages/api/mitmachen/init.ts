import {NextApiRequest, NextApiResponse} from 'next'
import NextCors from 'nextjs-cors'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json({
    sentry: {
      dsn: process.env.SENTRY_JOIN_DSN,
      environment: process.env.NEXT_PUBLIC_HOST_ENV,
      release: process.env.SENTRY_RELEASE
    },
    data: {
      stripeApiKey: process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY
    }
  })
}
