import {NextApiRequest, NextApiResponse} from 'next'
import * as Sentry from '@sentry/nextjs'
import {MongoDBAdapter} from '@wepublish/api-db-mongodb'
import {
  DateFilterComparison,
  InputCursorType,
  LimitType,
  SortOrder,
  SubscriptionSort
} from '@wepublish/api'

const dbAdapter = MongoDBAdapter.connect({
  url: process.env.MONGO_URL,
  locale: process.env.MONGO_LOCALE ?? 'en'
})

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const oneDay = 24 * 60 * 60 * 1000 // hours*minutes*seconds*milliseconds
  const firstDate = new Date().getTime()
  const secondDate = new Date(2015, 1, 18).getTime()
  const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay))

  let members: number = 0
  try {
    const client = await dbAdapter
    const subscriptions = await client.subscription.getSubscriptions({
      cursor: {type: InputCursorType.None},
      limit: {count: 10, type: LimitType.First, skip: 0},
      order: SortOrder.Ascending,
      sort: SubscriptionSort.CreatedAt,
      filter: {
        paidUntil: {comparison: DateFilterComparison.GreaterThanOrEqual, date: new Date()}
      }
    })
    members = subscriptions.totalCount
  } catch (error) {
    console.log('error', error)
    Sentry.captureException(error)
  }

  res.status(200).json({
    climateMembers: 0,
    members,
    employees: 14,
    daysAlive: diffDays,
    averageSalary: 4000,
    earnedMoney: 0
  })
}
