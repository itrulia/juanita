import httpProxyMiddleware from 'next-http-proxy-middleware'
import {NextApiRequest, NextApiResponse} from 'next'
import {parseCookies} from '../../helpers'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const cookies = parseCookies(req)
  const authToken = cookies['juanita-next-auth.session-token']
  return httpProxyMiddleware(req, res, {
    // You can use the `http-proxy` option
    target: process.env.API_URL,
    // In addition, you can use the `pathRewrite` option provided by `next-http-proxy-middleware`
    headers: {
      authorization: authToken ? `Bearer ${authToken}` : ''
    }
  })
}
