import {getSession, signIn} from 'next-auth/react'
import {ssrMe, ssrMeAndMySubscriptions} from '../../@types/codegen/page'
import * as Sentry from '@sentry/nextjs'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import React, {useEffect, useState} from 'react'
import {
  FullSubscriptionFragment,
  FullUserFragment,
  useUpdateUserMutation
} from '../../@types/codegen/graphql'
import {Trans, useTranslation} from 'next-i18next'
import {toast} from 'react-toastify'
import {Loader} from '../../components/atoms/loader'
import Section from '../../components/Section'
import Info from '../../components/Info'
import AccountLayout from '../../components/AccountLayout'
import Button, {ButtonType} from '../../components/atoms/Button'
import {useRouter} from 'next/router'

export interface ProfileProps {
  user: FullUserFragment
  subscription?: FullSubscriptionFragment
  navs: any
}

export async function getServerSideProps({req, query, locale}) {
  const session = await getSession({req})
  if (!session) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }

  try {
    const userSubscriptionResponse = await ssrMeAndMySubscriptions.getServerPage({}, req)
    const {data: userAndSubscription} = userSubscriptionResponse.props

    if (!userAndSubscription?.me) {
      return {
        redirect: {
          destination: '/login',
          permanent: false
        }
      }
    }

    //TODO: what happens with users that don't have a subscription???
    const subscription = userAndSubscription.subscriptions[0]

    if (subscription?.deactivation) {
      const deactivationDate = new Date(subscription.deactivation.date)
      if (deactivationDate <= new Date()) {
        return {
          redirect: {
            destination: '/account/renew',
            permanent: false
          }
        }
      }
    }

    return {
      props: {
        user: userAndSubscription.me,
        subscription: subscription || null,
        ...(await serverSideTranslations(locale, ['common']))
      }
    }
  } catch (error) {
    return {
      redirect: {
        destination: '/account/error',
        permanent: false
      }
    }
  }
}

export default function Profile({user, subscription, navs}: ProfileProps) {
  const router = useRouter()
  const [userDetails, setUserDetails] = useState<FullUserFragment>(user)
  const [hasGoogle] = useState<boolean>(
    user.oauth2Accounts.some(account => account.provider === 'google')
  )
  const [hasTwitter] = useState<boolean>(
    user.oauth2Accounts.some(account => account.provider === 'twitter')
  )
  const [hasFacebook] = useState<boolean>(
    user.oauth2Accounts.some(account => account.provider === 'facebook')
  )
  const [isUpdating, setIsUpdating] = useState(false)
  const [updateUserDetails, {loading: isLoadingUpdateUser, error}] = useUpdateUserMutation()

  const {t} = useTranslation('common')

  async function handleSave() {
    try {
      const response = await updateUserDetails({
        variables: {
          input: {
            email: userDetails.email,
            name: userDetails.name,
            preferredName: userDetails.preferredName,
            address: {
              streetAddress: userDetails.address?.streetAddress || '',
              streetAddress2: userDetails.address?.streetAddress2 || '',
              zipCode: userDetails.address?.zipCode || '',
              city: userDetails.address?.city || '',
              country: userDetails.address?.country || ''
            }
          }
        }
      })
      const {updateUser} = response.data
      setUserDetails(updateUser)
      setIsUpdating(false)
    } catch (error) {
      console.log('error', error)
      toast(t('account.errorToast'))
      Sentry.captureException(error)
    }
  }

  useEffect(() => {
    if (router.query?.hasOwnProperty('socialLinkError')) {
      const {socialLinkError} = router.query
      toast.error(
        t(
          socialLinkError === 'OAuthAccountNotLinked'
            ? 'account.linkingErrorOauthNotLinked'
            : 'account.linkingErrorDefault',
          {error: socialLinkError}
        )
      )
      router.push('/account/profile', undefined, {shallow: true})
    }
  }, [router.query])

  const [subscriptionNeedsAction, setSubscriptionNeedsAction] = useState(false)

  useEffect(() => {
    if (subscription) {
      const paidUntilDate = new Date(subscription.paidUntil)
      setSubscriptionNeedsAction(paidUntilDate < new Date())
    }
  }, [subscription])

  return (
    <AccountLayout navs={navs} user={user} showNav={true}>
      {subscriptionNeedsAction && (
        <div
          onClick={() => {
            router.push('/account/subscription')
          }}
          className="p-4 mt-4 w-full text-sm bg-red rounded-lg cursor-pointer"
          role="alert">
          <span className="font-medium">{t('loginModal.attention')}</span>{' '}
          {t('loginModal.subscriptionNeedsYourAttention')}
        </div>
      )}
      <Loader isLoading={isLoadingUpdateUser} />
      <Section
        title={t('account.address')}
        onSave={handleSave}
        isUpdating={isUpdating}
        toggleUpdating={setIsUpdating}>
        <Info
          isUpdating={isUpdating}
          label={t('account.name')}
          name="name"
          value={userDetails?.name}
          onChange={e => setUserDetails({...userDetails, name: e.target.value})}
        />
        <Info
          isUpdating={isUpdating}
          label={t('account.preferredName')}
          name="preferredName"
          value={userDetails?.preferredName}
          onChange={e => setUserDetails({...userDetails, preferredName: e.target.value})}
        />
        <Info
          isUpdating={isUpdating}
          label={t('account.streetAddress')}
          name="streetAddress"
          value={userDetails.address?.streetAddress}
          onChange={e =>
            setUserDetails({
              ...userDetails,
              address: {
                ...userDetails.address,
                streetAddress: e.target.value
              }
            })
          }
        />
        <Info
          isUpdating={isUpdating}
          label={t('account.zip')}
          name="zipCode"
          value={userDetails.address?.zipCode}
          onChange={e =>
            setUserDetails({
              ...userDetails,
              address: {
                ...userDetails.address,
                zipCode: e.target.value
              }
            })
          }
        />
        <Info
          isUpdating={isUpdating}
          label={t('account.city')}
          name="city"
          value={userDetails.address?.city}
          onChange={e =>
            setUserDetails({
              ...userDetails,
              address: {
                ...userDetails.address,
                city: e.target.value
              }
            })
          }
        />
        <Info
          isUpdating={isUpdating}
          label={t('account.country')}
          name="country"
          value={userDetails.address?.country}
          onChange={e =>
            setUserDetails({
              ...userDetails,
              address: {
                ...userDetails.address,
                country: e.target.value
              }
            })
          }
        />
      </Section>
      <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
      {/* TODO: readonly as API won't accept changing it  */}
      <Section
        title={t('account.email')}
        // onSave={onSave}
        // isUpdating={isUpdating}
        //   toggleUpdating={setIsUpdating}
      >
        <Info
          type="email"
          isUpdating={false}
          name="email"
          label={t('account.email')}
          value={userDetails?.email}
          onChange={e => setUserDetails({...userDetails, email: e.target.value})}
        />
        <div className="mb-10 w-full">
          <Trans i18nKey="account.emailChangeInfoText">
            {'fake text'}
            <a
              className="underline"
              href=" mailto:seraina.manser@tsri.ch?subject=E-Mail-Adresse%20%C3%A4ndern">
              Name
            </a>
            {'more  fake text'}
          </Trans>
        </div>
      </Section>
      <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
      <Section title={t('account.socialLink')}>
        <div className="mb-10 w-full">
          <p>{t('account.socialLinkHelpInfo')}</p>
        </div>
        <Button
          className="mt-4 w-full disabled:opacity-50 disabled:cursor-not-allowed"
          disabled={hasGoogle}
          type={ButtonType.Primary}
          onClick={() => {
            signIn('google', {callbackUrl: window.location.href})
          }}>
          {t(hasGoogle ? 'account.linkedWithGoogle' : 'account.linkWithGoogle')}
        </Button>
        <Button
          className="mt-4 w-full disabled:opacity-50 disabled:cursor-not-allowed"
          disabled={hasFacebook}
          type={ButtonType.Primary}
          onClick={() => {
            signIn('facebook', {callbackUrl: window.location.href})
          }}>
          {t(hasFacebook ? 'account.linkedWithFacebook' : 'account.linkWithFacebook')}
        </Button>
        <Button
          className="mt-4 w-full disabled:opacity-50 disabled:cursor-not-allowed"
          disabled={hasTwitter}
          type={ButtonType.Primary}
          onClick={() => {
            signIn('twitter', {callbackUrl: window.location.href})
          }}>
          {t(hasTwitter ? 'account.linkedWithTwitter' : 'account.linkWithTwitter')}
        </Button>
      </Section>
    </AccountLayout>
  )
}
