import {
  FullSubscriptionFragment,
  Invoice,
  MemberPlan,
  PaymentPeriodicity,
  useCancelSubscriptionMutation,
  useCheckInvoiceStatusLazyQuery,
  useCreatePaymentFromInvoiceMutation,
  User,
  useUpdatePaymentProviderCustomersMutation,
  useUpdateSubscriptionMutation
} from '../../@types/codegen/graphql'
import React, {useEffect, useMemo, useState} from 'react'
import {useTranslation} from 'next-i18next'
import {toast} from 'react-toastify'
import * as Sentry from '@sentry/nextjs'
import Section from '../../components/Section'
import Info from '../../components/Info'
import dateFormat from '../../utils/dateFormat'
import StripeCardUpdate from '../../components/stripe/StripeCardUpdate'
import Button, {ButtonType} from '../../components/atoms/Button'
import {getSession} from 'next-auth/react'
import {
  ssrCheckInvoiceStatus,
  ssrInvoiceList,
  ssrMeAndMySubscriptions,
  ssrMemberPlanList
} from '../../@types/codegen/page'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import Stripe from 'stripe'
import AccountLayout from '../../components/AccountLayout'
import {useRouter} from 'next/router'
import StripeElement from '../../components/stripe/StripeElement'

import StripePayment from '../../components/stripe/StripePayment'
import {NextApiRequest} from 'next'
import {TrackingEventCategories} from '../../components/utility'
import {useMatomo} from '@datapunt/matomo-tracker-react'
import {handleError, PaymentSlugs, sortCreatedAtDateByNewestFirst} from '../../helpers'
import {InvoiceTable} from '../../components/InvoiceTable'
import {ChooseRightModal} from '../../components/Modal'
import {Loader} from '../../components/atoms/loader'
import Link from 'next/link'

export interface Card {
  brand: string
  last4: string
  exp_month: number
  exp_year: number
}

export interface StripeCustomer {
  customer: Stripe.Customer
  card: Card | null
}

export interface SubscriptionProps {
  user: User
  subscription?: FullSubscriptionFragment
  memberPlans: MemberPlan[]
  invoices: Invoice[]
  stripeCustomer?: StripeCustomer
  payInvoiceNow?: string
  navs: any
}

interface SubscriptionRequestQuery {
  payInvoiceNow?: string
  paymentSuccess?: string
  invoiceID?: string
}

interface SubscriptionGetServerSideProps {
  req: NextApiRequest
  query: SubscriptionRequestQuery
  locale: string
}

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {apiVersion: '2020-08-27'})

export async function getStripeCustomer(customerID): Promise<StripeCustomer> {
  let customer = null
  let card: Card
  try {
    customer = await stripe.customers.retrieve(customerID)
    if (customer?.invoice_settings?.default_payment_method) {
      const pm = await stripe.paymentMethods.retrieve(
        customer?.invoice_settings?.default_payment_method
      )
      card = {
        brand: pm.card.brand,
        last4: pm.card.last4,
        exp_month: pm.card.exp_month,
        exp_year: pm.card.exp_year
      }
    } else if (customer?.default_source) {
      const source = (await stripe.customers.retrieveSource(
        customer.id,
        customer.default_source
      )) as Stripe.Source.Card
      card = {
        brand: source.brand,
        last4: source.last4,
        exp_month: source.exp_month,
        exp_year: source.exp_year
      }
    }
  } catch (error) {
    console.warn('Stripe error', error)
    Sentry.captureException(error)
  }
  return {card, customer}
}

export async function getServerSideProps({
  req,
  query = {},
  locale
}: SubscriptionGetServerSideProps) {
  const session = await getSession({req})

  if (!session) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }

  if (query.paymentSuccess === 'true' && query.hasOwnProperty('invoiceID')) {
    const {invoiceID} = query
    try {
      await ssrCheckInvoiceStatus.getServerPage(
        {
          variables: {id: invoiceID}
        },
        req
      )
    } catch (error) {
      // error isn't critical
      console.warn('could not check invoice')
    }
  }

  try {
    const userSubscriptionResponse = await ssrMeAndMySubscriptions.getServerPage({}, req)
    const {data: userAndSubscription} = userSubscriptionResponse.props

    if (!userAndSubscription?.me) {
      return {
        redirect: {
          destination: '/login',
          permanent: false
        }
      }
    }

    //TODO: what happens with users that don't have a subscription???
    const subscription = userAndSubscription.subscriptions[0]

    if (subscription?.deactivation) {
      const deactivationDate = new Date(subscription.deactivation.date)
      if (deactivationDate <= new Date()) {
        return {
          redirect: {
            destination: '/account/renew',
            permanent: false
          }
        }
      }
    }

    const memberPlanListResponse = await ssrMemberPlanList.getServerPage(
      {
        variables: {
          first: 10
        }
      },
      req
    )
    const {data: memberPlansList} = memberPlanListResponse.props
    if (memberPlansList.memberPlans.nodes.length === 0) {
      throw new Error('API did not return any MemberPlans')
    }
    const invoicesResponse = await ssrInvoiceList.getServerPage({}, req)
    const {
      data: {invoices}
    } = invoicesResponse.props

    const customerID = userAndSubscription.me.paymentProviderCustomers.find(
      ppc => ppc.paymentProviderID === 'stripe'
    )?.customerID
    let customer = null
    let card: Card
    if (customerID) {
      const cardAndCustomer = await getStripeCustomer(customerID)
      customer = cardAndCustomer.customer
      card = cardAndCustomer.card
    }
    return {
      props: {
        user: userAndSubscription?.me,
        subscription: subscription || null,
        invoices: invoices.slice().sort(sortCreatedAtDateByNewestFirst),
        ...(query.payInvoiceNow ? {payInvoiceNow: query.payInvoiceNow} : {}),
        stripeCustomer: customer
          ? {
              customer,
              card: card || null
            }
          : null,
        memberPlans: memberPlansList.memberPlans.nodes,
        ...(await serverSideTranslations(locale, ['common']))
      }
    }
  } catch (error) {
    Sentry.captureException(error)
    console.log('error', error)
    return {
      redirect: {
        destination: '/account/error',
        permanent: false
      }
    }
  }
}

interface IntentAndInvoice {
  intentSecret: string
  invoiceID: string
}

export default function Subscription({
  user,
  subscription,
  memberPlans,
  invoices: invoicesList,
  payInvoiceNow,
  stripeCustomer,
  navs
}: SubscriptionProps) {
  const [isUpdating, setIsUpdating] = useState(false)
  const [isUpdatingCard, setIsUpdatingCard] = useState(false)
  const [subscriptionDetails, setSubscriptionDetails] = useState<
    FullSubscriptionFragment | undefined
  >(subscription)
  const [paymentMethod, setPaymentMethod] = useState(subscription?.paymentMethod)
  const [monthlyAmount, setMonthlyAmount] = useState<number | undefined>(
    subscription ? Math.floor(subscription.monthlyAmount / 100) : undefined
  )
  const [stripeCustomerClient, setStripeCustomerClient] = useState<StripeCustomer | undefined>(
    stripeCustomer
  )

  // TODO: what happens if the monthlyAmount is not a whole number
  const tsriMemberPlan: MemberPlan = memberPlans[0] // TODO: error handling
  const [invoices, setInvoices] = useState<Invoice[]>(invoicesList)
  const [invoiceToPay, setInvoiceToPay] = useState<string | null>(null)
  const [intentSecret, setIntentSecret] = useState<IntentAndInvoice | null>(null)
  const [loading, setLoading] = useState<boolean>(false)

  const {t} = useTranslation('common')
  const router = useRouter()
  const {trackEvent} = useMatomo()
  const [updatePaymentProviderCustomers] = useUpdatePaymentProviderCustomersMutation()
  const [updateSubscriptionDetails] = useUpdateSubscriptionMutation()
  const [checkInvoiceStatus, {loading: invoiceChecking}] = useCheckInvoiceStatusLazyQuery({
    fetchPolicy: 'no-cache',
    onCompleted: data => {
      if (data.checkInvoiceStatus.paidAt) {
        // reload page to also update the credit card information which can only be fetched server-side because of the
        // secret stripe key, which cannot be exposed publicly.
        router.reload()
      }
    },
    onError: error => handleError({errorText: error.message})
  })
  const [createPaymentFromInvoice] = useCreatePaymentFromInvoiceMutation()
  const [cancelUserSubscription] = useCancelSubscriptionMutation()

  /**
   * compute open invoice out of invoices
   */
  const openInvoice: Invoice | null = useMemo(() => {
    const openInvoice =
      invoices?.find(invoice => invoice.paidAt === null && invoice.canceledAt === null) || null
    return openInvoice
  }, [invoices])

  /**
   * on mounted
   */
  useEffect(() => {
    checkOpenInvoices()
    checkUpdatedCreditCard()
  }, [])

  /**
   * Indicate loading, when checking invoice
   */
  useEffect(() => {
    if (invoiceChecking) {
      setLoading(true)
    } else {
      setLoading(false)
    }
  }, [invoiceChecking])

  useEffect(() => {
    if (subscriptionDetails?.paymentMethod) {
      setPaymentMethod(subscriptionDetails.paymentMethod)
    }
  }, [subscriptionDetails])

  async function checkUpdatedCreditCard() {
    const creditCardUpdated = router.query?.creditCard
    if (!creditCardUpdated) return
    toast(
      'Danke für die Aktualisierung deiner Kreditkarte. Falls du eine offene Rechnung hast, wird diese in ' +
        'den kommenden Tagen automatisch belastet. Du musst nichts weiter unternehmen.',
      {
        type: 'success',
        autoClose: false,
        position: 'top-left',
        style: {width: '100%', backgroundColor: 'black', color: 'white'}
      }
    )
    await router.replace({query: undefined})
  }

  /**
   * Check api, if invoices have been paid in the meantime.
   */
  async function checkOpenInvoices() {
    // skip, if no open invoice
    for (const invoice of invoices) {
      // invoice paid or cancelled. No need to check it.
      if (invoice.paidAt || invoice.canceledAt) {
        continue
      }
      checkInvoiceStatus({variables: {id: invoice.id}})
    }
  }

  const handleSave = async (executePaymentAction: boolean = false) => {
    try {
      const response = await updateSubscriptionDetails({
        variables: {
          id: subscription.id,
          input: {
            id: subscription.id,
            autoRenew: subscriptionDetails.autoRenew,
            monthlyAmount: monthlyAmount * 100,
            memberPlanID: subscriptionDetails.memberPlan?.id,
            paymentMethodID: subscriptionDetails.paymentMethod?.id,
            paymentPeriodicity: subscriptionDetails.paymentPeriodicity
          }
        }
      })
      const {updateUserSubscription} = response.data
      // @ts-ignore
      setSubscriptionDetails(updateUserSubscription)
      setIsUpdating(false)
      router.reload()
    } catch (error) {
      toast(t('account.errorToast'))
      Sentry.captureException(error)
    }
  }

  const handleRemoveCreditCard = async () => {
    if (confirm(t('account.confirmCardDelete'))) {
      const updatedPaymentProviderCustomers = user.paymentProviderCustomers.filter(
        ppc => ppc.paymentProviderID !== 'stripe'
      )
      await updatePaymentProviderCustomers({
        variables: {
          input: updatedPaymentProviderCustomers
        }
      })
      router.reload()
    }
  }

  const handleCancelSubscription = async () => {
    const message = t('account.confirmDeactivation', {
      date: dateFormat(new Date(subscriptionDetails.paidUntil), 'do MMMM yyyy')
    })
    if (confirm(message)) {
      try {
        const response = await cancelUserSubscription({variables: {id: subscription.id}})
        const {cancelUserSubscription: updatedUserSubscription} = response.data
        if (!updatedUserSubscription) throw new Error('Server did not return data')
        setSubscriptionDetails(updatedUserSubscription as FullSubscriptionFragment)
        setIsUpdating(false)
        toast('Dein Abo wurde gekündet.', {type: 'success'})
      } catch (error) {
        handleError({errorText: error})
      }
    }
  }

  async function createPaymentFromInvoiceAndExecuteAction(invoiceID, paymentMethodSlug = null) {
    trackEvent({
      category: TrackingEventCategories.Subscription,
      action: 'subscription-link-payment',
      name: 'payment'
    })
    try {
      const selectedPaymentMethod = tsriMemberPlan.availablePaymentMethods[0].paymentMethods.find(
        pm => pm.slug === paymentMethodSlug
      )

      const paymentResponse = await createPaymentFromInvoice({
        variables: {
          input: {
            invoiceID: invoiceID,
            paymentMethodID: selectedPaymentMethod ? selectedPaymentMethod.id : paymentMethod.id,
            successURL: `${window.location.href}?paymentSuccess=true&invoiceID=${invoiceID}`,
            failureURL: `${window.location.href}?paymentSuccess=false`
          }
        }
      })

      const {createPaymentFromInvoice: payment} = paymentResponse.data
      if (payment.paymentMethod.paymentProviderID === 'stripe') {
        setIntentSecret({
          intentSecret: payment.intentSecret,
          invoiceID: invoiceID
        })
      } else if (payment.intentSecret.startsWith('http')) {
        window.location.href = payment.intentSecret
      }
    } catch (error) {
      handleError({errorText: error})
    }
  }

  useEffect(() => {
    if (payInvoiceNow && paymentMethod?.paymentProviderID === 'payrexx') {
      trackEvent({
        category: TrackingEventCategories.Subscription,
        action: 'mail-link-payment',
        name: 'payment'
      })
      createPaymentFromInvoiceAndExecuteAction(payInvoiceNow).catch(error => {
        Sentry.captureException(error)
      })
    }
  }, [])

  /**
   * User does not have any subscription. Show hint.
   */
  if (!subscriptionDetails) {
    return (
      <>
        <Loader isLoading={loading} />
        <AccountLayout navs={navs} showNav>
          <div className="grid grid-cols-1 md:grid-cols-3 items-center pt-8">
            <div className="text-3xl text-center md:text-left md:col-span-2">
              Du bist noch kein:e Tsüri-Member:in. Werde es jetzt.
            </div>
            <div className="pt-4 md:pt-0 text-center md:text-right">
              <Link href="/mitmachen">
                <Button type={ButtonType.Primary}>Tsüri-Member werden</Button>
              </Link>
            </div>
          </div>
        </AccountLayout>
      </>
    )
  }

  return (
    <>
      <Loader isLoading={loading} />
      <AccountLayout navs={navs} user={user} showNav={true}>
        <ChooseRightModal
          show={intentSecret !== null}
          allowBackgroundClose={false}
          showTopClose={false}
          onClose={() => setIntentSecret(null)}>
          {intentSecret && (
            <StripeElement clientSecret={intentSecret.intentSecret}>
              <StripePayment
                amount={
                  invoices.find(invoice => invoice.id === intentSecret.invoiceID)?.total / 100
                }
                onClose={async () => {
                  setIntentSecret(null)
                  await checkOpenInvoices()
                }}
              />
            </StripeElement>
          )}
        </ChooseRightModal>
        <ChooseRightModal
          show={invoiceToPay !== null}
          allowBackgroundClose={false}
          showTopClose={true}
          onClose={() => setInvoiceToPay(null)}>
          <div className="mt-2 grid-cols-1 content-center">
            <div className="w-full" role="info">
              {t('account.billingWhichPaymentProvider')}
            </div>
            <button
              onClick={() => {
                createPaymentFromInvoiceAndExecuteAction(invoiceToPay, PaymentSlugs.STRIPE_PAYMENT)
                setInvoiceToPay(null)
              }}
              className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full">
              {t('account.billingWithCreditcard')}
            </button>
            <button
              onClick={() => {
                createPaymentFromInvoiceAndExecuteAction(invoiceToPay, PaymentSlugs.PAYREXX_PAYMENT)
                setInvoiceToPay(null)
              }}
              className="bg-tsri text-white border border-transparent px-4 py-2 mt-4 w-full">
              {t('account.billingWithPayrexx')}
            </button>
          </div>
        </ChooseRightModal>
        {openInvoice && (
          <section className="my-10 flex flex-row justify-between flex-wrap">
            <h2 className="w-full md:w-1/4 text-error mb-4">{t('account.billingOpen')}</h2>
            <InvoiceTable
              invoices={[openInvoice]}
              onInvoicePayClick={invoiceID => setInvoiceToPay(invoiceID)}
            />
          </section>
        )}
        <Section title={t('account.subscription')}>
          <Info
            label={t('account.subscription')}
            name="startsAt"
            value={t('account.subscriptionValue', {
              memberSince: dateFormat(new Date(subscriptionDetails.startsAt), 'do MMMM yyyy')
            })}
          />
          <Info
            label={t('account.paidUntil')}
            name="paidUntil"
            value={
              subscriptionDetails.paidUntil
                ? t('account.paidUntilValue', {
                    paidUntil: dateFormat(new Date(subscriptionDetails.paidUntil), 'do MMMM yyyy')
                  })
                : t('account.notYetPaid')
            }
          />
          <Info
            label={t('account.yearlyAmount')}
            name="monthlyAmount"
            value={t('account.yearlyAmountValue', {
              amount: (subscriptionDetails.monthlyAmount * 12) / 100
            })}
          />
          {((subscriptionDetails.deactivation &&
            new Date(subscriptionDetails.deactivation.date) < new Date()) ||
            subscriptionDetails.paidUntil === null ||
            new Date(subscriptionDetails.paidUntil) < new Date()) && (
            <div className="mb-10 w-full">
              <p className="uppercase text-error">{t('account.subscriptionNeedsRenewalTitle')}</p>
              <p className="font-semibold">
                {t(
                  subscriptionDetails.deactivation
                    ? 'account.subscriptionNeedsRenewalDeactivated'
                    : subscriptionDetails.autoRenew ||
                      invoices.some(invoice => invoice.paidAt === null)
                    ? 'account.subscriptionNeedsRenewalPaidUntil'
                    : 'account.subscriptionNeedsRenewalNoAutoRenew'
                )}
              </p>
              {!subscriptionDetails.deactivation &&
                !subscriptionDetails.autoRenew &&
                invoices.every(invoice => invoice.paidAt !== null) && (
                  <Button
                    className="mt-4"
                    type={ButtonType.Primary}
                    buttonType={'button'}
                    onClick={async () => {
                      trackEvent({
                        category: TrackingEventCategories.Subscription,
                        action: 'renew-now-button',
                        name: 'payment'
                      })
                      await handleSave(true)
                    }}>
                    {t('account.renewMembership')}
                  </Button>
                )}
            </div>
          )}
        </Section>
        <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
        <Section
          buttons={{
            saveButtonText: subscriptionDetails.deactivation
              ? t('account.reactivateMembership')
              : undefined
          }}
          onSave={handleSave}
          isUpdating={isUpdating}
          toggleUpdating={setIsUpdating}
          title={t('account.subscriptionSettings')}>
          {isUpdating && (
            <div className="mb-10 w-full">
              <p className="uppercase">{t('account.subscriptionChangeWillApplyTitle')}</p>
              <p className="font-semibold">{t('account.subscriptionChangeWillApply')}</p>
            </div>
          )}
          <div className="mb-10 w-full lg:w-1/2 lg:pr-2">
            <p className="uppercase">
              {t('account.monthlyAmount', {
                minAmount: subscriptionDetails.memberPlan.amountPerMonthMin / 100
              })}
            </p>
            {isUpdating ? (
              <div className="pt-2 pl-2 inline-flex space-x-4">
                <input
                  type="range"
                  min={subscriptionDetails.memberPlan.amountPerMonthMin / 100}
                  max="50"
                  value={monthlyAmount}
                  onChange={e => {
                    setMonthlyAmount(parseInt(e.target.value))
                  }}
                />
                <p>{t('account.monthlyAmountValue', {amount: monthlyAmount})}</p>
              </div>
            ) : (
              <p className="font-semibold">
                {t('account.monthlyAmountValue', {amount: monthlyAmount})}
              </p>
            )}
          </div>
          <div className="mb-10 w-full lg:w-1/2">
            <p className="uppercase">{t('account.paymentInterval')}</p>
            {isUpdating ? (
              <select
                className="w-full border-solid border-2 p-2"
                name="paymentPeriodicity"
                value={subscriptionDetails.paymentPeriodicity}
                onChange={e => {
                  const paymentPeriodicity = tsriMemberPlan.availablePaymentMethods[0].paymentPeriodicities.find(
                    apm => apm === e.target.value
                  )
                  const autoRenew =
                    paymentPeriodicity === PaymentPeriodicity.Monthly
                      ? true
                      : subscriptionDetails.autoRenew
                  setSubscriptionDetails({
                    ...subscriptionDetails,
                    paymentPeriodicity,
                    autoRenew
                  })
                }}>
                {tsriMemberPlan.availablePaymentMethods[0].paymentPeriodicities.map((pp, index) => {
                  return (
                    <option value={pp} key={index}>
                      {t(`account.${pp}`)}
                    </option>
                  )
                })}
              </select>
            ) : (
              <p className="font-semibold">
                {t(`account.${subscriptionDetails.paymentPeriodicity}`)}
              </p>
            )}
          </div>
          <div className="mb-10 w-full lg:w-1/2 lg:pr-2">
            <p className="uppercase">{t('account.autoRenew')}</p>
            {isUpdating ? (
              <>
                <input
                  checked={subscriptionDetails.autoRenew}
                  disabled={subscriptionDetails.paymentPeriodicity === PaymentPeriodicity.Monthly}
                  type="checkbox"
                  className="w-1/6 border-solid border-2 p-2"
                  name="autoRenew"
                  id="autoRenew"
                  onChange={e => {
                    const obj = {...subscriptionDetails, autoRenew: e.target.checked}
                    setSubscriptionDetails(obj)
                  }}
                />
                <label htmlFor="autoRenew">{t('account.autoRenewTrue')}</label>
              </>
            ) : (
              <p className="font-semibold">
                {subscriptionDetails.autoRenew && !subscriptionDetails.deactivation
                  ? t('account.autoRenewTrue')
                  : t('account.autoRenewFalse', {
                      endDate: dateFormat(
                        new Date(
                          subscriptionDetails?.deactivation?.date ?? subscriptionDetails.paidUntil
                        ),
                        'do MMMM yyyy'
                      )
                    })}
              </p>
            )}
          </div>
          <div className="mb-10 w-full lg:w-1/2">
            <p className="uppercase">{t('account.paymentMethod')}</p>
            {isUpdating ? (
              <select
                className="w-full border-solid border-2 p-2"
                name="paymentMethod"
                value={subscriptionDetails.paymentMethod.id}
                onChange={e => {
                  const paymentMethod = tsriMemberPlan.availablePaymentMethods[0].paymentMethods.find(
                    pm => pm.id === e.target.value
                  )
                  setSubscriptionDetails({...subscriptionDetails, paymentMethod})
                }}>
                {tsriMemberPlan.availablePaymentMethods[0].paymentMethods.map(pm => {
                  return (
                    <option value={pm.id} key={pm.id}>
                      {pm.description || pm.name}
                    </option>
                  )
                })}
              </select>
            ) : (
              <p className="font-semibold">
                {subscriptionDetails.paymentMethod.description ||
                  subscriptionDetails.paymentMethod.name}
              </p>
            )}
          </div>
          {isUpdating && (
            <div className="mb-10 w-full">
              {!subscriptionDetails.deactivation &&
                new Date(subscriptionDetails.paidUntil) > new Date() && (
                  <Button type={ButtonType.Danger} onClick={() => handleCancelSubscription()}>
                    {t('account.deactivateSubscription')}
                  </Button>
                )}
            </div>
          )}
        </Section>
        <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
        <section className="my-10 flex flex-row justify-between flex-wrap">
          <h2 className="w-full md:w-1/4">{t('account.creditCard')}</h2>
          <div className="w-full md:w-3/4 flex flex-wrap mt-2 md:p-2 mb-10">
            {!isUpdatingCard ? <p className="uppercase">{t('account.creditCardNumber')}</p> : null}
            <div className="relative mb-10 w-full">
              <div className="absolute w-full">
                {isUpdatingCard ? (
                  <StripeElement>
                    <StripeCardUpdate
                      stripeCustomer={stripeCustomerClient}
                      paymentProviderCustomers={user.paymentProviderCustomers}
                      onClose={async reload => {
                        setIsUpdatingCard(false)
                        await router.replace({query: {creditCard: 'updated'}})
                        if (reload) router.reload()
                      }}
                    />
                  </StripeElement>
                ) : (
                  <>
                    <p className="font-semibold">
                      {t(
                        stripeCustomerClient && stripeCustomerClient.card
                          ? 'account.activeCreditCard'
                          : 'account.noCreditCard',
                        {
                          cardType: stripeCustomerClient?.card?.brand,
                          last4: stripeCustomerClient?.card?.last4,
                          validTill: `${stripeCustomerClient?.card?.exp_month}/${stripeCustomerClient?.card?.exp_year}`
                        }
                      )}
                    </p>
                    <div className="mt-2">
                      <Button onClick={() => setIsUpdatingCard(true)} type={ButtonType.Secondary}>
                        {t(
                          stripeCustomerClient && stripeCustomerClient.card
                            ? 'account.replaceCard'
                            : 'account.addCard'
                        )}
                      </Button>
                      {stripeCustomerClient && stripeCustomerClient.card && (
                        <Button
                          className="ml-4"
                          type={ButtonType.Danger}
                          onClick={() => handleRemoveCreditCard()}>
                          {t('account.removeCard')}
                        </Button>
                      )}
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </section>
        <div className="bg-gray-400 my-4 mr-2 h-0.5 lg:w-full lg:mt-5" />
        <section className="my-10 flex flex-row justify-between flex-wrap">
          <h2 className="w-full md:w-1/4 mb-4">{t('account.billingHistory')}</h2>
          <InvoiceTable
            invoices={invoices.filter(
              invoice => invoice.paidAt !== null || invoice.canceledAt !== null
            )}
            onInvoicePayClick={invoiceID => setInvoiceToPay(invoiceID)}
          />
        </section>
      </AccountLayout>
    </>
  )
}
