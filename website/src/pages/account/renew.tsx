import {getSession} from 'next-auth/react'
import {ssrMe, ssrMeAndMySubscriptions, ssrMemberPlanList} from '../../@types/codegen/page'
import * as Sentry from '@sentry/nextjs'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import React, {useEffect, useState} from 'react'
import {
  FullUserFragment,
  FullMemberPlanFragment,
  useInvoiceListLazyQuery,
  useCreatePaymentFromInvoiceMutation,
  useUpdateSubscriptionMutation,
  FullSubscriptionFragment
} from '../../@types/codegen/graphql'
import {useTranslation} from 'next-i18next'
import {toast} from 'react-toastify'
import {Loader} from '../../components/atoms/loader'
import AccountLayout from '../../components/AccountLayout'
import {useRouter} from 'next/router'
import {PaymentPeriodicity} from '../../@types/codegen/api'
import {
  getMonthsinNumberFromPaymentPeriodicity,
  handleError,
  PaymentSlugs,
  sortCreatedAtDateByNewestFirst
} from '../../helpers'
import Button, {ButtonType} from '../../components/atoms/Button'
import {TrackingEventCategories} from '../../components/utility'
import {useMatomo} from '@datapunt/matomo-tracker-react'
import StripeElement from '../../components/stripe/StripeElement'
import StripePayment from '../../components/stripe/StripePayment'
import dateFormat from '../../utils/dateFormat'
import {InfoBox} from 'components/atoms/InfoBox'

export interface RenewProps {
  user: FullUserFragment
  subscription?: FullSubscriptionFragment
  tsriMemberPlan: FullMemberPlanFragment
  navs: any
}

export async function getServerSideProps({req, query, locale}) {
  const session = await getSession({req})
  if (!session) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }

  try {
    const userSubscriptionResponse = await ssrMeAndMySubscriptions.getServerPage({}, req)
    const {data: userAndSubscription} = userSubscriptionResponse.props

    if (!userAndSubscription?.me) {
      return {
        redirect: {
          destination: '/login',
          permanent: false
        }
      }
    }

    //TODO: what happens with users that don't have a subscription???
    const subscription = userAndSubscription.subscriptions[0]

    if (!subscription?.deactivation) {
      return {
        redirect: {
          destination: '/account/subscription',
          permanent: false
        }
      }
    } else {
      const deactivationDate = new Date(subscription.deactivation.date)
      if (deactivationDate > new Date()) {
        return {
          redirect: {
            destination: '/account/subscription',
            permanent: false
          }
        }
      }
    }

    const memberPlansResponse = await ssrMemberPlanList.getServerPage({variables: {first: 10}}, req)
    const {data: memberPlans} = memberPlansResponse.props

    // get current memberplan
    const tsriMemberPlan = memberPlans.memberPlans.nodes.find(
      memberPlan => memberPlan.id === subscription.memberPlan.id
    )

    return {
      props: {
        user: userAndSubscription.me,
        subscription: subscription || null,
        tsriMemberPlan,
        ...(await serverSideTranslations(locale, ['common']))
      }
    }
  } catch (error) {
    return {
      redirect: {
        destination: '/account/error',
        permanent: false
      }
    }
  }
}

const noMemberPlanForSubscriptionTxt =
  'Ein Fehler ist aufgetreten: für Deine Subscription ist kein Memberplan verfügbar.'

export default function Renew({user, subscription, tsriMemberPlan, navs}: RenewProps) {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const {t} = useTranslation('common')
  const {trackEvent} = useMatomo()

  const [paymentPeriodicity, setPaymentPeriodicity] = useState<PaymentPeriodicity>(
    PaymentPeriodicity.Yearly
  )
  const [autoRenew, setAutoRenew] = useState<boolean>(true)
  const [monthlyAmount, setMonthlyAmount] = useState<number>(15)
  const [paymentMethod, setPaymentMethod] = useState<string>('')
  const [intentSecret, setIntentSecret] = useState<string | null>(null)

  useEffect(() => {
    if (paymentPeriodicity === PaymentPeriodicity.Monthly) {
      setAutoRenew(true)
    }
  }, [paymentPeriodicity])

  const [updateSubscriptionDetails] = useUpdateSubscriptionMutation()
  const [
    loadInvoices,
    {called, loading, data: invoiceData, error: loadInvoicesError}
  ] = useInvoiceListLazyQuery({fetchPolicy: 'no-cache'})
  const [createPaymentFromInvoice] = useCreatePaymentFromInvoiceMutation()

  useEffect(() => {
    if (invoiceData) {
      const openInvoice = invoiceData.invoices
        .sort(sortCreatedAtDateByNewestFirst)
        .find(invoice => invoice.paidAt === null)
      if (openInvoice) {
        createPaymentFromInvoice({
          variables: {
            input: {
              invoiceID: openInvoice.id,
              paymentMethodSlug: paymentMethod,
              successURL: `${window.location.protocol}//${window.location.host}/account/subscription?paymentSuccess=true&invoiceID=${openInvoice.id}`,
              failureURL: `${window.location.protocol}//${window.location.host}/account/subscription?paymentSuccess=false`
            }
          }
        })
          .then(paymentResponse => {
            if (paymentResponse.data?.createPaymentFromInvoice) {
              setIntentSecret(paymentResponse.data.createPaymentFromInvoice.intentSecret)
              setIsLoading(false)
            } else {
              Sentry.captureMessage(`Invoice with id: ${openInvoice.id} did not return payment`)
              toast.error(t('account.errorToast'), {autoClose: false})
              setIsLoading(false)
            }
          })
          .catch(error => {
            Sentry.captureException(error)
            toast.error(t('account.errorToast'), {autoClose: false})
            setIsLoading(false)
          })
      } else {
        Sentry.captureMessage(`No open invoice found`)
        toast.error(t('account.errorToast'), {autoClose: false})
        setIsLoading(false)
      }
    }
  }, [invoiceData])

  useEffect(() => {
    if (intentSecret && paymentMethod === PaymentSlugs.PAYREXX_PAYMENT) {
      window.location.href = intentSecret
    }
  }, [intentSecret])

  async function handleRenew(paymentMethodSlug: string) {
    // check memberplan is available
    if (!subscription.memberPlan.slug) {
      handleError({errorText: noMemberPlanForSubscriptionTxt})
      return
    }

    setIsLoading(true)
    setPaymentMethod(paymentMethodSlug)
    try {
      const paymentMethod = tsriMemberPlan.availablePaymentMethods[0].paymentMethods.find(
        pm => pm.slug === paymentMethodSlug
      )

      await updateSubscriptionDetails({
        variables: {
          id: subscription.id,
          input: {
            id: subscription.id,
            autoRenew,
            monthlyAmount: monthlyAmount * 100,
            paymentPeriodicity,
            memberPlanID: subscription.memberPlan.id,
            paymentMethodID: paymentMethod.id
          }
        }
      })
      setTimeout(() => {
        loadInvoices({})
      }, 1000)
    } catch (error) {
      toast(t('account.errorToast'))
      Sentry.captureException(error)
    }
  }

  const startDate = new Date(subscription.startsAt)
  const endDate = new Date(subscription.deactivation.date)

  // no memberPlan available
  if (!subscription.memberPlan.slug) {
    return <InfoBox>{noMemberPlanForSubscriptionTxt}</InfoBox>
  }

  return (
    <AccountLayout navs={navs} user={user} showNav={false}>
      <Loader isLoading={isLoading} />

      <h2>{endDate >= new Date('2021-12-15') ? t('account.renew') : t('account.renewOld')}</h2>

      <div className="grid grid-cols-1 md:grid-cols-2 gap-x-6">
        <div className="md:w-5/6">
          <p className="mt-4">
            {endDate >= new Date('2021-12-15')
              ? t('account.renewInfoText', {
                  month: dateFormat(startDate, 'MMMM'),
                  year: dateFormat(startDate, 'yyyy')
                })
              : t('account.renewInfoTextOld', {
                  monthStart: dateFormat(startDate, 'MMMM'),
                  yearStart: dateFormat(startDate, 'yyyy'),
                  monthEnd: dateFormat(endDate, 'MMMM'),
                  yearEnd: dateFormat(endDate, 'yyyy'),
                  amount: subscription.monthlyAmount / 100
                })}
          </p>
        </div>
        {intentSecret === null && (
          <div className="md:w-4/6">
            <div className="grid grid-cols-1">
              <div className="mb-6 mobile:mt-4">
                <p className="uppercase">{t('account.paymentInterval')}</p>
                <select
                  value={paymentPeriodicity}
                  onChange={event => {
                    setPaymentPeriodicity(event.target.value as PaymentPeriodicity)
                  }}
                  className="w-full border-solid border-2 p-2">
                  <option value={PaymentPeriodicity.Monthly}>
                    {t(`account.${PaymentPeriodicity.Monthly}`)}
                  </option>
                  <option value={PaymentPeriodicity.Quarterly}>
                    {t(`account.${PaymentPeriodicity.Quarterly}`)}
                  </option>
                  <option value={PaymentPeriodicity.Yearly}>
                    {t(`account.${PaymentPeriodicity.Yearly}`)}
                  </option>
                </select>
              </div>
              <div className="mb-4">
                <label className="text-gray-700">
                  <input
                    type="checkbox"
                    className="accent-tsri"
                    checked={autoRenew}
                    disabled={paymentPeriodicity === PaymentPeriodicity.Monthly}
                    onChange={() => {
                      setAutoRenew(!autoRenew)
                    }}
                  />
                  <span className="ml-1 uppercase">{t('account.autoRenew')}</span>
                </label>
              </div>
              <div className="mb-6">
                <div className="relative pt-1">
                  <label htmlFor="monthlyAmountSlider" className="form-label">
                    {t('account.monthlyAmountValueNew', {amount: monthlyAmount})}
                  </label>
                  <input
                    type="range"
                    min={subscription.memberPlan.amountPerMonthMin / 100}
                    max="50"
                    step="1"
                    value={monthlyAmount}
                    onChange={event => {
                      setMonthlyAmount(parseInt(event.target.value))
                    }}
                    className="appearance-none w-full h-2 bg-tsri mt-4"
                    id="monthlyAmountSlider"
                  />
                </div>
              </div>
              <div>
                <p className="uppercase w-full">
                  {t('account.subscriptionOverview')}{' '}
                  <span className="float-right">
                    {t('account.subscriptionOverviewValue', {
                      months: getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity),
                      amount:
                        getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity) * monthlyAmount
                    })}
                  </span>
                </p>
              </div>
              <div className="bg-gray-300 mr-2 h-0.5 lg:w-full mt-2" />
              <Button
                className="mt-4"
                type={ButtonType.Primary}
                buttonType={'button'}
                onClick={async () => {
                  trackEvent({
                    category: TrackingEventCategories.Subscription,
                    action: 'renew-with-stripe',
                    name: 'payment'
                  })
                  await handleRenew(PaymentSlugs.STRIPE_PAYMENT)
                }}>
                {t('account.billingWithCreditcard')}
              </Button>
              <Button
                className="mt-4"
                type={ButtonType.Primary}
                buttonType={'button'}
                onClick={async () => {
                  trackEvent({
                    category: TrackingEventCategories.Subscription,
                    action: 'renew-with-payrexx',
                    name: 'payment'
                  })
                  await handleRenew(PaymentSlugs.PAYREXX_PAYMENT)
                }}>
                {t('account.billingWithPayrexx')}
              </Button>
            </div>
          </div>
        )}
        {intentSecret !== null && paymentMethod === PaymentSlugs.STRIPE_PAYMENT && (
          <div className="md:w-4/6">
            <div className="grid grid-cols-1">
              <StripeElement clientSecret={intentSecret}>
                <StripePayment
                  amount={
                    getMonthsinNumberFromPaymentPeriodicity(paymentPeriodicity) * monthlyAmount
                  }
                  onClose={refresh => {
                    setIntentSecret(null)
                    if (refresh) {
                      setIsLoading(true)
                      setTimeout(() => {
                        router.push('/account/subscription?paymentSuccess=true')
                      }, 3000)
                    }
                  }}
                />
              </StripeElement>
            </div>
          </div>
        )}
      </div>
    </AccountLayout>
  )
}
