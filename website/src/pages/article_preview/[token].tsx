import {GetServerSideProps} from 'next'
import {ssrArticle} from '../../@types/codegen/page'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import ArticleComponent from '../../components/ArticleComponent'
import React from 'react'
import {ArticleTemplateContainerProps} from '../zh/[slugID]'

export const getServerSideProps: GetServerSideProps = async ({params, locale}) => {
  if (!params || !params.token || typeof params.token !== 'string') {
    return {
      notFound: true
    }
  }

  const articleRes = await ssrArticle.getServerPage({
    variables: {
      token: params.token
    }
  })
  const {data} = articleRes.props

  if (!data.article) {
    return {
      notFound: true
    }
  }

  const tags = data.article.tags.map(tag => tag.replace('category:', ''))

  return {
    props: {
      articleID: 'fakeID',
      isPreview: true,
      article: {...data.article, tags},
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export function ArticlePreview(props: ArticleTemplateContainerProps) {
  return <ArticleComponent {...props} />
}

export default ArticlePreview
