import App from 'components/App'
import {ssrAuthorList, ssrNavigation, ssrNavigationList} from '../../@types/codegen/page'
import {RoundImage} from 'components/atoms/roundImage'
import Link from 'next/link'
import {ImageRefData} from 'components/types'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import Head from 'next/head'

export async function getServerSideProps({locale}) {
  const authorsListResponse = await ssrAuthorList.getServerPage({
    variables: {
      first: 100
    }
  })

  const {data: authorsData, error: authorsError} = authorsListResponse.props

  /** **************** */
  /** Different Team lists */
  /** ************** */

  const coreSlugs = [
    'simon-jacoby',
    'lara-blatter',
    'elio-donauer',
    'seraina-manser',
    'rahel-bains',
    'isabel-brun',
    'ladina-cavelti',
    'william-stern',
    'steffen-kolberg',
    'antonio-auf-der-mauer',
    'jula-zwinggi',
    'ursina-storrer'
  ]
  const coreTeamList = authorsData?.authors?.nodes?.filter(author =>
    coreSlugs.includes(author.slug)
  )

  const coreTeam = coreSlugs
    .map(slug => coreTeamList.find(author => author.slug === slug))
    .filter(author => author)

  const formerTeamSlugs = [
    'member-499',
    'member-599',
    'member-699',
    'member-1399',
    'demo-8499',
    'oliver-camenzind-2399'
  ]

  const formerTeam = authorsData?.authors?.nodes?.filter(author =>
    formerTeamSlugs.includes(author.slug)
  )

  const freelanceSlugs = [
    'member-299',
    'member-499',
    'member-599',
    'member-699',
    'member-1399',
    'demo-8499',
    'oliver-camenzind-2399'
  ]

  const freelanceTeam = authorsData?.authors?.nodes?.filter(author =>
    freelanceSlugs.includes(author.slug)
  )

  return {
    props: {
      team: {coreTeam, freelanceTeam, formerTeam},
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

interface TeamMemberCardProps {
  image: ImageRefData
  name: string
  jobTitle?: string
  id: string
  slug: string
}

const TeamMemberCard = ({image, name, jobTitle, slug}: TeamMemberCardProps) => (
  <Link href={`/redaktion/${slug}`}>
    <div className="flex flex-col items-center my-6 cursor-pointer">
      <RoundImage height={150} width={150} src={image?.mediumURL} />
      <div className="text-center capitalize mt-4">
        <h6 className="text-tsri text-xl font-bold">{name}</h6>
        {jobTitle && <p>{jobTitle}</p>}
      </div>
    </div>
  </Link>
)

export default function TeamPage(props: any) {
  const {navs, team} = props

  const {coreTeam, freelanceTeam, formerTeam} = team

  return (
    <>
      <Head>
        <meta name="description" content="Das Tsüri.ch Team" />
      </Head>
      <App navs={navs}>
        <div className="lg:container lg:mx-auto mb-20">
          <div>
            <h1 className="text-tsri lg:text-5xl pt-20 mb-14">Tsüri-Team</h1>
            <div className="bg-gray-400	my-4 mr-2 h-0.5 w-full" />
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-10">
              {coreTeam?.map(({id, slug, image, name, jobTitle}) => (
                <TeamMemberCard
                  key={id}
                  image={image}
                  name={name}
                  jobTitle={jobTitle}
                  id={id}
                  slug={slug}
                />
              ))}
            </div>
          </div>
          {/*} <div>
            <h1 className="text-tsri lg:text-5xl my-14">Freelancers</h1>
            <div className="bg-gray-400	my-4 mr-2 h-0.5 w-full" />
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-10">
              {freelanceTeam?.map(({id, slug, image, name, jobTitle}) => (
                <TeamMemberCard
                  key={id}
                  image={image}
                  name={name}
                  jobTitle={jobTitle}
                  id={id}
                  slug={slug}
                />
              ))}
            </div>
          </div>
          <div>
            <h1 className="text-tsri lg:text-5xl my-14">Former Team Members</h1>
            <div className="bg-gray-400	my-4 mr-2 h-0.5 w-full" />
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-10">
              {formerTeam?.map(({id, slug, image, name, jobTitle}) => (
                <TeamMemberCard
                  key={id}
                  image={image}
                  name={name}
                  jobTitle={jobTitle}
                  id={id}
                  slug={slug}
                />
              ))}
            </div>
          </div> */}
        </div>
      </App>
    </>
  )
}
