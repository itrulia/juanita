import React from 'react'
import {ssrArticleList, ssrAuthor} from '../../@types/codegen/page'
import {Loader} from 'components/atoms/loader'
import Head from 'next/head'
import {useRouter} from 'next/router'
import App from 'components/App'
import {SocialMediaButtons} from 'components/atoms/socialMediaButtons'
import {RichTextBlock} from 'components/blocks/richTextBlock'
import ArticleTeaser from 'components/atoms/deprecatedArticleTeaser'
import {ArticleRefFragment, FullAuthorFragment} from '../../@types/codegen/graphql'
import {useTranslation} from 'next-i18next'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {TSRI_PAGE_TITLE} from '../../helpers'
import {GetServerSideProps} from 'next'
import TeaserView from '../../components/atoms/teaser/teaserView'

export interface AuthorDetailProps {
  author: FullAuthorFragment
  articles: ArticleRefFragment[]
  navs: any
}

export const getServerSideProps: GetServerSideProps = async ({params, locale}) => {
  if (!params || !params.slug || typeof params.slug !== 'string') {
    return {
      redirect: {
        destination: '/redaktion',
        permanent: false
      }
    }
  }

  const authorResponse = await ssrAuthor.getServerPage({
    variables: {
      slug: params.slug
    }
  })
  const {data, error} = authorResponse.props

  if (!data.author || error) {
    return {
      notFound: true
    }
  }

  const articleListResponse = await ssrArticleList.getServerPage({
    variables: {
      first: 32,
      filter: {authors: [data.author.id]}
    }
  })

  const {data: articleListData} = articleListResponse.props

  return {
    props: {
      author: data.author,
      articles: articleListData?.articles.nodes,
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export function AuthorDetail(props: AuthorDetailProps) {
  const router = useRouter()
  const {t} = useTranslation('common')

  if (router.isFallback) {
    return <Loader text="Loading" isLoading={true} />
  }

  const {author, navs, articles} = props

  const {name, jobTitle, bio, links, image} = author

  return (
    <App navs={navs}>
      <Head>
        <title>{`${name} - ${TSRI_PAGE_TITLE}`}</title>
        {jobTitle && <meta name="description" content={jobTitle} />}
        <meta property="og:title" content={name} />
        <meta property="og:type" content="author" />
        {image && <meta property="og:image" content={image?.largeURL ?? ''} />}
      </Head>

      <div>
        <div className="flex flex-row items-center my-6 justify-between">
          <div className="capitalize mt-4">
            <h1 className="text-tsri font-bold capitalize text-3xl lg:text-4xl">{name}</h1>
            {jobTitle && <h2 className="text-gray-600 mb-6">{jobTitle}</h2>}
            <img className="mb-6" src={image?.detailURL} />
            <SocialMediaButtons links={links} />
          </div>
        </div>
        <RichTextBlock value={bio} />
        <div className="bg-gray-400	my-6 mr-2 h-0.5	lg:w-full lg:mt-1" />
        <h1 className="font-bold text-2xl lg:text-3xl text-center text-black mb-10">
          {t('team.allArticles', {name})}
        </h1>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-10">
          {articles.map(article => (
            <div className="mb-10" key={article.id}>
              <ArticleTeaser
                key={article.id}
                authors={article.authors}
                date={new Date(article.publishedAt)}
                title={article.title}
                image={article?.image}
                tags={article.tags}
                href={`/zh/${article.slug}.${article.id}`}
              />
            </div>
          ))}
        </div>
      </div>
    </App>
  )
}

export default AuthorDetail
