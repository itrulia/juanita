import App from '../components/App'

import {useRouter} from 'next/router'
import {useCookies} from 'react-cookie'
import {useRevokeActiveSessionMutation} from '../@types/codegen/graphql'
import {useEffect} from 'react'

const Logout = ({navs}) => {
  const router = useRouter()
  const [cookie, setCookie] = useCookies(['token'])

  const [creatSessionWithJwtMutation, {loading, error}] = useRevokeActiveSessionMutation()

  useEffect(() => {
    setCookie('authToken', '', {path: '/', sameSite: true})
    if (!loading) {
      creatSessionWithJwtMutation()
        .then(result => {
          // TODO check for graphql error
          console.log('logout', result)
        })
        .catch(error => {
          // TODO log error
          console.warn('logout error', error)
        })
        .finally(() => {
          router.replace('/')
        })
    }
  }, [])

  return (
    <App navs={navs}>
      <p>Logout</p>
    </App>
  )
}

export default Logout
