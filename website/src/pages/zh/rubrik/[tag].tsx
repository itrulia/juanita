import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {ssrArticleList} from '../../../@types/codegen/page'
import OldTeaserGrid from '../../../components/OldTeaserGrid'
import ArticleTeaser from '../../../components/atoms/deprecatedArticleTeaser'
import React from 'react'
import {GetServerSideProps} from 'next'
import {ArticleConnection} from '../../../@types/codegen/graphql'

export interface TagListProps {
  articleList: ArticleConnection
  tag?: string
  navs: any
}

export const getServerSideProps: GetServerSideProps = async ({params, query, locale}) => {
  const tag = params && params.tag && typeof params.tag === 'string' ? params.tag : undefined
  const after = query.after && typeof query.after === 'string' ? query.after : undefined
  const before = query.before && typeof query.before === 'string' ? query.before : undefined
  const articleListResponse = await ssrArticleList.getServerPage({
    variables: {
      ...(tag ? {filter: {tags: [tag]}} : {}),
      ...((!after && !before) || (after && before) ? {first: 32} : {}),
      ...(after && !before ? {after, first: 32} : {}),
      ...(before && !after ? {before, last: 32} : {})
    }
  })

  const {data: articleListData} = articleListResponse.props
  return {
    props: {
      articleList: articleListData?.articles,
      tag,
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

export default function TagList({articleList, tag, navs}: TagListProps) {
  const newerArticlesLink = articleList.pageInfo.hasPreviousPage
    ? `/zh/rubrik/${tag}?before=${articleList.pageInfo.startCursor}`
    : undefined
  const olderArticlesLink = articleList.pageInfo.hasNextPage
    ? `/zh/rubrik/${tag}?after=${articleList.pageInfo.endCursor}`
    : undefined

  return (
    <OldTeaserGrid
      title={tag}
      navs={navs}
      newerArticlesLink={newerArticlesLink}
      olderArticlesLink={olderArticlesLink}>
      {articleList.nodes.map(article => (
        <div className="mb-10" key={article.id}>
          <ArticleTeaser
            key={article.id}
            authors={article.authors}
            date={new Date(article.publishedAt)}
            title={article.title}
            image={article?.image}
            tags={article.tags}
            href={`/zh/${article.slug}.${article.id}`}
          />
        </div>
      ))}
    </OldTeaserGrid>
  )
}
