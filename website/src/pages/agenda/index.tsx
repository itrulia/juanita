import {Image, ImageFit} from 'components/atoms/image'
import {MovieIcon, MusicIcon, LiteratureIcon} from 'components/atoms/icon'
import {useSession} from 'next-auth/react'
import {useEffect, useState} from 'react'
import App from '../../components/App'
import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {useTranslation} from 'next-i18next'
import * as Sentry from '@sentry/nextjs'
import {useAppContext} from '../../utils/AppContext.'

export const EventCategoryIcon = (category: string) => {
  switch (category) {
    case 'movie':
      return <MovieIcon />
    case 'music':
    case 'Disko':
      return <MusicIcon />
    case 'literature':
      return <LiteratureIcon />
    default:
      return <LiteratureIcon />
  }
}

interface AgendaProps {
  agendaEvents: AgendaEvent[]
  navs: any
}

interface AgendaEvent {
  category: string
  title: string
  start_date: string | null
  start_time: string | null
  end_date: string | null
  end_time: string | null
  description: string
  url: string
  cost: string
  image: string
  pretty_time: string
}

export async function getServerSideProps({locale}) {
  const agendaEvents: AgendaEvent[] = []

  const username = process.env.TSRI_AGENDA_API_USER
  const password = process.env.TSRI_AGENDA_API_PASS
  if (!username) {
    console.error('Cannot find key: TSRI_AGENDA_API_USER')
  } else if (!password) {
    console.error('Cannot find key: TSRI_AGENDA_API_PASS')
  } else {
    const authorizationHeader = Buffer.from(`${username}:${password}`).toString('base64')

    try {
      const res = await fetch(process.env.TSRI_AGENDA_API_URL, {
        headers: new Headers({
          Authorization: `Basic ${authorizationHeader}`
        })
      })

      const data = await res.json()

      if (data?.objects) {
        data.objects.forEach(event => agendaEvents.push(event as AgendaEvent))
      }
      if (data.error) Sentry.captureException(data.error)
    } catch (err) {
      Sentry.captureException(err)
    }
  }

  return {
    props: {
      agendaEvents,
      ...(await serverSideTranslations(locale, ['common']))
    }
  }
}

interface EventCardProps {
  event: AgendaEvent
  isNewDay: boolean
}

function EventCard({event, isNewDay}: EventCardProps) {
  const {category, title, description, image, start_date, url, start_time, end_time} = event

  const startTime = start_time ? start_time.substring(0, 5) : null
  const endTime = end_time ? end_time.substring(0, 5) : null

  return (
    <a target="_blank" rel="noreferrer" href={url}>
      <div className="grid grid-flow-row grid-cols-4 lg:grid-flow-row lg:grid-cols-12 gap-x-3">
        <div className="col-span-4 lg:col-span-1 text-4xl font-bold flex flex-row lg:flex-col">
          {isNewDay && (
            <>
              <span className="lg:mr-0">
                {new Date(start_date).toLocaleDateString('de-CH', {
                  day: '2-digit'
                })}
              </span>
              <span className="visible lg:hidden">.</span>
              <span>
                {new Date(start_date).toLocaleDateString('de-CH', {
                  month: '2-digit'
                })}
              </span>
            </>
          )}
        </div>
        <div className="col-span-4 lg:col-span-2">
          <img
            src={image}
            style={{
              width: '100%',
              minHeight: '250px',
              height: '100%',
              display: 'block',
              objectFit: 'cover'
            }}
          />
        </div>
        <div className="col-span-4 lg:col-span-2 flex flex-col justify-between">
          <div>
            <p>
              {startTime ?? ''} {!startTime || !endTime ? '' : ' - '} {endTime ?? ''}
            </p>
            <h2 className="text-3xl">{title}</h2>
          </div>
          <div className="flex flex-row">
            <div className="w-8">{EventCategoryIcon(category)}</div>
            <p>{category}</p>
          </div>
        </div>
        <div
          className="col-span-4 lg:col-span-5 lg:col-start-8"
          dangerouslySetInnerHTML={{__html: description}}
        />
      </div>
    </a>
  )
}

export default function Index({agendaEvents, navs}: AgendaProps) {
  const [agendaEntries, setAgendaEntries] = useState<AgendaEvent[]>(
    agendaEvents?.length > 0 ? [agendaEvents?.[0]] : []
  )
  const {status} = useSession()
  const {t} = useTranslation('common')

  const {state: appState, update: updateAppState} = useAppContext()

  async function handleProfileClick() {
    updateAppState({...appState, showLoginModal: true})
  }

  useEffect(() => {
    if (status === 'authenticated' && agendaEvents?.length > 0) {
      setAgendaEntries(agendaEvents)
    }
  }, [status])

  return (
    <App navs={navs}>
      <div className="lg:container mx-4 lg:mx-auto">
        <h1 className="lg:text-5xl pt-10 mb-14 uppercase">{t('agenda.title')}</h1>
        <div>
          <div className="bg-gray-400	my-4 mr-2 h-0.5	lg:w-full lg:mt-5" />
          <div className="text-center uppercase text-3xl">
            {new Date().toLocaleDateString('de-CH', {
              month: 'long'
            })}
          </div>
        </div>
        {agendaEntries.map((event, index, array) => {
          const isNewDay = index > 0 ? array[index - 1].start_date !== event.start_date : true
          return (
            <div key={index}>
              {isNewDay && <div className="bg-gray-400	my-4 mr-2 h-0.5	lg:w-full lg:mt-5" />}
              <div className="my-10">
                <EventCard event={event} isNewDay={isNewDay} />
              </div>
            </div>
          )
        })}
        {status === 'unauthenticated' && (
          <div>
            <h2>{t('agenda.whyOnlyOneTitle')}</h2>

            <p className="mt-2">{t('agenda.whyOnlyOneText')}</p>
            <p
              onClick={() => handleProfileClick()}
              className="mt-3 hover:text-tsri cursor-pointer mobile:text-tsri">
              {t('agenda.whyOnlyOneCTA')}
            </p>
          </div>
        )}
      </div>
    </App>
  )
}
