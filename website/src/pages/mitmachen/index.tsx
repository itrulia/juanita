import {serverSideTranslations} from 'next-i18next/serverSideTranslations'
import {
  DateFilterComparison,
  InputCursorType,
  LimitType,
  SortOrder,
  SubscriptionSort
} from '@wepublish/api'
import * as Sentry from '@sentry/nextjs'
import {MongoDBAdapter} from '@wepublish/api-db-mongodb'
import React, {useState} from 'react'
import {Loader} from '../../components/atoms/loader'
import MitmachenIntroduction from '../../components/pages/mitmachen/MitmachenIntroduction'
import MitmachenForm from '../../components/pages/mitmachen/MitmachenForm'
import App from '../../components/App'
import MitmachenAppendix from '../../components/pages/mitmachen/MitmachenAppendix'

const dbAdapter = MongoDBAdapter.connect({
  url: process.env.MONGO_URL,
  locale: process.env.MONGO_LOCALE ?? 'en'
})

export async function getStaticProps({locale}) {
  const oneDay = 24 * 60 * 60 * 1000 // hours*minutes*seconds*milliseconds
  const firstDate = new Date().getTime()
  const secondDate = new Date(2015, 1, 18).getTime()
  const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay))

  let members: number = 0
  try {
    const client = await dbAdapter
    const subscriptions = await client.subscription.getSubscriptions({
      cursor: {type: InputCursorType.None},
      limit: {count: 10, type: LimitType.First, skip: 0},
      order: SortOrder.Ascending,
      sort: SubscriptionSort.CreatedAt,
      filter: {
        paidUntil: {comparison: DateFilterComparison.GreaterThanOrEqual, date: new Date()}
      }
    })
    members = subscriptions.totalCount
  } catch (error) {
    console.log(error)
    Sentry.captureException(error)
  }

  return {
    props: {
      climateMembers: 0,
      members,
      employees: 14,
      daysAlive: diffDays,
      averageSalary: 4000,
      earnedMoney: 0,
      ...(await serverSideTranslations(locale, ['common']))
    },
    revalidate: 10
  }
}

export default function Mitmachen({navs, members}: {navs: any; members: number}) {
  const [isLoading, setIsLoading] = useState<boolean>(false)

  return (
    <App navs={navs}>
      <Loader isLoading={isLoading} />
      {/* introduction */}
      <MitmachenIntroduction memberCount={members} />

      {/* form */}
      <MitmachenForm
        isLoading={state => {
          setIsLoading(state)
        }}
      />

      <MitmachenAppendix />
    </App>
  )
}
