import {createContext, Dispatch, SetStateAction, useContext, useState} from 'react'

interface AppContextState {
  showLoginModal: boolean
}

interface AppContextInterface {
  state: AppContextState
  update: Dispatch<SetStateAction<AppContextState>>
}

const AppContext = createContext<AppContextInterface | null>(null)

export function AppProvider({children}) {
  const [appContext, setAppContext] = useState<AppContextState>({
    showLoginModal: false
  })

  return (
    <AppContext.Provider value={{state: appContext, update: setAppContext}}>
      {children}
    </AppContext.Provider>
  )
}

export function useAppContext() {
  return useContext(AppContext)
}
