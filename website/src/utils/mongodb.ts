// This approach is taken from https://github.com/vercel/next.js/tree/canary/examples/with-mongodb
import {MongoDBAdapter} from '@wepublish/api-db-mongodb/lib'

let clientPromise

if (!process.env.MONGODB_URI) {
  throw new Error('Please add your Mongo URI to .env.local')
}

if (process.env.NODE_ENV === 'development' && false) {
  // In development mode, use a global variable so that the value
  // is preserved across module reloads caused by HMR (Hot Module Replacement).
  // @ts-ignore
  if (!global._mongoClientPromise) {
    // @ts-ignore
    global._mongoClientPromise = clientPromise = MongoDBAdapter.connect({
      url: process.env.MONGO_URL,
      locale: process.env.MONGO_LOCALE ?? 'en'
    })
  }
  // @ts-ignore
  clientPromise = global._mongoClientPromise
} else {
  console.log('test new mongo connect')
  clientPromise = MongoDBAdapter.connect({
    url: process.env.MONGO_URL,
    locale: process.env.MONGO_LOCALE ?? 'en'
  })
}

// Export a module-scoped MongoClient promise. By doing this in a
// separate module, the client can be shared across functions.
export default clientPromise
