import {Adapter} from 'next-auth/adapters'
import {MongoDBAdapter} from '@wepublish/api-db-mongodb/lib'
import {Subscription, UserSession} from '@wepublish/api'
import dbConnect from '../utils/dbConnect'
import VerificationTokenModel from '../utils/VerificationTokenModel'

export interface SubscriptionState {
  hasBeenActivated: boolean
  hasSubscription: boolean
  isActive: boolean
  subscription?: Subscription
}

export function wepublishNextAuthAdapter(client, options = {}): Adapter {
  return {
    async createUser(user) {
      return {id: 'fake', emailVerified: null} // TODO: redirect mitmachen
    },
    async getUser(id) {
      const adapter = (await client) as MongoDBAdapter
      const user = await adapter.user.getUserByID(id)
      if (!user) return null
      return user && user.active
        ? {
            id: user.id,
            emailVerified: user.emailVerifiedAt,
            name: user.name,
            email: user.email
          }
        : null
    },
    async getUserByEmail(email) {
      const adapter = (await client) as MongoDBAdapter
      const user = await adapter.user.getUser(email)
      if (!user) return null
      return user && user.active
        ? {
            id: user.id,
            emailVerified: user.emailVerifiedAt,
            name: user.name,
            email: user.email
          }
        : null
    },
    async getUserByAccount(account) {
      const adapter = (await client) as MongoDBAdapter
      const user = await adapter.user.getUserByOAuth2Account({
        provider: account.provider,
        providerAccountId: account.providerAccountId
      })
      if (!user) return null
      return user && user.active
        ? {
            id: user.id,
            emailVerified: user.emailVerifiedAt,
            name: user.name,
            email: user.email
          }
        : null
    },
    async updateUser(authUser) {
      const adapter = (await client) as MongoDBAdapter
      const user = await adapter.user.getUserByID(authUser.id)
      if (!user) return null

      const updatedUser = await adapter.user.updateUser({
        id: user.id,
        input: {
          email: user.email,
          emailVerifiedAt: user.emailVerifiedAt,
          name: user.name,
          active: user.active,
          address: user.address,
          preferredName: user.preferredName,
          roleIDs: user.roleIDs,
          properties: user.properties
        }
      })
      if (!updatedUser) return null
      return {
        id: updatedUser.id,
        emailVerified: updatedUser.emailVerifiedAt,
        name: updatedUser.name,
        email: updatedUser.email
      }
    },
    async deleteUser(userId) {
      return
    },
    // @ts-ignore
    async linkAccount(account) {
      const adapter = (await client) as MongoDBAdapter
      const updatedUser = await adapter.user.addOAuth2Account({
        userID: account.userId,
        oauth2Account: {
          tokenType: account.token_type ?? 'N/A',
          providerAccountId: account.providerAccountId,
          provider: account.provider,
          scope: account.scope ?? 'N/A',
          idToken: account.id_token ?? 'N/A',
          accessToken: account.access_token ?? 'N/A',
          type: account.type,
          expiresAt: account.expires_at ?? 0
        }
      })
      if (!updatedUser) return null
      const oauth2Account = updatedUser.oauth2Accounts.find(
        acc =>
          acc.providerAccountId === account.providerAccountId && acc.provider === account.provider
      )
      return {
        providerAccountId: oauth2Account.providerAccountId,
        userId: updatedUser.id,
        provider: oauth2Account.provider,
        type: oauth2Account.type
      }
    },
    async unlinkAccount({provider}) {
      return
    },
    async createSession({userId}) {
      const adapter = (await client) as MongoDBAdapter
      const user = await adapter.user.getUserByID(userId)
      if (!user) return null

      const session = await adapter.session.createUserSession(user)
      if (!session) return null

      return {
        id: session.id,
        sessionToken: session.token,
        userId: session.user.id,
        expires: session.expiresAt
      }
    },
    async getSessionAndUser(sessionToken) {
      const adapter = (await client) as MongoDBAdapter
      const session = (await adapter.session.getSessionByToken(sessionToken)) as UserSession
      if (!session || !session.user) return null
      const subscriptions = await adapter.subscription.getSubscriptionsByUserID(session.user.id)

      return {
        session: {
          id: session.id,
          sessionToken: session.token,
          userId: session.user.id,
          expires: session.expiresAt
        },
        user: {
          id: session.user.id,
          name: session.user.name,
          emailVerified: session.user.emailVerifiedAt,
          email: session.user.email,
          subscriptionState: {
            subscription: subscriptions.length > 0 ? subscriptions[0] : undefined,
            hasSubscription: subscriptions.length > 0,
            isActive: subscriptions.length > 0 && subscriptions[0].paidUntil >= new Date(),
            hasBeenActivated: subscriptions.length > 0 && subscriptions[0].paidUntil !== null
          } as SubscriptionState
        }
      }
    },
    async updateSession({sessionToken}) {
      const adapter = (await client) as MongoDBAdapter
      const updatedSession = await adapter.session.extendUserSessionByToken(sessionToken)
      if (!updatedSession) return null
      return {
        id: updatedSession.id,
        sessionToken: updatedSession.token,
        userId: updatedSession.user.id,
        expires: updatedSession.expiresAt
      }
    },
    async deleteSession(sessionToken) {
      const adapter = (await client) as MongoDBAdapter
      const session = (await adapter.session.getSessionByToken(sessionToken)) as UserSession
      if (!session) return null
      const isDeleted = await adapter.session.deleteUserSessionByToken(sessionToken)
      return isDeleted
        ? {
            id: session.id,
            sessionToken: session.token,
            userId: session.user.id,
            expires: session.expiresAt
          }
        : null
    },
    async createVerificationToken({identifier, expires, token}) {
      try {
        await dbConnect()
        const verificationToken = await VerificationTokenModel.create({
          token,
          expires,
          identifier
        })
        return {
          identifier: verificationToken.identifier,
          expires: verificationToken.expires,
          token: verificationToken.token
        }
      } catch (error) {
        console.warn('Error while creating VerificationToken', error)
        return null
      }
    },
    async useVerificationToken({identifier, token}) {
      try {
        await dbConnect()
        const verificationToken = await VerificationTokenModel.findOneAndDelete({
          identifier,
          token
        })
        if (!verificationToken || verificationToken.expires < new Date()) {
          return null
        }
        return {
          identifier: verificationToken.identifier,
          expires: verificationToken.expires,
          token: verificationToken.token
        }
      } catch (error) {
        console.warn('Error while using VerificationToken', error)
        return null
      }
    }
  }
}
