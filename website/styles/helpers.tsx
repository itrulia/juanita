export function removePx(breakpoint: string): number {
  return parseInt(breakpoint.replace('px', ''))
}

export function pxToRem(px: number) {
  return `${px / 10}rem`
}

export const GRID_COLUMNS = 12
export const GRID_GAP = 15
export const GRID_ROW_GAP = GRID_GAP * 3
export const BLOCK_GAP = GRID_GAP * 3
