import {
  BasePaymentProvider,
  CheckIntentProps,
  CreatePaymentIntentProps,
  Intent,
  IntentState,
  WebhookForPaymentIntentProps,
  PaymentState,
  logger
} from '@wepublish/api'

export class NoMoneyPaymentProvider extends BasePaymentProvider {
  constructor(props: any) {
    super(props)
  }

  async webhookForPaymentIntent(props: WebhookForPaymentIntentProps): Promise<IntentState[]> {
    logger('NoMoneyPaymentProvider').error(props, 'Webhook was called')
    return []
  }

  async createIntent(props: CreatePaymentIntentProps): Promise<Intent> {
    return {
      intentID: `nomoney_${new Date().getTime()}`,
      intentSecret: '',
      intentData: '',
      paidAt: new Date(),
      state: PaymentState.Paid
    }
  }

  async checkIntentStatus({intentID}: CheckIntentProps): Promise<IntentState> {
    return {
      state: PaymentState.Paid,
      paidAt: new Date(),
      paymentID: 'fakePaymentID',
      paymentData: ''
    }
  }
}
