import {Client} from 'pg'
import {MongoClient} from 'mongodb'

export async function fixUserAddresses() {
  const clientpg = new Client({
    host: process.env.PGHOST,
    port: process.env.PGPORT ? parseInt(process.env.PGPORT) : 5432,
    user: process.env.PGUSER,
    password: process.env.PGPASSWORD,
    database: process.env.PGDATABASE,
    ssl: false
  })
  await clientpg.connect()

  if (!process.env.MONGO_URL) throw new Error('process.env.MONGO_URL not defined')
  const clientTsri = await MongoClient.connect(process.env.MONGO_URL)
  const userCollection = await clientTsri.db().collection('users')
  const bulkOp = userCollection.initializeUnorderedBulkOp()
  console.log('clientTs', clientTsri)
  let count = 0
  const users = await clientpg.query('SELECT * FROM accounts_user')

  for (const [index, user] of users.rows.entries()) {
    console.log('user', index, user.id, user.zip_code)
    if (user.zip_code) {
      count += 1
      bulkOp.find({email: user.email, address: {$ne: null}}).update({
        $set: {
          'address.zipCode': `${user.zip_code}`
        }
      })
    }
  }
  console.log('updating adresses:', count)
  await bulkOp.execute()
  console.log('Done')
}
