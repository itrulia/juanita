import {
  BasePaymentProvider,
  CheckIntentProps,
  CreatePaymentIntentProps,
  Intent,
  IntentState,
  WebhookForPaymentIntentProps,
  PaymentState,
  logger
} from '@wepublish/api'

export class LegacyPaymentProvider extends BasePaymentProvider {
  constructor(props: any) {
    super(props)
  }

  async webhookForPaymentIntent(props: WebhookForPaymentIntentProps): Promise<IntentState[]> {
    logger('legacyPaymentProvider').error(props, 'Webhook was called')
    return []
  }

  async createIntent(props: CreatePaymentIntentProps): Promise<Intent> {
    logger('legacyPaymentProvider').error(props, 'createIntent was called')

    return {
      intentID: 'fakeID',
      intentSecret: 'fakeSecret',
      intentData: '',
      state: PaymentState.Submitted
    }
  }

  async checkIntentStatus({intentID}: CheckIntentProps): Promise<IntentState> {
    return {
      state: PaymentState.Paid,
      paymentID: 'fakePaymentID',
      paymentData: ''
    }
  }
}
