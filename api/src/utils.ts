import {DBAdapter, InputCursorType, LimitType, SortOrder, User, UserSort} from '@wepublish/api'

export const ONE_HOUR_IN_MILLISECONDS = 60 * 60 * 1000
export const ONE_DAY_IN_MILLISECONDS = 24 * ONE_HOUR_IN_MILLISECONDS

export const TWO_WEEKS_IN_MINUTES = 14 * ONE_DAY_IN_MILLISECONDS

export async function getAllUsers(dbAdapter: DBAdapter): Promise<User[]> {
  const allUsers: User[] = []
  let hasMore = true
  let skip = 0
  while (hasMore) {
    const users = await dbAdapter.user.getUsers({
      cursor: {type: InputCursorType.None},
      limit: {count: 100, type: LimitType.First, skip},
      order: SortOrder.Ascending,
      sort: UserSort.CreatedAt
    })

    hasMore = users.pageInfo.hasNextPage
    skip += 100
    allUsers.push(...users.nodes)
  }
  return allUsers
}
