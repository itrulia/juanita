import process from 'process'
import {
  DBAdapter,
  InputCursorType,
  LimitType,
  MailLog,
  MailLogSort,
  SortOrder
} from '@wepublish/api'
import {ONE_DAY_IN_MILLISECONDS} from '../utils'
import ObjectsToCsv from 'objects-to-csv'
import path from 'path'
import {drive, auth as GoogleAuth} from '@googleapis/drive'
import fs from 'fs'
import Mailgun from 'mailgun.js'
import FormData from 'form-data'

export async function maillogAnalytics(dbAdapter: DBAdapter) {
  const invoiceReminderFrequencyInDays = parseInt(process.env.INVOICE_REMINDER_FREQ ?? '') || 3

  if (!process.env.MAILGUN_API_KEY)
    throw new Error('No MAILGUN_FULL_API_KEY defined in environment.')
  if (!process.env.MAILGUN_BASE_DOMAIN)
    throw new Error('No MAILGUN_BASE_DOMAIN defined in environment.')
  if (!process.env.MAILGUN_MAIL_DOMAIN)
    throw new Error('No MAILGUN_MAIL_DOMAIN defined in environment.')
  if (!process.env.GDRIVE_DRIVE_ID) throw new Error('No GDRIVE_DRIVE_ID defined in environment.')

  const mailgun = new Mailgun(FormData)

  const mg = mailgun.client({
    username: 'api',
    key: process.env.MAILGUN_API_KEY,
    url: `https://${process.env.MAILGUN_BASE_DOMAIN}`
  })

  try {
    let hasMoreLogs = true
    let skip = 0
    const mailLogsInTheLastThreeDays: MailLog[] = []
    const threeDaysAgo = new Date(
      new Date().getTime() - invoiceReminderFrequencyInDays * ONE_DAY_IN_MILLISECONDS
    )
    while (hasMoreLogs) {
      const maillogs = await dbAdapter.mailLog.getMailLogs({
        filter: {subject: 'Tsüri-Mitgliedschaft'},
        sort: MailLogSort.ModifiedAt,
        order: SortOrder.Descending,
        limit: {count: 100, type: LimitType.First, skip},
        cursor: {type: InputCursorType.None}
      })

      hasMoreLogs =
        maillogs.pageInfo.hasNextPage &&
        maillogs.nodes[maillogs.nodes.length - 1].createdAt >= threeDaysAgo
      console.log('last date', maillogs.nodes[maillogs.nodes.length - 1].createdAt)
      skip += 100
      mailLogsInTheLastThreeDays.push(
        ...maillogs.nodes.filter(maillog => maillog.createdAt >= threeDaysAgo)
      )
    }

    const mailLogsWithEvents = []
    for (const maillog of mailLogsInTheLastThreeDays) {
      let mailData: any = {}
      try {
        mailData = JSON.parse(maillog.mailData || '{}')
      } catch (error) {
        console.log('JSON parser error', error)
        continue
      }
      const messageId = mailData.message?.headers?.['message-id']
      if (messageId) {
        const events = await mg.events.get(process.env.MAILGUN_MAIL_DOMAIN, {
          // @ts-ignore
          'message-id': messageId
        })

        const mailLogWithEvents = {
          mailogID: maillog.id,
          sent: maillog.createdAt.toLocaleDateString(),
          email: maillog.recipient,
          subject: maillog.subject,
          accepted: '-',
          delivered: '-',
          opened: '-',
          clicked: '-'
        }

        for (const eventItem of events.items) {
          switch (eventItem.event) {
            case 'opened':
              mailLogWithEvents.opened = new Date(eventItem.timestamp * 1000).toLocaleString()
              break
            case 'delivered':
              mailLogWithEvents.delivered = new Date(eventItem.timestamp * 1000).toLocaleString()
              break
            case 'accepted':
              mailLogWithEvents.accepted = new Date(eventItem.timestamp * 1000).toLocaleString()
              break
            case 'clicked':
              mailLogWithEvents.clicked = new Date(eventItem.timestamp * 1000).toLocaleString()
              break
            default:
              console.log('Event type not known', eventItem.event)
          }
        }
        mailLogsWithEvents.push(mailLogWithEvents)
      }
    }
    const csv = new ObjectsToCsv(mailLogsWithEvents)
    const tempFileName = `maillog_analytics_${new Date().getTime()}.csv`
    await csv.toDisk(path.join(__dirname, tempFileName))

    const KEYFILEPATH =
      process.env.NODE_ENV === 'production'
        ? '/var/secrets/drive/key.json'
        : path.join(__dirname, '../../maillog-uploader.json')
    const SCOPES = ['https://www.googleapis.com/auth/drive']

    const auth = new GoogleAuth.GoogleAuth({
      keyFile: KEYFILEPATH,
      scopes: SCOPES
    })
    const driveService = drive({version: 'v3', auth})

    const {data: {id, name} = {}} = await driveService.files.create({
      // @ts-ignore
      resource: {
        driveId: process.env.GDRIVE_DRIVE_ID,
        name: `${process.env.HOST_ENV}-mailLogs-${new Date().toISOString()}.csv`,
        parents: [process.env.GDRIVE_DRIVE_ID],
        mimeType: 'text/csv'
      },
      media: {
        mimeType: 'text/csv',
        body: fs.createReadStream(path.join(__dirname, tempFileName))
      },
      supportsAllDrives: true,
      fields: 'id,name'
    })
    console.log('File Uploaded', name, id)
  } catch (error) {
    console.log('error', error)
  }
  return
}
