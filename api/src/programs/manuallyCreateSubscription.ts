import {
  contextFromRequest,
  DBAdapter,
  PaymentPeriodicity,
  WepublishServerOpts
} from '@wepublish/api'
import {ONE_DAY_IN_MILLISECONDS} from '../utils'

function generatePassword() {
  const length = 64
  const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
  let password = ''
  for (let i = 0, n = charset.length; i < length; ++i) {
    password += charset.charAt(Math.floor(Math.random() * n))
  }
  return password
}

export async function manuallyExtendSubscription(
  dbAdapter: DBAdapter,
  opts: WepublishServerOpts,
  userEmail: string
): Promise<void> {
  const paymentMethods = await dbAdapter.paymentMethod.getPaymentMethods()
  const legacyPaymentMethod = paymentMethods.find(pm => pm.slug === 'tsri-legacy')
  if (!legacyPaymentMethod) throw Error('Could not load legacy payment Method')
  const stripePaymentMethod = paymentMethods.find(pm => pm.slug === 'stripe')
  if (!stripePaymentMethod) throw Error('Could not load stripe payment Method')
  const payrexxPaymentMethod = paymentMethods.find(pm => pm.slug === 'payrexx')
  if (!payrexxPaymentMethod) throw Error('Could not load payrexx payment Method')
  const noMoneyPaymentMethod = paymentMethods.find(pm => pm.slug === 'no-money')
  if (!noMoneyPaymentMethod) throw Error('Could not load no-money payment Method')

  const user = await dbAdapter.user.getUser(userEmail)
  if (!user) throw Error('User not found')

  const subscriptions = await dbAdapter.subscription.getSubscriptionsByUserID(user.id)
  if (!subscriptions || subscriptions.length === 0) throw Error('Subscriptions not found')
  const subscription = subscriptions[0]
  if (!subscription) throw Error('Subscription not found')
  if (subscription.paidUntil === null) throw Error('Paid Until is null')
  if (subscription.paidUntil < new Date()) throw new Error('Subscription is not active')

  const startDate = new Date(subscription.paidUntil.getTime() + ONE_DAY_IN_MILLISECONDS)
  const nextDate = new Date(startDate.getTime() + 365 * ONE_DAY_IN_MILLISECONDS)

  const invoice = await dbAdapter.invoice.createInvoice({
    input: {
      subscriptionID: subscription.id,
      description: `Membership from ${startDate.toISOString()} for ${user.name || user.email}`,
      mail: user.email,
      dueAt: startDate,
      items: [
        {
          createdAt: new Date(),
          modifiedAt: new Date(),
          name: 'Membership',
          description: `From ${startDate.toISOString()} to ${nextDate.toISOString()}`,
          amount: 500,
          quantity: 1
        }
      ],
      paidAt: null,
      canceledAt: null
    }
  })

  await dbAdapter.subscription.addSubscriptionPeriod({
    subscriptionID: subscription.id,
    input: {
      startsAt: startDate,
      endsAt: nextDate,
      amount: 500,
      paymentPeriodicity: PaymentPeriodicity.Yearly,
      invoiceID: invoice.id
    }
  })
  const context = await contextFromRequest(null, opts)
  await context.memberContext.chargeInvoice({
    user,
    paymentMethodID: noMoneyPaymentMethod.id,
    invoice: invoice,
    customer: {paymentProviderID: 'noMoney', customerID: 'fake'}
  })
  console.log('Charged invoice with no-money payment method')

  await new Promise(resolve => setTimeout(resolve, 500))

  console.log('DONE')
  process.exit(0)
}

export async function manuallyRenewSubscription(
  dbAdapter: DBAdapter,
  opts: WepublishServerOpts,
  userEmail: string,
  renewActiveSubscriptions: boolean,
  paymentMethodSlug?: string,
  paymentPeriodicity?: PaymentPeriodicity
): Promise<void> {
  const paymentMethods = await dbAdapter.paymentMethod.getPaymentMethods()
  const legacyPaymentMethod = paymentMethods.find(pm => pm.slug === 'tsri-legacy')
  if (!legacyPaymentMethod) throw Error('Could not load legacy payment Method')
  const stripePaymentMethod = paymentMethods.find(pm => pm.slug === 'stripe')
  if (!stripePaymentMethod) throw Error('Could not load stripe payment Method')
  const payrexxPaymentMethod = paymentMethods.find(pm => pm.slug === 'payrexx')
  if (!payrexxPaymentMethod) throw Error('Could not load payrexx payment Method')
  const noMoneyPaymentMethod = paymentMethods.find(pm => pm.slug === 'no-money')
  if (!noMoneyPaymentMethod) throw Error('Could not load no-money payment Method')

  const user = await dbAdapter.user.getUser(userEmail)
  if (!user) throw Error('User not found')

  const subscriptions = await dbAdapter.subscription.getSubscriptionsByUserID(user.id)
  if (!subscriptions || subscriptions.length === 0) throw Error('Subscriptions not found')
  const subscription = subscriptions[0]
  if (!subscription) throw Error('Subscription not found')
  if (subscription.paidUntil === null) throw Error('Paid Until is null')
  if (!renewActiveSubscriptions && subscription.paidUntil > new Date())
    throw new Error('Subscription still active')

  const updatedSubscription = await dbAdapter.subscription.updateSubscription({
    id: subscription.id,
    input: {
      ...subscription,
      ...(paymentPeriodicity ? {paymentPeriodicity} : {}),
      deactivation: null
    }
  })
  if (!updatedSubscription) throw Error('Subscription not updated')

  const context = await contextFromRequest(null, opts)
  const invoice = await context.memberContext.renewSubscriptionForUser({
    subscription: updatedSubscription
  })
  if (!invoice) throw new Error('Could not create invoice')

  if (paymentMethodSlug === 'no-money') {
    await context.memberContext.chargeInvoice({
      user,
      paymentMethodID: noMoneyPaymentMethod.id,
      invoice: invoice,
      customer: {paymentProviderID: 'noMoney', customerID: 'fake'}
    })
    console.log('Charged invoic ewith no-money payment method')
  } else {
    console.log('Invoice created will be charged/sent out at the next interval')
  }

  await new Promise(resolve => setTimeout(resolve, 500))

  console.log('DONE')
  process.exit(0)
}

export async function manuallyCreateSubscription(
  dbAdapter: DBAdapter,
  opts: WepublishServerOpts,
  userEmail: string,
  userName: string
): Promise<void> {
  const paymentMethods = await dbAdapter.paymentMethod.getPaymentMethods()
  const legacyPaymentMethod = paymentMethods.find(pm => pm.slug === 'tsri-legacy')
  if (!legacyPaymentMethod) throw Error('Could not load legacy payment Method')
  const stripePaymentMethod = paymentMethods.find(pm => pm.slug === 'stripe')
  if (!stripePaymentMethod) throw Error('Could not load stripe payment Method')
  const payrexxPaymentMethod = paymentMethods.find(pm => pm.slug === 'payrexx')
  if (!payrexxPaymentMethod) throw Error('Could not load payrexx payment Method')
  const noMoneyPaymentMethod = paymentMethods.find(pm => pm.slug === 'no-money')
  if (!noMoneyPaymentMethod) throw Error('Could not load no-money payment Method')

  const memberplans = await dbAdapter.memberPlan.getActiveMemberPlansBySlug(['tsri-member'])
  if (memberplans.length === 0) throw new Error('Could not load member plan')
  const memberPlan = memberplans[0]
  if (!memberPlan) throw new Error('Could not load member plan')

  const user = await dbAdapter.user.createUser({
    password: generatePassword(),
    input: {
      name: userName,
      preferredName: '',
      email: userEmail,
      active: true,
      emailVerifiedAt: null,
      roleIDs: [],
      address: {
        city: '',
        streetAddress: '',
        streetAddress2: '',
        zipCode: '',
        company: '',
        country: ''
      },
      properties: []
    }
  })
  if (!user) throw new Error('Could not create user')

  const subscription = await dbAdapter.subscription.createSubscription({
    input: {
      userID: user.id,
      paymentPeriodicity: PaymentPeriodicity.Yearly,
      properties: [
        {
          key: 'manually',
          value: 'crowdfunding-watchdog',
          public: false
        }
      ],
      autoRenew: false,
      monthlyAmount: 500,
      startsAt: new Date(),
      memberPlanID: memberPlan.id,
      paymentMethodID: payrexxPaymentMethod.id,
      deactivation: null,
      paidUntil: null
    }
  })
  if (!subscription) throw new Error('Could not create subscription')
  const context = await contextFromRequest(null, opts)

  const invoice = await context.memberContext.renewSubscriptionForUser({subscription})
  if (!invoice) throw new Error('Could not create invoice')

  await context.memberContext.chargeInvoice({
    user,
    paymentMethodID: noMoneyPaymentMethod.id,
    invoice: invoice,
    customer: {paymentProviderID: 'noMoney', customerID: 'fake'}
  })

  await new Promise(resolve => setTimeout(resolve, 500))

  console.log('DONE')
  process.exit(0)
}
