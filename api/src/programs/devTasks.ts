import process from 'process'
import {
  DBAdapter,
  InputCursorType,
  /* Invoice,
  InvoiceSort, */
  LimitType,
  SortOrder,
  User,
  UserSort
} from '@wepublish/api'

export async function transformUserDataForTesting(dbAdapter: DBAdapter) {
  if (process.env.NODE_ENV !== 'development') throw new Error('NODE_ENV is not development')
  if (process.env.HOST_ENV !== 'development') throw new Error('HOST_ENV is not development')

  try {
    let hasMoreUsers = true
    let skip = 0
    const allUsers: User[] = []
    while (hasMoreUsers) {
      const users = await dbAdapter.user.getUsers({
        sort: UserSort.CreatedAt,
        order: SortOrder.Descending,
        limit: {count: 100, type: LimitType.First, skip},
        cursor: {type: InputCursorType.None}
      })

      hasMoreUsers = users.pageInfo.hasNextPage
      skip += 100
      allUsers.push(...users.nodes)
    }

    for (const user of allUsers) {
      const splitMail = user.email.split('@')
      if (!user.email.startsWith('nico.roos')) {
        await dbAdapter.user.updateUser({
          id: user.id,
          input: {
            ...user,
            email: `nico.roos+${splitMail[0]}--${splitMail[1]}@tsri.ch`
          }
        })
      }

      if (user.paymentProviderCustomers.length > 0) {
        await dbAdapter.user.updatePaymentProviderCustomers({
          userID: user.id,
          paymentProviderCustomers: []
        })
      }
    }

    /*console.log('start3')
    let hasMoreInvoices = true
    skip = 0
    const allInvoices: Invoice[] = []
    while (hasMoreInvoices) {
      console.log(`Getting invoices skip: ${skip}`)
      const invoices = await dbAdapter.invoice.getInvoices({
        sort: InvoiceSort.CreatedAt,
        order: SortOrder.Descending,
        limit: {count: 100, type: LimitType.First, skip},
        cursor: {type: InputCursorType.None}
      })

      hasMoreInvoices = invoices.pageInfo.hasNextPage
      skip += 100
      allInvoices.push(...invoices.nodes)
    }

    for (const invoice of allInvoices) {
      const splitMail = invoice.mail.split('@')
      if (!invoice.mail.startsWith('nico.roos')) {
        console.log(`Changing Invoice with ID ${invoice.id}`)
        await dbAdapter.invoice.updateInvoice({
          id: invoice.id,
          input: {
            ...invoice,
            mail: `nico.roos+${splitMail[0]}--${splitMail[1]}@tsri.ch`
          }
        })
      }
    } */
  } catch (error) {
    console.log('error', error)
  }
  return
}
