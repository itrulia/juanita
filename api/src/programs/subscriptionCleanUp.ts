import {
  DateFilterComparison,
  DBAdapter,
  InputCursor,
  //InputCursorType,
  Invoice,
  InvoiceSort,
  LimitType,
  SortOrder
  /*SubscriptionDeactivationReason,
  User,
  UserSort */
} from '@wepublish/api'
import {ONE_DAY_IN_MILLISECONDS} from '../utils'

async function cleanUpTempUsers(dbAdapter: DBAdapter) {
  /* const today = new Date()
  try {
    let hasMore = true
    let skip = 0
    const tempUsers: User[] = []
    while (hasMore) {
      const users = await dbAdapter.user.getUsers({
        filter: {subscription: {paidUntil: {date: null, comparison: DateFilterComparison.Equal}}},
        sort: UserSort.CreatedAt,
        order: SortOrder.Descending,
        limit: {count: 100, type: LimitType.First, skip},
        cursor: {type: InputCursorType.None}
      })

      hasMore = users.pageInfo.hasNextPage
      skip += 100
      tempUsers.push(...users.nodes)
    }
    const olderTempUsers = tempUsers
      .filter(user => user.createdAt < new Date(today.getTime() - ONE_DAY_IN_MILLISECONDS))
      .filter(user => user.email.includes('temp_') && user.subscription)
    console.log('olderTempUsers count', olderTempUsers.length)

    for (const user of olderTempUsers) {
      const invoices = await dbAdapter.invoice.getInvoicesByUserID(user.id)

      for (const invoice of invoices) {
        if (!invoice || invoice.paidAt || invoice.canceledAt) continue
        console.log('canceling Invoice', invoice.id)
        await dbAdapter.invoice.updateInvoice({
          id: invoice.id,
          input: {
            ...invoice,
            canceledAt: today
          }
        })
      }
    }
  } catch (error) {
    console.log('error', error)
  } */
}

async function cleanUpUsers(dbAdapter: DBAdapter) {
  /* const today = new Date()
  const SIXTEEN_DAYS_AGO = new Date(today.getTime() - 14 * ONE_DAY_IN_MILLISECONDS)

  try {
    let hasMore = true
    let skip = 0
    const usersActiveUntilTodayAndNotDeactivated: User[] = []
    while (hasMore) {
      const users = await dbAdapter.user.getUsers({
        filter: {
          subscription: {
            paidUntil: {date: new Date(), comparison: DateFilterComparison.LowerThan},
            deactivationDate: {date: null, comparison: DateFilterComparison.Equal}
          }
        },
        sort: UserSort.CreatedAt,
        order: SortOrder.Descending,
        limit: {count: 100, type: LimitType.First, skip},
        cursor: {type: InputCursorType.None}
      })

      hasMore = users.pageInfo.hasNextPage
      skip += 100
      usersActiveUntilTodayAndNotDeactivated.push(...users.nodes)
    }

    const activeUsersToBeDeactivated = usersActiveUntilTodayAndNotDeactivated.filter(
      user => user.active
    )

    //console.log('users', activeUsersToBeDeactivated)
    console.log('active users to be deactivated count', activeUsersToBeDeactivated.length)
    for (const user of activeUsersToBeDeactivated) {
      let deactivateUser = false
      if (!user.subscription) continue
      console.log(user.id, user.name, user.subscription?.paidUntil)
      const invoices = await dbAdapter.invoice.getInvoicesByUserID(user.id)
      const openInvoices = invoices.filter(
        invoice => invoice?.paidAt === null && invoice?.canceledAt === null
      )
      if (openInvoices.length === 0) {
        deactivateUser = true
      } else {
        for (const invoice of openInvoices) {
          if (!invoice) continue
          if (invoice.createdAt < SIXTEEN_DAYS_AGO) {
            await dbAdapter.invoice.updateInvoice({
              id: invoice.id,
              input: {
                ...invoice,
                canceledAt: new Date()
              }
            })
          }
        }
        const newInvoices = await dbAdapter.invoice.getInvoicesByUserID(user.id)
        const newOpenInvoices = newInvoices.filter(
          invoice => invoice?.paidAt === null && invoice?.canceledAt === null
        )
        if (newOpenInvoices.length === 0) {
          deactivateUser = true
        }
      }

      if (deactivateUser) {
        await dbAdapter.user.updateUserSubscription({
          userID: user.id,
          input: {
            ...user.subscription,
            deactivation: {
              date: new Date(),
              reason: SubscriptionDeactivationReason.None
            }
          }
        })
      }
    }
  } catch (error) {
    console.log('error')
  } */
}

async function cleanUpInvoices(dbAdapter: DBAdapter) {
  const today = new Date()
  const SIXTEEN_DAYS_AGO = new Date(today.getTime() - 16 * ONE_DAY_IN_MILLISECONDS)
  try {
    let endCursor: string | boolean = true
    const invoicesNotPaidOrCanceled: Invoice[] = []
    while (endCursor) {
      console.log('getting next', endCursor)
      const inputCursor = InputCursor(endCursor === true ? undefined : endCursor)
      const invoices = await dbAdapter.invoice.getInvoices({
        filter: {
          paidAt: {date: null, comparison: DateFilterComparison.Equal},
          canceledAt: {date: null, comparison: DateFilterComparison.Equal}
        },
        sort: InvoiceSort.CreatedAt,
        order: SortOrder.Descending,
        limit: {count: 100, type: LimitType.First},
        cursor: inputCursor
      })

      endCursor = invoices.pageInfo.endCursor ?? false
      invoicesNotPaidOrCanceled.push(...invoices.nodes)
    }

    const invoicesNotPaidOrCanceledOlder = invoicesNotPaidOrCanceled.filter(
      invoice => invoice.createdAt <= SIXTEEN_DAYS_AGO
    )

    console.log('invoices', invoicesNotPaidOrCanceledOlder)
    //TODO: inform seraina or nico about users that are not deactivated that should
    console.log('count', invoicesNotPaidOrCanceledOlder.length)
  } catch (error) {
    console.log('error', error)
  }
}

export async function subscriptionCleanUp(dbAdapter: DBAdapter) {
  //TODO: Update clean up jobs
  await cleanUpTempUsers(dbAdapter)
  await cleanUpUsers(dbAdapter)
  await cleanUpInvoices(dbAdapter)

  return
}
