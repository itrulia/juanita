import FormData from 'form-data'
import fetch from 'node-fetch'
import {User} from '@wepublish/api'
import * as Sentry from '@sentry/node'

Sentry.init({
  dsn: process.env.SENTRY_DSN
})

interface AgendaEvent {
  category: string
  title: string
  start_date: string | null
  start_time: string | null
  end_date: string | null
  end_time: string | null
  description: string
  url: string
  cost: string
  image: string
  pretty_time: string
}

export interface SendNewsletterProps {
  users: User[]
  defaultFromMailAddress: string
}

export async function sendNewsletter({users, defaultFromMailAddress}: SendNewsletterProps) {
  const username = process.env.TSRI_AGENDA_API_USER
  const password = process.env.TSRI_AGENDA_API_PASS
  const url = process.env.TSRI_AGENDA_API_URL
  const baseDomain = process.env.MAILGUN_BASE_DOMAIN
  const mailDomain = process.env.MAILGUN_MAIL_DOMAIN
  const apiKey = process.env.MAILGUN_API_KEY

  if (!username || !password || !url || !baseDomain || !mailDomain || !apiKey) {
    console.error('Missing ENV vars')
    return
  }

  const auth = Buffer.from(`api:${apiKey}`).toString('base64')

  const agendaEvents: AgendaEvent[] = []
  const authorizationHeader = Buffer.from(`${username}:${password}`).toString('base64')
  try {
    const res = await fetch(url, {
      headers: {
        Authorization: `Basic ${authorizationHeader}`
      }
    })
    const data: any = await res.json()

    if (data?.objects) {
      data.objects.forEach((event: any) => agendaEvents.push(event as AgendaEvent))
    }
    if (data.error) {
      Sentry.captureException(data.error)
      return
    }
  } catch (error) {
    Sentry.captureException(error)
    console.error('error fetching events', error)
    return
  }

  const recipients = users
    .filter(user => {
      return !user.properties.some(
        property => property.key === 'weeklyAgenda' && property.value === 'false'
      )
    })
    .map(user => ({to: user.email, name: user.preferredName || user.name}))

  const events = agendaEvents.map(event => {
    if (event.start_date) {
      const startDate = new Date(event.start_date)
      event['start_date'] = `${startDate.getDate()}.${startDate.getMonth() +
        1}.${startDate.getUTCFullYear()}`
    }
    return event
  })

  while (recipients.length > 0) {
    // send in 500 batches. Mailgun limit is at 1000
    const recipients500 = recipients.splice(0, 500)
    const names = recipients500.reduce((prev: Record<string, any>, current) => {
      prev[current.to] = {name: current.name}
      return prev
    }, {})

    try {
      await new Promise((resolve, reject) => {
        const form = new FormData()
        form.append('from', 'Dein Stadtmagazin info@tsri.ch')
        for (const recipient of recipients500) {
          form.append('to', recipient.to)
        }
        form.append('subject', `Tsüri-Agenda: Die besten Events der nächsten 7 Tage`)
        form.append('template', 'juanita_weekly_agenda')
        form.append('recipient-variables', JSON.stringify(names))
        form.append('h:X-Mailgun-Variables', JSON.stringify({events}))
        form.submit(
          {
            protocol: 'https:',
            host: baseDomain,
            path: `/v3/${mailDomain}/messages`,
            method: 'POST',
            headers: {
              Accept: 'application/json',
              Authorization: `Basic ${auth}`
            }
          },
          (err, res) => {
            console.log('status code', res.statusCode)
            return err || res.statusCode !== 200 ? reject(err || res) : resolve({})
          }
        )
      })
    } catch (error) {
      Sentry.captureException(error)
      console.error('error sending mail', error)
    }
  }
}
