#!/usr/bin/env node
import {
  AlgebraicCaptchaChallenge,
  Author,
  DateFilterComparison,
  InputCursorType,
  JobType,
  LimitType,
  Oauth2Provider,
  PaymentProvider,
  PayrexxPaymentProvider,
  Peer,
  PublicArticle,
  PublicComment,
  PublicPage,
  SendMailType,
  SortOrder,
  Subscription,
  SubscriptionDeactivationReason,
  SubscriptionSort,
  URLAdapter,
  User,
  WepublishServer,
  WepublishServerOpts
} from '@wepublish/api'

import {MailgunMailProvider} from './TempMailgunMailProvider'

import {KarmaMediaAdapter} from '@wepublish/api-media-karma'
import {URL} from 'url'
import {MongoDBAdapter} from '@wepublish/api-db-mongodb/lib'

import {program} from 'commander'
import bodyParser from 'body-parser'

import jwt, {SignOptions} from 'jsonwebtoken'
import pinoMultiStream from 'pino-multi-stream'
import pinoStackdriver from 'pino-stackdriver'
import {createWriteStream} from 'pino-sentry'
import {sendNewsletter} from './sendNewsletterMail'
import {LegacyPaymentProvider} from './legacyPaymentProvider'
import path from 'path'
import FormData from 'form-data'
import * as Sentry from '@sentry/node'
import * as process from 'process'
import {maillogAnalytics} from './programs/maillogAnalytics'
import {transformUserDataForTesting} from './programs/devTasks'
import {subscriptionCleanUp} from './programs/subscriptionCleanUp'
import {TsriStripePaymentProvider} from './TempStripePaymentProvider'
import {getAllUsers, TWO_WEEKS_IN_MINUTES} from './utils'
import {SlackMailProvider} from './SlackMailProvider'
import {NoMoneyPaymentProvider} from './NoMoneyPaymentProvider'
import {
  manuallyCreateSubscription,
  manuallyRenewSubscription
} from './programs/manuallyCreateSubscription'

interface TsriURLAdapterProps {
  readonly websiteURL: string
}

class TsriURLAdapter implements URLAdapter {
  readonly websiteURL: string

  constructor(props: TsriURLAdapterProps) {
    this.websiteURL = props.websiteURL
  }

  getPublicArticleURL(article: PublicArticle): string {
    return `${this.websiteURL}/zh/${article.slug}.${article.id}`
  }

  getPublicPageURL(page: PublicPage): string {
    return `${this.websiteURL}/${page.slug}`
  }

  getAuthorURL(author: Author): string {
    return `${this.websiteURL}/redaktion/${author.slug || author.id}`
  }

  getArticlePreviewURL(token: string): string {
    return `${this.websiteURL}/article_preview/${token}`
  }

  getPagePreviewURL(token: string): string {
    return `${this.websiteURL}/preview/${token}` //TODO: implement
  }

  getLoginURL(token: string): string {
    return `${this.websiteURL}/api/login?jwt=${token}`
  }

  getCommentURL(item: PublicArticle | PublicPage, comment: PublicComment): string {
    return ''
  }

  getPeeredArticleURL(peer: Peer, article: PublicArticle): string {
    return ''
  }
}

export interface GenerateJWTProps {
  id: string
  audience?: string
  expiresInMinutes?: number
}

async function asyncMain() {
  if (!process.env.MONGO_URL) throw new Error('No MONGO_URL defined in environment.')
  if (!process.env.HOST_URL) throw new Error('No HOST_URL defined in environment.')
  if (!process.env.WEBSITE_URL) throw new Error('No WEBSITE_URL defined in environment.')

  const hostURL = process.env.HOST_URL
  const websiteURL = process.env.WEBSITE_URL

  const defaultFromMailAddress =
    process.env.DEFAULT_FROM_MAIL_ADDRESS ?? 'Seraina von Tsüri.ch info@tsri.ch'

  if (!process.env.MEDIA_SERVER_URL) {
    throw new Error('No MEDIA_SERVER_URL defined in environment.')
  }

  if (!process.env.MEDIA_SERVER_TOKEN) {
    throw new Error('No MEDIA_SERVER_TOKEN defined in environment.')
  }

  if (!process.env.MEDIA_SERVER_INTERNAL_URL) {
    throw new Error('No MEDIA_SERVER_URL defined in environment.')
  }

  const mediaAdapter = new KarmaMediaAdapter(
    process.env.MEDIA_SERVER_CACHED_URL && process.env.MEDIA_SERVER_USE_CACHED === 'true'
      ? new URL(process.env.MEDIA_SERVER_CACHED_URL)
      : new URL(process.env.MEDIA_SERVER_URL),
    process.env.MEDIA_SERVER_TOKEN,
    new URL(process.env.MEDIA_SERVER_INTERNAL_URL)
  )

  await MongoDBAdapter.initialize({
    url: process.env.MONGO_URL!,
    locale: process.env.MONGO_LOCALE ?? 'en',
    seed: async adapter => {
      const adminUserRole = await adapter.userRole.getUserRole('Admin')
      const adminUserRoleId = adminUserRole ? adminUserRole.id : 'fake'

      await adapter.user.createUser({
        input: {
          email: 'admin@tsri.ch',
          name: 'Admin',
          emailVerifiedAt: new Date(),
          roleIDs: [adminUserRoleId],
          active: true,
          properties: []
        },
        password: '123'
      })
    }
  })

  const port = process.env.PORT ? parseInt(process.env.PORT) : 4000
  const address = process.env.ADDRESS ? process.env.ADDRESS : 'localhost'

  const dbAdapter = await MongoDBAdapter.connect({
    url: process.env.MONGO_URL!,
    locale: process.env.MONGO_LOCALE ?? 'en'
  })

  const generateJWT = (props: GenerateJWTProps): string => {
    if (!process.env.JWT_SECRET_KEY) throw new Error('No JWT_SECRET_KEY defined in environment.')
    const jwtOptions: SignOptions = {
      issuer: hostURL,
      audience: props.audience ?? websiteURL,
      algorithm: 'HS256',
      expiresIn: `${props.expiresInMinutes || 15}m`
    }
    return jwt.sign({sub: props.id}, process.env.JWT_SECRET_KEY, jwtOptions)
  }

  const streams: pinoMultiStream.Streams = []
  if (process.env.NODE_ENV === 'development') {
    const prettyStream = pinoMultiStream.prettyStream()
    streams.push({stream: prettyStream})
  } else {
    streams.push({level: 'info', stream: process.stdout})
    if (process.env.GOOGLE_PROJECT) {
      streams.push({
        level: 'info',
        stream: pinoStackdriver.createWriteStream({
          projectId: process.env.GOOGLE_PROJECT,
          logName: 'juanita-api'
        })
      })
    }
    if (process.env.SENTRY_DSN) {
      streams.push({
        level: 'error',
        stream: createWriteStream({
          dsn: process.env.SENTRY_DSN,
          environment: process.env.SENTRY_ENV ?? 'dev'
        })
      })
    }
  }

  const logger = pinoMultiStream({
    streams,
    level: 'info'
  })

  const oauth2Providers: Oauth2Provider[] = []

  const {
    OAUTH_GOOGLE_DISCOVERY_URL,
    OAUTH_GOOGLE_CLIENT_ID,
    OAUTH_GOOGLE_CLIENT_KEY,
    OAUTH_GOOGLE_REDIRECT_URL
  } = process.env
  if (
    OAUTH_GOOGLE_DISCOVERY_URL &&
    OAUTH_GOOGLE_CLIENT_ID &&
    OAUTH_GOOGLE_CLIENT_KEY &&
    OAUTH_GOOGLE_REDIRECT_URL
  ) {
    oauth2Providers.push({
      name: 'google',
      discoverUrl: OAUTH_GOOGLE_DISCOVERY_URL,
      clientId: OAUTH_GOOGLE_CLIENT_ID,
      clientKey: OAUTH_GOOGLE_CLIENT_KEY,
      redirectUri: OAUTH_GOOGLE_REDIRECT_URL.split(','),
      scopes: ['openid profile email']
    })
  }
  const {
    OAUTH_TSRI_DISCOVERY_URL,
    OAUTH_TSRI_CLIENT_ID,
    OAUTH_TSRI_CLIENT_KEY,
    OAUTH_TSRI_REDIRECT_URL
  } = process.env
  if (
    OAUTH_TSRI_DISCOVERY_URL &&
    OAUTH_TSRI_CLIENT_ID &&
    OAUTH_TSRI_CLIENT_KEY &&
    OAUTH_TSRI_REDIRECT_URL
  ) {
    oauth2Providers.push({
      name: 'tsri',
      discoverUrl: OAUTH_TSRI_DISCOVERY_URL,
      clientId: OAUTH_TSRI_CLIENT_ID,
      clientKey: OAUTH_TSRI_CLIENT_KEY,
      redirectUri: OAUTH_TSRI_REDIRECT_URL.split(','),
      scopes: ['openid profile email']
    })
  }

  let mailProvider
  if (
    process.env.MAILGUN_API_KEY &&
    process.env.MAILGUN_BASE_DOMAIN &&
    process.env.MAILGUN_MAIL_DOMAIN &&
    process.env.MAILGUN_WEBHOOK_SECRET
  ) {
    mailProvider = new MailgunMailProvider({
      id: 'mailgun',
      name: 'Mailgun',
      fromAddress: defaultFromMailAddress,
      webhookEndpointSecret: process.env.MAILGUN_WEBHOOK_SECRET,
      baseDomain: process.env.MAILGUN_BASE_DOMAIN,
      mailDomain: process.env.MAILGUN_MAIL_DOMAIN,
      apiKey: process.env.MAILGUN_API_KEY,
      incomingRequestHandler: bodyParser.json()
    })
  } else {
    if (!process.env.MAILGUN_API_KEY) logger.warn('No MAILGUN_API_KEY defined in environment.')
    if (!process.env.MAILGUN_BASE_DOMAIN)
      logger.warn('No MAILGUN_BASE_DOMAIN defined in environment.')
    if (!process.env.MAILGUN_MAIL_DOMAIN)
      logger.warn('No MAILGUN_MAIL_DOMAIN defined in environment.')
    if (!process.env.MAILGUN_WEBHOOK_SECRET)
      logger.warn('No MAILGUN_WEBHOOK_SECRET defined in environment.')
  }

  if (process.env.SLACK_DEV_MAIL_WEBHOOK_URL) {
    mailProvider = new SlackMailProvider({
      id: 'slackMail',
      name: 'Slack Mail',
      fromAddress: 'fakeMail@tsri.ch',
      webhookURL: process.env.SLACK_DEV_MAIL_WEBHOOK_URL
    })
  }

  const paymentProviders: PaymentProvider[] = []
  if (process.env.STRIPE_SECRET_KEY && process.env.STRIPE_WEBHOOK_SECRET) {
    paymentProviders.push(
      new TsriStripePaymentProvider({
        id: 'stripe',
        name: 'Stripe',
        offSessionPayments: true,
        secretKey: process.env.STRIPE_SECRET_KEY,
        webhookEndpointSecret: process.env.STRIPE_WEBHOOK_SECRET,
        incomingRequestHandler: bodyParser.raw({type: 'application/json'})
      })
    )
  } else {
    if (!process.env.STRIPE_SECRET_KEY) logger.warn('No STRIPE_SECRET_KEY defined in environment.')
    if (!process.env.STRIPE_WEBHOOK_SECRET)
      logger.warn('No STRIPE_WEBHOOK_SECRET defined in environment.')
  }

  if (process.env.PAYREXX_INSTANCE_NAME && process.env.PAYREXX_API_SECRET) {
    paymentProviders.push(
      new PayrexxPaymentProvider({
        id: 'payrexx',
        name: 'Payrexx',
        offSessionPayments: false,
        instanceName: process.env.PAYREXX_INSTANCE_NAME,
        instanceAPISecret: process.env.PAYREXX_API_SECRET,
        psp: [0, 15, 17, 2, 3, 36],
        pm: [
          'twint',
          'postfinance_card',
          'postfinance_efinance',
          // "mastercard",
          // "visa",
          // "invoice",
          'paypal'
        ],
        vatRate: 7.7,
        incomingRequestHandler: bodyParser.json()
      })
    )
  } else {
    if (!process.env.PAYREXX_INSTANCE_NAME)
      logger.warn('No PAYREXX_INSTANCE_NAME defined in environment.')
    if (!process.env.PAYREXX_API_SECRET)
      logger.warn('No PAYREXX_API_SECRET defined in environment.')
  }

  paymentProviders.push(
    new LegacyPaymentProvider({
      id: 'legacy',
      name: 'Legacy',
      incomingRequestHandler: bodyParser.json(),
      offSessionPayments: false
    })
  )

  paymentProviders.push(
    new NoMoneyPaymentProvider({
      id: 'no-money',
      name: 'No Money',
      incomingRequestHandler: bodyParser.json(),
      offSessionPayments: true
    })
  )

  const tsriURLAdapter = new TsriURLAdapter({websiteURL})

  if (!process.env.ALGEBRAIC_CAPTCHA_CHALLENGE) {
    throw new Error('No ALGEBRAIC_CAPTCHA_CHALLENGE defined in environment.')
  }

  const challenge = new AlgebraicCaptchaChallenge(process.env.ALGEBRAIC_CAPTCHA_CHALLENGE, 600, {
    width: 200,
    height: 200,
    background: '#ffffff',
    noise: 5,
    minValue: 1,
    maxValue: 10,
    operandAmount: 1,
    operandTypes: ['+', '-'],
    mode: 'formula',
    targetSymbol: '?'
  })

  const wepublishOpts: WepublishServerOpts = {
    hostURL,
    websiteURL,
    mediaAdapter,
    dbAdapter,
    paymentProviders,
    mailProvider,
    challenge,
    mailContextOptions: {
      defaultFromAddress: defaultFromMailAddress,
      mailTemplatesPath: path.resolve('templates', 'emails'),
      mailTemplateMaps: [
        {
          type: SendMailType.NewMemberSubscription,
          localTemplate: 'juanita_new_member',
          local: true
        },
        {
          type: SendMailType.RenewedMemberSubscription,
          localTemplate: 'juanita_renewed_subscription',
          local: true
        },
        {
          type: SendMailType.MemberSubscriptionOffSessionFailed,
          localTemplate: 'juanita_offsession_failed',
          local: true
        },
        {
          type: SendMailType.LoginLink,
          remoteTemplate: 'juanita_login_link',
          local: false,
          subject: 'Tsüri.ch Login Link'
        },
        {
          type: SendMailType.MemberSubscriptionOnSessionAfter,
          localTemplate: 'juanita_onsession_after',
          local: true
        }
      ]
    },
    oauth2Providers,
    logger,
    urlAdapter: tsriURLAdapter,
    playground: process.env.HOST_ENV === 'development',
    introspection: true,
    tracing: process.env.HOST_ENV === 'development'
  }

  const server = new WepublishServer(wepublishOpts)

  program.version('0.0.1')

  program
    .command('listen')
    .description('start the api server')
    .action(async () => {
      await server.listen(port, address)
    })

  program
    .command('renew-memberships')
    .description('Renews all memberships')
    .action(async () => {
      await server.runJob(JobType.DailyMembershipRenewal, {
        startDate: new Date()
      })
      process.exit(0)
    })

  program
    .command('check-open-invoices')
    .description('Check intent state of all open invoices')
    .action(async () => {
      await server.runJob(JobType.DailyInvoiceChecker, {})
      process.exit(0)
    })

  program
    .command('charge-open-invoices')
    .description('Charge all open invoices')
    .action(async () => {
      await server.runJob(JobType.DailyInvoiceCharger, {})
      process.exit(0)
    })

  program
    .command('remind-open-invoices')
    .description('Remind all open invoices')
    .action(async () => {
      await server.runJob(JobType.DailyInvoiceReminder, {
        replyToAddress: 'info@tsri.ch',
        userPaymentURL: 'https://tsri.ch/account/subscription'
      })
      process.exit(0)
    })

  program
    .command('send-weekly-newsletter')
    .description('Sends the weekly newsletter')
    .action(async () => {
      const activeSubscriptions: Subscription[] = []
      let hasMoreSubscriptions = true
      let skip = 0
      while (hasMoreSubscriptions) {
        const subscriptions = await dbAdapter.subscription.getSubscriptions({
          cursor: {type: InputCursorType.None},
          limit: {count: 100, type: LimitType.First, skip},
          order: SortOrder.Ascending,
          sort: SubscriptionSort.CreatedAt,
          filter: {
            paidUntil: {comparison: DateFilterComparison.GreaterThanOrEqual, date: new Date()}
          }
        })
        hasMoreSubscriptions = subscriptions.pageInfo.hasNextPage
        skip += 100
        activeSubscriptions.push(...subscriptions.nodes)
      }

      const allUsers = await getAllUsers(dbAdapter)
      const usersWithActiveSubscription: User[] = []

      for (const user of allUsers) {
        if (user.email.includes('@tsri.ch')) {
          usersWithActiveSubscription.push(user)
        } else if (activeSubscriptions.some(subscription => user.id === subscription.userID)) {
          usersWithActiveSubscription.push(user)
        }
      }

      await sendNewsletter({users: usersWithActiveSubscription, defaultFromMailAddress})
      process.exit(0)
    })

  program
    .command('remind-expired-subscription')
    .description('Sends email reminders to people that have auto renew set to false')
    .action(async () => {
      const expiredSubscriptionButNotYetDeactivated: Subscription[] = []
      let hasMoreSubscriptions = true
      let skip = 0
      const today = new Date()
      while (hasMoreSubscriptions) {
        const subscriptions = await dbAdapter.subscription.getSubscriptions({
          cursor: {type: InputCursorType.None},
          limit: {count: 100, type: LimitType.First, skip},
          order: SortOrder.Ascending,
          sort: SubscriptionSort.CreatedAt,
          filter: {
            paidUntil: {comparison: DateFilterComparison.LowerThan, date: today},
            autoRenew: false,
            deactivationDate: {date: null, comparison: DateFilterComparison.Equal}
          }
        })
        hasMoreSubscriptions = subscriptions.pageInfo.hasNextPage
        skip += 100
        expiredSubscriptionButNotYetDeactivated.push(...subscriptions.nodes)
      }

      const TWO_WEEKS_IN_MILLISECONDS = 1209600000
      const ONE_DAY_IN_MILLISECONDS = 86400000
      const TWO_WEEKS_IN_MINUTES = 20160

      const baseDomain = process.env.MAILGUN_BASE_DOMAIN
      const mailDomain = process.env.MAILGUN_MAIL_DOMAIN
      const apiKey = process.env.MAILGUN_API_KEY
      const auth = Buffer.from(`api:${apiKey}`).toString('base64')

      const allUsers = await getAllUsers(dbAdapter)

      // TODO: filter out users that deactivated on purpose
      for (const subscription of expiredSubscriptionButNotYetDeactivated) {
        if (subscription.paidUntil === null) continue

        try {
          const {paidUntil} = subscription
          let template = ''
          let subject = 'Deine Tsüri-Mitgliedschaft ist abgelaufen'

          if (
            paidUntil < today &&
            paidUntil > new Date(today.getTime() - ONE_DAY_IN_MILLISECONDS)
          ) {
            template = 'juanita_subscription_expired_d1'
          } else if (
            paidUntil < new Date(today.getTime() - TWO_WEEKS_IN_MILLISECONDS) &&
            paidUntil >
              new Date(today.getTime() - TWO_WEEKS_IN_MILLISECONDS - ONE_DAY_IN_MILLISECONDS)
          ) {
            template = 'juanita_subscription_expired_w2'
          } else if (
            paidUntil <= new Date(today.getTime() - 2 * TWO_WEEKS_IN_MILLISECONDS) &&
            paidUntil >
              new Date(today.getTime() - 2 * TWO_WEEKS_IN_MILLISECONDS - ONE_DAY_IN_MILLISECONDS)
          ) {
            template = 'juanita_subscription_expired_w4'
            subject = 'Letzte Chance!'
          } else if (
            paidUntil <=
            new Date(today.getTime() - 2 * TWO_WEEKS_IN_MILLISECONDS - ONE_DAY_IN_MILLISECONDS)
          ) {
            await dbAdapter.subscription.updateSubscription({
              id: subscription.id,
              input: {
                ...subscription,
                deactivation: {
                  date: today,
                  reason: SubscriptionDeactivationReason.None
                }
              }
            })
          }
          const user = allUsers.find(user => user.id === subscription.userID)

          if (template === '' || !user) continue

          const jwt = generateJWT({
            id: user.id,
            audience: websiteURL,
            expiresInMinutes: TWO_WEEKS_IN_MINUTES
          })
          const loginURL = tsriURLAdapter.getLoginURL(jwt)

          await new Promise((resolve, reject) => {
            const form = new FormData()
            form.append('from', defaultFromMailAddress)
            form.append('to', user.email)
            form.append('subject', subject)
            form.append('v:name', user.preferredName ?? user.name ?? ':)')
            form.append('v:loginURL', loginURL)
            form.append('template', template)
            form.append('o:tracking-clicks', 'htmlonly')
            form.submit(
              {
                protocol: 'https:',
                host: baseDomain,
                path: `/v3/${mailDomain}/messages`,
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  Authorization: `Basic ${auth}`
                }
              },
              (err, res) => {
                console.log('status code', res.statusCode)
                return err || res.statusCode !== 200 ? reject(err || res) : resolve({})
              }
            )
          })
        } catch (error) {
          Sentry.captureException(error)
          console.error('error sending mail', error)
        }
      }
      process.exit(0)
    })

  program
    .command('onetime-reminder-202205')
    .option('-r, --real', 'actually do it')
    .description('temporary command')
    .action(async args => {
      const subscriptionsSinceDec2021: Subscription[] = []
      let hasMoreSubscriptions = true
      let skip = 0
      const migrationDate = new Date('2021-12-20')
      const today = new Date()
      while (hasMoreSubscriptions) {
        const subscriptions = await dbAdapter.subscription.getSubscriptions({
          cursor: {type: InputCursorType.None},
          limit: {count: 100, type: LimitType.First, skip},
          order: SortOrder.Ascending,
          sort: SubscriptionSort.CreatedAt,
          filter: {
            paidUntil: {
              comparison: DateFilterComparison.GreaterThanOrEqual,
              date: migrationDate
            },
            deactivationDate: {
              comparison: DateFilterComparison.LowerThanOrEqual,
              date: today
            }
          }
        })
        hasMoreSubscriptions = subscriptions.pageInfo.hasNextPage
        skip += 100
        subscriptionsSinceDec2021.push(...subscriptions.nodes)
      }

      const expiredSubscription = subscriptionsSinceDec2021.filter(
        subscription =>
          subscription.deactivation?.reason !== SubscriptionDeactivationReason.UserSelfDeactivated
      )

      const allUsers = await getAllUsers(dbAdapter)

      for (const subscription of expiredSubscription) {
        console.log(
          `${allUsers.find(user => user.id === subscription.userID)?.email ||
            'user not found'},${subscription.paidUntil?.getDate()}.${(subscription.paidUntil?.getMonth() ||
            0) + 1}.${subscription.paidUntil?.getFullYear()},${
            subscription.deactivation ? '1' : '0'
          }`
        )
      }
      console.log('count', expiredSubscription.length)

      if (args.real) {
        for (const subscription of expiredSubscription) {
          const user = allUsers.find(user => user.id === subscription.userID)
          if (subscription.paidUntil === null || !user) continue

          const jwt = generateJWT({
            id: user.id,
            audience: websiteURL,
            expiresInMinutes: TWO_WEEKS_IN_MINUTES
          })

          const loginURL = tsriURLAdapter.getLoginURL(jwt)

          try {
            let template = 'juanita_onetime_reminder_202205'
            let subject = 'Deine Mitgliedschaft ist abgelaufen: bitte jetzt verlängern!'

            const baseDomain = process.env.MAILGUN_BASE_DOMAIN
            const mailDomain = process.env.MAILGUN_MAIL_DOMAIN
            const apiKey = process.env.MAILGUN_API_KEY
            const auth = Buffer.from(`api:${apiKey}`).toString('base64')

            await new Promise((resolve, reject) => {
              const form = new FormData()
              form.append('from', defaultFromMailAddress)
              form.append('to', user.email)
              form.append('subject', subject)
              form.append('v:name', user.preferredName ?? user.name ?? ':)')
              form.append('v:loginURL', loginURL)
              form.append('template', template)
              form.submit(
                {
                  protocol: 'https:',
                  host: baseDomain,
                  path: `/v3/${mailDomain}/messages`,
                  method: 'POST',
                  headers: {
                    Accept: 'application/json',
                    Authorization: `Basic ${auth}`
                  }
                },
                (err, res) => {
                  console.log('status code', res.statusCode)
                  return err || res.statusCode !== 200 ? reject(err || res) : resolve({})
                }
              )
            })
          } catch (error) {
            Sentry.captureException(error)
            console.error('error sending mail', error)
          }
        }
      }
      process.exit(0)
    })

  program
    .command('onetime-reminder-before-dez')
    .option('-r, --real', 'actually do it')
    .description('temporary command')
    .action(async args => {
      const deactivatedSubscriptionsBeforeDez2021: Subscription[] = []
      let hasMoreSubscriptions = true
      let skip = 0
      const migrationDate = new Date('2021-12-20')
      while (hasMoreSubscriptions) {
        const subscriptions = await dbAdapter.subscription.getSubscriptions({
          cursor: {type: InputCursorType.None},
          limit: {count: 100, type: LimitType.First, skip},
          order: SortOrder.Ascending,
          sort: SubscriptionSort.CreatedAt,
          filter: {
            deactivationDate: {
              comparison: DateFilterComparison.LowerThanOrEqual,
              date: migrationDate
            }
          }
        })
        hasMoreSubscriptions = subscriptions.pageInfo.hasNextPage
        skip += 100
        deactivatedSubscriptionsBeforeDez2021.push(...subscriptions.nodes)
      }

      const expiredSubscription = deactivatedSubscriptionsBeforeDez2021.filter(
        subscription =>
          subscription.deactivation?.reason !== SubscriptionDeactivationReason.UserSelfDeactivated
      )

      const allUsers = await getAllUsers(dbAdapter)

      for (const subscription of expiredSubscription) {
        console.log(
          `${allUsers.find(user => user.id === subscription.userID)?.email ||
            'user not found'},${subscription.paidUntil?.getDate()}.${(subscription.paidUntil?.getMonth() ||
            0) + 1}.${subscription.paidUntil?.getFullYear()},${
            subscription.deactivation ? '1' : '0'
          }`
        )
      }
      console.log('count', expiredSubscription.length)

      if (args.real) {
        for (const subscription of expiredSubscription) {
          const user = allUsers.find(user => user.id === subscription.userID)
          if (subscription.paidUntil === null || !user) continue

          const jwt = generateJWT({
            id: user.id,
            audience: websiteURL,
            expiresInMinutes: TWO_WEEKS_IN_MINUTES
          })

          const loginURL = tsriURLAdapter.getLoginURL(jwt)

          try {
            let template = 'juanita_onetime_reminder_before_dec2021'
            let subject = 'Tsüri.ch vermisst dich :('

            const baseDomain = process.env.MAILGUN_BASE_DOMAIN
            const mailDomain = process.env.MAILGUN_MAIL_DOMAIN
            const apiKey = process.env.MAILGUN_API_KEY
            const auth = Buffer.from(`api:${apiKey}`).toString('base64')

            await new Promise((resolve, reject) => {
              const form = new FormData()
              form.append('from', defaultFromMailAddress)
              form.append('to', user.email)
              form.append('subject', subject)
              form.append('v:name', user.preferredName ?? user.name ?? ':)')
              form.append('v:loginURL', loginURL)
              form.append('template', template)
              form.submit(
                {
                  protocol: 'https:',
                  host: baseDomain,
                  path: `/v3/${mailDomain}/messages`,
                  method: 'POST',
                  headers: {
                    Accept: 'application/json',
                    Authorization: `Basic ${auth}`
                  }
                },
                (err, res) => {
                  console.log('status code', res.statusCode)
                  return err || res.statusCode !== 200 ? reject(err || res) : resolve({})
                }
              )
            })
          } catch (error) {
            Sentry.captureException(error)
            console.error('error sending mail', error)
          }
        }
      }
      process.exit(0)
    })

  program
    .command('create-subscription <email> <name>')
    .description('Create manually a subscription')
    .action(async (email, name) => {
      await manuallyCreateSubscription(dbAdapter, wepublishOpts, email, name)
      process.exit(0)
    })

  program
    .command('renew-subscription <email> [paymentMethodSlug] [paymentPeriodicity] [renewActive]')
    .description('Manually renew a subscription')
    .action(async (email, paymentMethodSlug, paymentPeriodicity, renewActive = false) => {
      // 'monthly',
      // 'quarterly',
      // 'biannual',
      // 'yearly'
      await manuallyRenewSubscription(
        dbAdapter,
        wepublishOpts,
        email,
        renewActive === 'true',
        paymentMethodSlug,
        paymentPeriodicity
      )
      process.exit(0)
    })

  program
    .command('maillog-analytics')
    .description('get maillog analytics')
    .action(async () => {
      await maillogAnalytics(dbAdapter)
      process.exit(0)
    })

  program
    .command('dev-transform-user-data')
    .description('transform all userdata for testing purposes')
    .action(async () => {
      await transformUserDataForTesting(dbAdapter)
      process.exit(0)
    })

  program
    .command('subscription-clean-up')
    .description('Cleanup old invoices and subscriptions')
    .action(async () => {
      await subscriptionCleanUp(dbAdapter)
      process.exit(0)
    })

  program
    .command('_healthz')
    .description('Check API health')
    .action(async () => {
      try {
        const adminUserRole = await dbAdapter.userRole.getUserRoleByID('admin')
        if (adminUserRole && adminUserRole.systemRole) {
          console.log('database connected => api healthy')
          process.exit(0)
        } else {
          console.log('database not connected => api unhealthy')
          process.exit(1)
        }
      } catch (error) {
        console.error('Health Check failed', error)
        process.exit(1)
      }
    })

  program.parse(process.argv)
}

asyncMain().catch(err => {
  console.error(err)
  process.exit(1)
})
